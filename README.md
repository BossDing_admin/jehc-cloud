# jehc
### 前言

本开源平台重点打造医疗（专攻医学软件Web方向，病历云，影像学，实验室检验等等），IOT方向，工作流Workflow等

 **申明
功能迭代中...** 
### 微服务版本
集成产品线版本：https://gitee.com/jehc/jehc-cloud.git

工作流版本：https://gitee.com/jehc/jehc-cloud-workflow

IOT版本：https://gitee.com/jehc/jehc-cloud-iot

报表版本：https://gitee.com/jehc/jehc-cloud-report

病历云版本：https://gitee.com/jehc/jehc-cloud-medical

即时通讯版本：https://gitee.com/jehc/jehc-cloud-im

代码生成器：https://gitee.com/jehc/jehc-cloud-help


### 单工程2.0版本
纯净版本：https://gitee.com/jehc/jehc

工作流版本：https://gitee.com/jehc/jehc-workflow

IOT版本：https://gitee.com/jehc/jehc-iot

病历云（HIS）版本：https://gitee.com/jehc/jehc-medical


### 单工程2.0前端版本
1.传统工程jehc-front

2.VUE版本jehc-front-vue（新增）


### 单工程1.0版本
纯净版本：https://gitee.com/jehc/jehc-boot

#### 介绍
JEHC-CLOUD基于Spring Cloud 2.X版本，采用前后端分离

集成了多方向平台如互联网，物联网，传统软件，医疗方向等

 **技术栈** 

 **后端** 

Spring，
SpringBoot2.0，
Mybatis，
PageHelper，
Solr全文检索，
Redis，
Ehcache，
JWT，
Oauth2，
数据库读写分离，
Activity5.22工作流，
客户端负载均衡Rule，
Sentinel限流体系，
Nacos注册中心 配置中心，
Gateway网关，
Junit，
Netty，
Quartz调度器，
FTP，
ES全文检索,
Openoffice，
Onvif摄像头,
OpenCV,
Mqtt,
ffmpeg



 **前端** 

可视化流程设计器，
Bootstrap4+，
Jquery2，
DataTables，
Mxgraph，
PDFJS，
ZTree,
SVGEditor,
VTK,
ITK,
video

 **开发工具** 

  eclipse-jee-mars-1、eclipse-jee-mars-2、eclipse-juno、STS、IDEA

#### 软件架构
 **架构图** 
![输入图片说明](https://gitee.com/uploads/images/2019/0424/112257_71e2da4a_1341290.png "微服务架构图.png")

 **后端工程** 

![输入图片说明](screenshot/%E5%B7%A5%E7%A8%8B%E5%9B%BE.png)

**前端工程VUE版本** 

![输入图片说明](screenshot/%E5%89%8D%E7%AB%AFvue%E7%89%88%E6%9C%AC.jpg)

 **前端工程传统版本** 

![输入图片说明](screenshot/%E5%89%8D%E7%AB%AF%E9%A1%B5%E9%9D%A2.png)

 **授权中心数据结构**
![输入图片说明](https://images.gitee.com/uploads/images/2021/0313/221812_6cc2e51a_1341290.png "JEHC-CLOUD微服务授权中心数据结构.png") 

#### 安装教程

1. 安装mysql5.7++数据库（其它数据库如Oracle）
2. 安装IntelliJ IDEA 2017.3.2 x64开发工具
3. 安装apache-maven-3.2.1及本地库repository
4. 安装Redis3版本以上
5. 导入JEHC-CLOUD工程项目
6. 设置maven环境

#### 功能模块


 **医学平台（将专攻该方向）**

![输入图片说明](screenshot/CT.jpg)

![输入图片说明](screenshot/%E6%A3%80%E6%9F%A5%E9%A1%B9.jpg)

![输入图片说明](screenshot/B%E8%B6%85.jpg)

![输入图片说明](screenshot/%E5%B0%B1%E8%AF%8A%E8%AE%B0%E5%BD%95.jpg)

![输入图片说明](screenshot/MR.jpg)

**工作流**

![输入图片说明](screenshot/%E6%B5%81%E7%A8%8B%E7%AE%A1%E7%90%86.jpg)
![输入图片说明](screenshot/%E5%9C%A8%E7%BA%BF%E8%AE%BE%E8%AE%A1.jpg)
![输入图片说明](screenshot/%E6%B4%BB%E5%8A%A8%E4%BB%BB%E5%8A%A1.jpg)
![输入图片说明](screenshot/%E6%B5%81%E7%A8%8B%E6%8A%A5%E8%A1%A8.jpg)
![输入图片说明](screenshot/%E6%B5%81%E7%A8%8B%E5%9B%BE.jpg)


**IOT**

![输入图片说明](screenshot/IOT.jpg)
![输入图片说明](https://images.gitee.com/uploads/images/2020/0312/111959_ba6cc0b7_1341290.png "tpt.png")
![输入图片说明](screenshot/IOT%E5%9B%BE%E5%85%83%E7%BC%96%E8%BE%91%E5%99%A8.jpg)
![输入图片说明](screenshot/IOT%E5%9B%BE%E5%85%83%E8%AE%BE%E8%AE%A1%E5%99%A8.jpg)

**运管**

![输入图片说明](screenshot/%E7%89%A9%E7%90%86%E6%9C%BA%E7%9B%91%E6%8E%A7.jpg)
![输入图片说明](screenshot/Redis%E7%9B%91%E6%8E%A7.png)

**文档**
![输入图片说明](screenshot/%E6%96%87%E6%A1%A3%E5%9C%A8%E7%BA%BF%E9%A2%84%E8%A7%88.jpg)
