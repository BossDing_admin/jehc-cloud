//返回r
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-function-role/oauth-function-role-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateOauthFunctionRole(){
	submitBForm('defaultForm',oauthModules+'/oauthFunctionRole/update',base_html_redirect+'/oauth/oauth-function-role/oauth-function-role-list.html',null,"PUT");
}
$(document).ready(function(){
	var function_role_id = GetQueryString("function_role_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthFunctionRole/get/'+function_role_id,{},function(result){
		$('#function_role_id').val(result.data.function_role_id);
		$('#function_info_id').val(result.data.function_info_id);
		$('#role_id').val(result.data.role_id);
		$('#resources_id').val(result.data.resources_id);

	});
});
