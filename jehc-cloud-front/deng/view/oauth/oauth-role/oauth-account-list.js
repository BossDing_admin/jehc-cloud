function addXtUserinfo(role_id,xt_role_name){
	var UserinfoModalCount = 0 ;
    $('#UserinfoBody').height(reGetBodyHeight()-128);
	$('#UserinfoModal').modal({backdrop:'static',keyboard:false});
	$('#UserinfoModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#UserinfoModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
		if(++UserinfoModalCount == 1){
			$('#UserinfoModalLabel').html("角色权限--->分配账户---><font color=red>"+xt_role_name+"</font>");
			$('#searchFormUnImportU')[0].reset();
			$('#searchFormImportU')[0].reset();
			$('#xt_role_id1').val(role_id);
			$('#xt_role_id2').val(role_id);
			initUnImportU(role_id);
			initImportU(role_id);

			initComboData("account_type_id_",oauthModules+"/oauthAccountType/listAll","account_type_id","title");
			initComboData("account_type_ids",oauthModules+"/oauthAccountType/listAll","account_type_id","title");

		}
    });  
}
/**待导入用户**/
function initUnImportU(role_id){
	var opt1 = {
			searchformId:'searchFormUnImportU'
		};
	var options1 = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,oauthModules+'/oauthRole/account/list?role_id='+role_id,opt1);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
		dom:'<"top"i>rt<"bottom"flp><"clear">',
		tableHeight:'120px',
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"account_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildUnImportU" value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"account_id",
				width:"50px"
			},
			{
				data:'account'
			},
			{
				data:'name'
			},
            {
                data:'account_type_text',
                render:function(data, type, row, meta) {
                    var oauthAccountTypes  = row.oauthAccountTypes;
                    var account_type_text = null;
                    if(null != oauthAccountTypes){
                        for(var i =0; i < oauthAccountTypes.length;i++){
                            console.info(oauthAccountTypes[i])
                            if(null == account_type_text){
                                account_type_text = oauthAccountTypes[i].title;
                            }else{
                                account_type_text = account_type_text+",</br>"+oauthAccountTypes[i].title;
                            }
                        }
                    }
                    return ""+account_type_text+"";
                }
            },
			{
				data:'email'
			}
		]
	});
	$('#UnImportUDatatables').dataTable(options1);
	//实现全选反选
	docheckboxall('checkallUnImportU','checkchildUnImportU');
	//实现单击行选中
	clickrowselected('UnImportUDatatables');
}


/**已导入用户**/
function initImportU(xt_role_id){
	var opt = {
			searchformId:'searchFormImportU'
		};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,oauthModules+'/oauthRole/account/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
		dom:'<"top"i>rt<"bottom"flp><"clear">',
		tableHeight:'120px',
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"account_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildImportU" value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"account_id",
				width:"50px"
			},
			{
				data:'account'
			},
			{
				data:'name'
			},
			{
				data:'account_type_text',
                render:function(data, type, row, meta) {
                    var oauthAccountTypes  = row.oauthAccountTypes;
                    var account_type_text = null;
                    if(null != oauthAccountTypes){
                        for(var i =0; i < oauthAccountTypes.length;i++){
                            console.info(oauthAccountTypes[i])
                            if(null == account_type_text){
                                account_type_text = oauthAccountTypes[i].title;
                            }else{
                                account_type_text = account_type_text+",</br>"+oauthAccountTypes[i].title;
                            }
                        }
                    }
                    return ""+account_type_text+"";
                }
			},
            {
                data:'email'
            }
		]
	});
	$('#ImportUDatatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkallImportU','checkchildImportU');
	//实现单击行选中
	clickrowselected('ImportUDatatables');

}

/**导入用户**/
function addXtUr(){
	if(returncheckedLength('checkchildUnImportU') <= 0){
		toastrBoot(4,"请选择待导入账户");
		return;
	}
	msgTishCallFnBoot("确定导入所选账户？",function(){
		var id = returncheckIds('checkchildUnImportU').join(",");
		var params = {account_id:id,role_id:$('#xt_role_id1').val()};
		ajaxBRequestCallFn(oauthModules+'/oauthRole/addOauthAR',params,function(result){
			window.parent.toastrBoot(3,result.message);
			search('UnImportUDatatables');
			search('ImportUDatatables');
		},null,"POST");
		
	})
}


/**移除用户**/
function delXtUR(){
	if(returncheckedLength('checkchildImportU') <= 0){
		toastrBoot(4,"请选择要移除的账户");
		return;
	}
	msgTishCallFnBoot("确定移除所选账户？",function(){
		var id = returncheckIds('checkchildImportU').join(",");
		var params = {account_id:id,role_id:$('#xt_role_id1').val(),_method:'DELETE'};
		ajaxBRequestCallFn(oauthModules+'/oauthRole/delOauthAR',params,function(result){
			window.parent.toastrBoot(3,result.message);
			search('UnImportUDatatables');
			search('ImportUDatatables');
		},null,"DELETE");
	})	
}