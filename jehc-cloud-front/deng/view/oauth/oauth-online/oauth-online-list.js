var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,oauthModules+'/oauthOnline/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"oauthAccountEntity.account_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
            {
                data:'oauthAccountEntity.account_id',
                width:"50px"
            },
            {
                data:'oauthAccountEntity.account',
                width:"100px"
            },
			{
				data:'oauthAccountEntity.name',
                width:"100px"
			},
			{
				data:'oauthAccountEntity.type',
                width:"100px",
				 render:function(data, type, row, meta) {
					 return "缺省";
				 }
			},
			{
				data:'oauthAccountEntity.last_login_time',
                width:"150px"/*,
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }*/
			},
            {
                data:"oauthAccountEntity.account_id",
                render:function(data, type, row, meta) {
                	var account = row.oauthAccountEntity.account;
                	var name = row.oauthAccountEntity.name;
                    var btn = '<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick=removeAccount("'+data+'","'+account+'","'+name+'") title="剔除"><i class="la la-close"></i></button> ';
                    return btn;
                }
            }
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
});


function removeAccount(id,account,name) {
    msgTishCallFnBoot("确定要剔除<br>账号："+account+",<br>姓名："+name+"？",function(){
        var params = {_method:'DELETE'};
        ajaxBReq(oauthModules+'/oauthOnline/delete/'+id,params,['datatables'],null,"DELETE");
    })
}