//编辑窗体
function updateXtFunctioninfo(id){
    // var zTree = $.fn.zTree.getZTreeObj("tree"),
    //     nodes = zTree.getSelectedNodes();
    // if (nodes.length != 1) {
    //     toastrBoot(4,"必须选择一条记录");
    //     return;
    // }
    // if(nodes[0].tempObject != 'Function'){
    //     toastrBoot(4,"选择的记录必须为功能");
    //     return;
    // }
    $('#updateXtFunctioninfoForm')[0].reset();
    $('#updateXtFunctioninfoForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    $.ajax({
        type:"GET",
        url:oauthModules+'/oauthFunctionInfo/get/'+id,
        success: function(result){
            result = result.data;
            $("#updateXtFunctioninfoForm").find("#menu_id_").val(result.menu_id);
            $("#updateXtFunctioninfoForm").find("#resources_title").val(result.resources_title);
            $("#updateXtFunctioninfoForm").find("#function_info_name").val(result.function_info_name);
            $("#updateXtFunctioninfoForm").find("#function_info_url").val(result.function_info_url);
            $("#updateXtFunctioninfoForm").find("#function_info_method").val(result.function_info_method);
            $("#updateXtFunctioninfoForm").find("#isfilter").val(result.isfilter);
            $("#updateXtFunctioninfoForm").find("#isAuthority").val(result.isAuthority);
            $("#updateXtFunctioninfoForm").find("#status").val(result.status);
            $("#updateXtFunctioninfoForm").find("#function_info_id").val(result.function_info_id);
            $('#updateXtFunctioninfoModal').modal({"backdrop":"static"});
        }
    });
}

function doUpdateXtFunctioninfo(){
    submitBFormCallFn('updateXtFunctioninfoForm',oauthModules+'/oauthFunctionInfo/update',function(result){
        try {
            if(typeof(result.success) != "undefined"){
                if(result.success){
                    window.parent.toastrBoot(3,result.message);
                    initTreeTable();
                    $('#updateXtFunctioninfoModal').modal('hide');
                }else{
                    window.parent.toastrBoot(4,result.message);
                }
            }
        } catch (e) {

        }
    },null,"PUT");
}