
$(document).ready(function(){
    init();
});

function init() {
    $("#bodyTemp").html("");
    //加载表单数据
    ajaxBRequestCallFn(oauthModules+'/oauthSysMode/listAll/',{},function(result){
        var length = result.data.length;
        var contentDIV = '<div class="row m-row--no-padding m-row--col-separator-xl">';
        for(var i = 1; i <= length; i++){
            var data = result.data[i-1];
            var time = data.create_time;
            var id =  data.sys_mode_id;
            if(null == time){
                time = "暂无";
            }
            if(i % 4 == 0){
                contentDIV = contentDIV+'<div class="col-md-12 col-lg-6 col-xl-3"><div class="m-widget24"><div class="m-widget24__item"><h4 class="m-widget24__title"><i class="'+data.sys_mode_icon+'"></i>'+data.sysname+' </h4><br><span class="m-widget24__desc"> &nbsp;&nbsp;&nbsp;&nbsp;'+time+'</span> <span class="m-widget24__stats m--font-brand" style="cursor: pointer;" onclick=deleteModule("'+id+'")><i class="la la-remove"></i></span></div></div></div>';
                $("#bodyTemp").append(contentDIV);
                contentDIV = "";
                if(i < length){
                    contentDIV = '<div class="row m-row--no-padding m-row--col-separator-xl">';
                }
            }else{
                contentDIV = contentDIV+'<div class="col-md-12 col-lg-6 col-xl-3"><div class="m-widget24"><div class="m-widget24__item"><h4 class="m-widget24__title"><i class="'+data.sys_mode_icon+'"></i>'+data.sysname+' </h4><br><span class="m-widget24__desc">&nbsp;&nbsp;&nbsp;&nbsp;'+time+'</span> <span class="m-widget24__stats m--font-brand"  style="cursor: pointer;" onclick=deleteModule("'+id+'")><i class="la la-remove"></i></span></div></div></div>';
            }
        }
        if("" != contentDIV){
            $("#bodyTemp").append(contentDIV);
        }
    });
}

/**
 * 卸载模块
 * @param id
 */
function deleteModule(id) {
    msgTishCallFnBoot("确定要卸载该模块数据？",function(){
        var params = {id:id,_method:'DELETE'};
        ajaxBRequestCallFn(oauthModules+'/oauth/uninstall',params,function () {
            init();
            window.parent.toastrBoot(3,"卸载成功！");
        },null,"DELETE");
    })
}