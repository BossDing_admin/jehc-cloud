var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,oauthModules+'/oauthSysMode/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
                jQuery('td:eq(1)', nRow).html("<span class='row-details la la-plus' data_id='"+aData.sys_mode_id+"' sysname='"+aData.sysname+"'></span> ");
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"sys_mode_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"sys_mode_id",
				width:"50px"
			},
			{
				data:'sysmode',
                width:"50px"
			},
			{
				data:'sysname',
                width:"50px"
			},
			{
				data:'sys_mode_status',
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--info'>正常</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--success'>禁用</span>"
                    }
                    if(data == 2){
                        return "<span class='m-badge m-badge--danger'>冻结</span>"
                    }
                    return "<span class='m-badge m-badge--info'>缺省</span>"
                }
			},
            {
                data:"createBy",
                width:"150px",
                render:function(data, type, row, meta) {
                    if(null == data || data == ''){
                        return "<font color='#6495ed'>\</font>";
                    }
                    return data;
                }
            },
            {
                data:"create_time",
                width:"150px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:"modifiedBy",
                width:"150px",
                render:function(data, type, row, meta) {
                    if(null == data || data == ''){
                        return "<font color='#6495ed'>\</font>";
                    }
                    return data;
                }
            },
            {
                data:"update_time",
                width:"150px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
			{
				data:"sys_mode_id",
				render:function(data, type, row, meta) {
					var sysname =  row.sysname;
                    var btn = '<button onclick=toOauthSysModeDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情"><i class="m-menu__link-icon la la-eye"></i></button>'
                    btn = btn+'<button onclick=addOauthSysModules("'+data+'","'+sysname+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="创建资源模块"><i class="m-menu__link-icon la la-plus"></i></button>'
                    return btn;
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
    $('.table').on('click', ' tbody td .row-details', function () {
        var nTr = $(this).parents('tr')[0];
        if ( grid.fnIsOpen(nTr) )//判断是否已打开
        {
            /* This row is already open - close it */
            $(this).addClass("la-plus").removeClass("la-minus");
            grid.fnClose( nTr );
        }else{
            /* Open this row */
            $(this).addClass("la-minus").removeClass("la-plus");
            // 调用方法显示详细信息 data_id为自定义属性 存放配置ID
            fnFormatDetails(nTr,$(this).attr("data_id"),$(this).attr("sysname"));
        }
    });
});
//新增
function toOauthSysModeAdd(){
	tlocation(base_html_redirect+'/oauth/oauth-sys-mode/oauth-sys-mode-add.html');
}
//修改
function toOauthSysModeUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+'/oauth/oauth-sys-mode/oauth-sys-mode-update.html?sys_mode_id='+id);
}
//详情
function toOauthSysModeDetail(id){
	tlocation(base_html_redirect+'/oauth/oauth-sys-mode/oauth-sys-mode-detail.html?sys_mode_id='+id);
}
//删除
function delOauthSysMode(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {sys_mode_id:id,_method:'DELETE'};
		ajaxBReq(oauthModules+'/oauthSysMode/delete',params,['datatables'],null,"DELETE");
	})
}

/**
 *
 * @param nTr
 * @param pdataId
 * @param sysname
 */
function fnFormatDetails(nTr,pdataId,sysname){
    //根据配置Id 异步查询数据
    //加载表单数据
    ajaxBRequestCallFn(oauthModules+'/oauthSysModules/list/'+pdataId,{},function(result){
        var res = result.data;
        var sOut = "<div style='text-align: center;border:1px; '><table  class='table' style='width: 50%;margin: auto;'>";
        console.log(res);
        if(res.length == 0){
            sOut+='<tr>';
            sOut+='<td width="100%">暂无数据</td>';
            sOut+='</tr>'
            sOut+='</table>';
            grid.fnOpen( nTr,sOut, 'details' );
            return;
		}else{
            sOut+='<tr>';
            sOut+='<td width="30%" style="font-size:16px;background:#e1f0ff">名称</td>';
            sOut+='<td width="30%" style="font-size:16px;background:#e1f0ff"><div class="div_left">模块标记</div></td>';
            sOut+='<td width="30%" style="font-size:16px;background:#e1f0ff"><div class="div_left">状态</div></td>';
            sOut+='<td width="10%" style="font-size:16px;background:#e1f0ff"><div class="div_left">操作</div></td>';
            sOut+='</tr>'

            for(var i=0;i<res.length;i++){
                sOut+='<tr>';
                sOut+='<td width="35%">'+res[i].sys_modules_name+'</td>';
                sOut+='<td width="30%"><div class="div_left">'+res[i].sys_modules_mode+'</div></td>';
                var sys_modules_status = res[i].sys_modules_status;
                var statusMsg = "缺省";
                if(sys_modules_status == 0){
                    statusMsg = "<span class='m-badge m-badge--info'>正常</span>";
                }
                if(sys_modules_status == 1){
                    statusMsg = "<span class='m-badge m-badge--danger'>冻结</span>";
                }
                sOut+='<td style="width:30%"><div class="div_left">'+statusMsg+'</div></td>';
                var btn = '<button onclick=updataeOauthSysModules("'+res[i].sys_modules_id+'","'+sysname+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="编辑资源模块">' + '<i class="m-menu__link-icon fa fa-magic fa-lg"></i>' + '</button>'
                btn = btn + '<button onclick=delOauthSysModules("'+res[i].sys_modules_id+'","'+sysname+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="冻结资源模块">' + '<i class="m-menu__link-icon fa fa-times fa-lg"></i>' + '</button>'
                sOut+='<td style="width:30%"><div class="div_left">'+btn+'</div></td>';
                sOut+='</tr>'
            }
            sOut+='</table></div>';
            grid.fnOpen( nTr,sOut, 'details' );
		}
    },null,"POST");
    /*
     $.ajax({
			type:'post',
			url:oauthModules+'/oauthSysModules/list',
			data:{"pdataId":pdataId},
		 	dataType:"text",
		  	async:true,
		 	beforeSend:function(xhr){//信息加载中
				grid.fnOpen( nTr, '<span id="configure_chart_loading">详细信息加载中...</span>', 'details' );
				},
			success:function (data,textStatus){
				if(textStatus=="success"){	//转换格式 组合显示内容
					var res = eval("("+data+")");
					var sOut = '<table style="width:100%;">';
					for(var i=0;i<res.length;i++){
						sOut+='<tr>';
						sOut+='<td width="5%"></td><td width="35%">'+res[i].name+'</td>';
						sOut+='<td width="30%"><div class="div_left">'+res[i].num1+'</div><div class="div_center">|</div><div class="div_right">'+res[i].count1+'</div></td>';
						sOut+='<td style="width:30%"><div class="div_left">'+res[i].num2+'</div><div class="div_center">|</div><div class="div_right">'+res[i].count2+'</div></td>';
						sOut+='</tr>'

					}
					sOut+='</table>';
					grid.fnOpen( nTr,sOut, 'details' );
				}
			},
			error: function(){//请求出错处理
				grid.fnOpen( nTr,'加载数据超时~', 'details' );
			}
		});*/

    /** 假数据
    var resData=[{name:'a1',num1:22,count1:'70.25%',num2:21,count2:'46.02%'},{name:'a2',num1:57,count1:'18.21%',num2:14,count2:'31.16%'},{name:'a3',num1:34,count1:'10.8%',num2:116,count2:'24.48%'},{name:'a4',num1:12,count1:'3.96%',num2:195,count2:'4.24%'},{name:'a5',num1:33,count1:'1.06%',num2:13,count2:'2.96%'}];
    var sOut = '<table style="width:100%;">';
    for(var i=0;i<resData.length;i++){
        sOut+='<tr>';
        sOut+='<td width="10%"></td><td width="30%">'+resData[i].name+'</td>';
        sOut+='<td width="30%">'+resData[i].num1+'<div class="div_center"></td>';
        sOut+='<td style="width:30%">'+resData[i].num2+'</td>';
        sOut+='</tr>'

    }
    sOut+='</table>';
    grid.fnOpen( nTr,sOut, 'details' );
	**/
}

$('#SysModulesForm').bootstrapValidator({
    message:'此值不是有效的'
});

/**
 *
 * @param id
 * @param sysname
 */
function addOauthSysModules(id,sysname) {
    var SysModulesModalCount = 0 ;
    $("#SysModulesModalLabel").text("创建子系统【"+sysname+"】-资源模块");
    $('#SysModulesModal').modal({backdrop:'static',keyboard:false});
    $('#SysModulesModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        $('#SysModulesForm')[0].reset();
        $("#sysmode_id").val(id);
    });
}

/**
 *
 * @param id
 * @param sysname
 */
function updataeOauthSysModules(sys_modules_id,sysname) {
    var SysModulesModalCount = 0 ;
    $("#SysModulesModalLabel").text("编辑子系统【"+sysname+"】-资源模块");
    $('#SysModulesModal').modal({backdrop:'static',keyboard:false});
    $('#SysModulesModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        $('#SysModulesForm')[0].reset();
        SysModulesModalCount++;
        if(SysModulesModalCount == 1) {
            //加载表单数据
            ajaxBRequestCallFn(oauthModules+'/oauthSysModules/get/'+sys_modules_id,{},function(result){
                $('#sys_modules_id').val(result.data.sys_modules_id);
                $('#sys_modules_name').val(result.data.sys_modules_name);
                $('#sys_modules_mode').val(result.data.sys_modules_mode);
                $('#sys_modules_status').val(result.data.sys_modules_status);
                $('#sysmode_id').val(result.data.sysmode_id);
                $('#keyid').val(result.data.keyid);
            });
        }
    });
}

/**
 * 保存
 */
function saveOauthSysModules() {
    var sys_modules_id = $('#sys_modules_id').val();
    if(null != sys_modules_id && "" != sys_modules_id){
        doUpdateOauthSysModules();
    }else{
        doAddOauthSysModules();
    }
}

//执行保存
function doUpdateOauthSysModules(){
    submitBForm('SysModulesForm',oauthModules+'/oauthSysModules/update',base_html_redirect+'/oauth/oauth-sys-mode/oauth-sys-mode-list.html',null,"PUT");
}

//执行添加
function doAddOauthSysModules(){
    submitBForm('SysModulesForm',oauthModules+'/oauthSysModules/add',base_html_redirect+'/oauth/oauth-sys-mode/oauth-sys-mode-list.html');
}

/**
 *
 * @param sys_modules_id
 * @param sysname
 */
function delOauthSysModules(sys_modules_id,sysname){
    msgTishCallFnBoot("确定要冻结："+sysname+"？",function(){
        var params = {sys_modules_id:sys_modules_id,_method:'DELETE'};
        ajaxBReq(oauthModules+'/oauthSysModules/delete',params,['datatables'],null,"DELETE");
    })
}