//返回
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-function-common/oauth-function-common-list.html');
}
$(document).ready(function(){
	var function_common_id = GetQueryString("function_common_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthFunctionCommon/get/'+function_common_id,{},function(result){
		$('#function_common_id').val(result.data.function_common_id);
		$('#function_common_title').val(result.data.function_common_title);
		$('#function_common_url').val(result.data.function_common_url);
		$('#function_common_method').val(result.data.function_common_method);
		$('#function_common_status').val(result.data.function_common_status);
		$('#function_common_content').val(result.data.function_common_content);
		$('#sysmode_id').val(result.data.sysmode_id);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
        initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname",result.data.sysmode_id);
	});
});
