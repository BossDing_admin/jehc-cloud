$(document).ready(function(){
    initData();
    initPage();
    initLockSystem();
    // $("#content-main").css("height", tableHeight()-153);
});


function indexHome(){
}
function clickAddTab(url,title,id){
	if(null == url || url == ''){
		return;
	}	
	$('#triggerID').attr('data-index',id);
	$('#triggerID').attr('rootId',id);
	$('#triggerID').attr('idBu',id);
	$('#triggerID').attr('href',url);
	$('#triggerSpanID').text(title);
	document.getElementById("triggerSpanID").click();
}
function doActive(id,idList,tabIdList){
	try{
    	//清空之前选中样式
    	if(typeof(tabIdList) == "undefined" || null == tabIdList || '' == tabIdList){
    		tabIdList = Addtabs.tabIdList();
    	}
    	for(var i = 0; i < tabIdList.length; i++){
    		var tabIdTemp = tabIdList[i];
    		if(null != tabIdTemp && '' != tabIdTemp){
    			var tabIdArray = tabIdTemp.split(',');
    			for(var j = 0; j < tabIdArray.length; j++){
    				$('#menu'+tabIdArray[j]).removeClass('start active open');
    			}
    		}
    	}
        //选中菜单样式开始
        if(null != idList && '' != idList){
        	var idArray = idList.split(',');
        	for(var i = 0; i < idArray.length; i++){
        		$('#menu'+idArray[i]).addClass('start active open');
            }
        }
        if(null != id && '' != id){
        	$('#menu'+id).addClass('start active open');
        }
    	//选中菜单样式结束
    }catch (e) {
	}
}

/**注销**/
function loginout(){
	msgTishCallFnBoot("确定要注销平台？",function(){
		$.ajax({
			url:oauthModules+'/loginOut',
	        type:'POST',
	        success:function(result){
	        	toastrBoot(3,"注销平台成功!");
	        	var win = top;
				if(window.opener != null) {win=opener.top; window.close();}
				win.location.href=loginPath;
	        }, 
	        error:function(){
	        	toastrBoot(4,"注销平台失败!");
	        }
	    })
	})
}

/////////////修改密码开始///////////////////
function updatePwd(){
	$('#updatePwdModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){  
		$("#updatePwdModalDialog").css({'width':'420px'}); 
    });
	$('#updatePwdForm').bootstrapValidator({
	  message:'此值不是有效的',
	  feedbackIcons:{
	  },
	  fields:{
		  oldPwd:{
	          validators:{
	              notEmpty:{
	                  message:'旧密码不能为空'
	              },
	              stringLength:{
	                  min:1,
	                  max:30,
	                  message:'用户账号字符长度不能超过30个'
	              }
	          }
	      },
	      newPwd:{
	         validators:{
	             notEmpty:{
	                 message:'新密码不能为空'
	             },
	             stringLength:{
	                 min:1,
	                 max:20,
	                 message:'新密码长度不能超过20个'
	             }
	         }
	     },
	     surePwd:{
	         validators:{
	             notEmpty:{
	                 message:'确认密码不能为空'
	             },
	             stringLength:{
	            	 min:1,
	                 max:20,
	                 message:'确认密码长度不能超过20个'
	             }
	         }
	     }
	  }
	});
}
function doUpdate(){
	var updatePwdForm =  $('#updatePwdForm');
	if(typeof(updatePwdForm) == "undefined" ||null == updatePwdForm || '' == updatePwdForm){
		window.parent.toastrBoot(4,"未能获取到form对象!");
		return;
	}
	var boostrapValidator =updatePwdForm.data('bootstrapValidator');
	boostrapValidator.validate();
	//验证有效开启发送异步请求
	if(boostrapValidator.isValid()){
		var newPwd = $('#newPwd').val();
		var surePwd = $('#surePwd').val();
		if(newPwd != surePwd){
			window.parent.toastrBoot(4,"两次输入的密码不一样,请重新输入!");
			return;
		}
		submitBFormCallFn('updatePwdForm',oauthModules+'/updatePwd',function(result){
			try{
	    		if(typeof(result.success) != "undefined"){
	    			if(result.success == true){
	    				msgTishCallFnBoot("修改密码成功，请重新登录该平台！",function(){
	                		var win = top;
	    					if(window.opener != null){win=opener.top; window.close();}
	    					win.location.href=loginPath;
	                	})
	    			}else{
	    				window.parent.toastrBoot(4,result.message);
	    			}
	    			
	    		}
			}catch(e){
				
			}
		},null,"POST");
	}else{
		window.parent.toastrBoot(4,"存在不合法的字段!");
	}
}
//////////////////修改密码结束////////////////


//////////////////初始化锁屏开始//////////////
function initLockSystem(flag){
	if(getCookie("syslock") == 1){
		$('#lockModal').modal({backdrop: 'static', keyboard: false}).on("shown.bs.modal",function(){  
			$("#lockModalDialog").css({'width':'420px'}); 
	    });
		setCookie("syslock", '1', 240);
	}else{
		if(flag == 1){
			$('#lockModal').modal({backdrop: 'static', keyboard: false}).on("shown.bs.modal",function(){  
				$("#lockModalDialog").css({'width':'420px'}); 
		    });
			setCookie("syslock", '1', 240);
		}
	}
}
//////////////////初始化锁屏结束//////////////

//////////////////执行解锁功能开始////////////
function unlockSystem() {
	var lockForm =  $('#lockForm');
	var password = $('#password').val();
	if(null == password || '' == password){
		window.parent.toastrBoot(4,"请输入解锁密码！");
		return;
	}
	$.fn.serializeObject = function(){
         var o = {};
         var a = this.serializeArray();
         $.each(a, function() {
             if (o[this.name] !== undefined) {
                 if (!o[this.name].push) {
                     o[this.name] = [o[this.name]];
                 }
                 o[this.name].push(this.value || '');
             } else {
                 o[this.name] = this.value || '';
             }
         });
         return o;
    };
	$.ajax({
		url:oauthModules+'/unlock',
        type:'POST',//PUT DELETE POST
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(lockForm.serializeObject()),
        success:function(result){
        	try{
	        	obj = result;
	        	if(obj.message == '1'){
	        		setCookie("syslock", '0', 240);
	        		$('#lockModal').modal('hide');
	        	}else{
	        		window.parent.toastrBoot(4,"密码错误！");
	        	}
        	}catch(e){}
        }, 
        error:function(){
        	window.parent.toastrBoot(4,"解锁失败!");
        }
    })
}
//////////////////解锁结束///////////////////
//关键字搜索
function search(){
	//采用Tab页打开方式
	//clickAddTab(basePath+'/xtSearchController/loadXtSearch?keywords='+encodeURI($('#keywords').val()),'关键词检索','search_page_');
	//直接打开新窗体方式
	// window.open('xtSearch/loadXtSearch?keywords='+encodeURI($('#keywords').val()), "_blank");
}

function updateUserPic(){
	toastrBoot(1,'该功能暂未开放');
}

//判断iframe是否加载完毕
var xtIframe;
function loadXtIframeComplete(id,dt1,text){
	/**开启多个Tab目的**/
	xtIframe = document.getElementById("iframe"+id);
	if(xtIframe == null){
		var dt2 = nowTimestamp();
		//执行监控页面信息操作
		loadinfo(text,dt1,dt2);
	}else{
		if(xtIframe.attachEvent){ 
			xtIframe.attachEvent("onload", function(){ 
				//执行监控页面信息操作
				loadinfo(text,dt1,dt2);
			}); 
		}else{ 
			xtIframe.onload = function(){ 
				var dt2 = nowTimestamp();
				//执行监控页面信息操作
				loadinfo(text,dt1,dt2);
			}; 
		} 
	}
}

//返回当前时间戳
function nowTimestamp(){
	return new Date().getTime();
}
function dt(){
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	var hour = date.getHours();
	var minute = date.getMinutes();
	var second = date.getSeconds();
	var dts = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;
	return dts;
}
//执行监控页面信息操作
function loadinfo(text,dt1,dt2){
    // $.ajax({
    //     url:rest_url_prefix_xt+'/xtLoading/add',
    //     type:'POST',
    //     contentType:"application/json;charset=utf-8",
    //     data:JSON.stringify({xt_loadinfo_begtime:dt1,xt_loadinfo_endtime:dt2,xt_loadinfo_modules:text}),
    //     success:function(result){
    //     },
    //     error:function(){
    //     }
    // })
}
//切换肤色
function displayTheme(){
	var themecss = getCookie("css");
	if(null != themecss && "" != themecss){
		if(themecss == 'defaultClass'){
			$("#chatheme1").attr("checked","checked");
		}else if(themecss == 'whiteClass'){
			$("#chatheme2").attr("checked","checked");
		}else if(themecss == 'BlackminiClass'){
			$("#chatheme3").attr("checked","checked");
		}else if(themecss == 'WhiteminiClass'){
			$("#chatheme4").attr("checked","checked");
		}else if(themecss == 'BlackminiClassshrink'){
			$("#chatheme5").attr("checked","checked");
		}else if(themecss == 'WhiteminiClassshrink'){
			$("#chatheme6").attr("checked","checked");
		}else if(themecss == 'BlackBigtextClass'){
            $("#chatheme7").attr("checked","checked");
        }else if(themecss == 'WhiteBigtextClass'){
            $("#chatheme8").attr("checked","checked");
        }else if(themecss == 'theme5'){
            $("#chatheme9").attr("checked","checked");
        }
	}else{
		$("#chatheme1").attr("checked","checked");
	}
	$('#themeModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){  
		$("#themeModalDialog").css({'width':'420px'}); 
    });
}
//保存肤色
function changeTheme(){
	var chatheme = $('input[name="chatheme"]:checked').val();
	msgTishCallFnBoot("切换肤色之后平台将重新刷新整个页面,确定要切换肤色吗？",function(){
		 var date=new Date();
      	 date.setTime(date.getTime() + 365*24*3066*1000);
		 window.document.cookie="css=" + chatheme + ";expires=" + date.toGMTString();
      	 window.document.cookie="css=" + chatheme + ";expires=" + date.toGMTString()+";path=/";
      	 window.location.href = basePath + "/index.html";
	})
}
/*//获取通知信息
function geDyRemind(){
	ajaxBRequestCallFn('../xtDyRemindController/getXtDyRemindList',null,function(result){
		////////////////我的通知开始////////////////
		var xtNotifyReceiver=result.xtNotifyReceiverList;
		if(null == xtNotifyReceiver){
			return;
		}
		$('#notifyNum').html(xtNotifyReceiver.length);
		$('#notifyNumright').html(xtNotifyReceiver.length);
		$("#notify").empty();
		if(xtNotifyReceiver.length>15){
			for(var i=0;i<15;i++){
				var xt_notify = xtNotifyReceiver[i];
				var xt_notify_receiver_id=xt_notify.xt_notify_receiver_id;
				var xt_notify_title=xt_notify.xt_notify_title;
				var sub_xt_notify_title;
				if(xt_notify_title.length>10){
					sub_xt_notify_title=xt_notify_title.substr(0,10)+"...";
				}else{
					sub_xt_notify_title=xt_notify_title;
				};
				var receive_time=new Date(xt_notify.receive_time);
				receive_time = receive_time.toLocaleStringMDHM();
				$("#notify").append("<li><a target='_blank' onclick=\"toXtReceiverDetail('"+xt_notify_receiver_id+"')\"><span class='time'>"+receive_time+"</span><span class='details' title='"+xt_notify_title+"'><span class='label label-sm label-icon label-success'><i class='fa fa-plus'></i></span>"+sub_xt_notify_title+"</span></a></li>")
			}
			$("#notify").append("<li class='text-center'><a href='../xtNotifyController/loadXtNotify' target='_blank'>查看更多</a></li>")
		}else{
			for(var i=0;i<xtNotifyReceiver.length;i++){
				var xt_notify = xtNotifyReceiver[i];
				var xt_notify_receiver_id=xt_notify.xt_notify_receiver_id;
				var xt_notify_title=xt_notify.xt_notify_title;
				var sub_xt_notify_title;
				if(xt_notify_title.length>10){
					sub_xt_notify_title=xt_notify_title.substr(0,10)+"...";
				}else{
					sub_xt_notify_title=xt_notify_title;
				};
				var receive_time=new Date(xt_notify.receive_time);
				receive_time = receive_time.toLocaleStringMDHM();
				$("#notify").append("<li><a target='_blank' onclick=\"toXtReceiverDetail('"+xt_notify_receiver_id+"')\"><span class='time'>"+receive_time+"</span><span class='details' title='"+xt_notify_title+"'><span class='label label-sm label-icon label-success'><i class='fa fa-plus'></i></span>"+sub_xt_notify_title+"</span></a></li>")
			}
		}
		////////////////我的通知结束////////////////
		
		
		
		////////////////我的消息开始////////////////
		var xtMessageList=result.xtMessageList;
		if(null == xtMessageList){
			return;
		}
		$('#messageNum').html(xtMessageList.length);
		$('#messageNumright').html(xtMessageList.length);
		$("#message").empty();
		if(xtMessageList.length>15){
			for(var i=0;i<15;i++){
				var xtMessage = xtMessageList[i];
				var xt_message_id=xtMessage.xt_message_id;
				var xt_meesage_content=xtMessage.xt_meesage_content;
				var sub_xt_meesage_content;
				if(xt_meesage_content.length>10){
					sub_xt_meesage_content=xt_meesage_content.substr(0,10)+"...";
				}else{
					sub_xt_meesage_content=xt_meesage_content;
				};
				var ctime=new Date(xtMessage.ctime);
				ctime = ctime.toLocaleStringMDHM();
				var fromName = xtMessage.fromName;
				$("#message").append(
					"<li>"+
		                "<a href='#' target='_blank' onclick=\"toXtMessageDetail('"+xt_message_id+"')\">"+
		                    '<span class="photo"><img src="..../../deng/images/default/user.png" class="img-circle" alt=""> </span>'+
		                    '<span class="subject">'+
		                        '<span class="from"> '+fromName+' </span>'+
		                        '<span class="time">'+ctime+' </span>'+
		                    '</span>'+
		                    '<span class="message">'+sub_xt_meesage_content+'</span>'+
		                '</a>'+
		            '</li>'
	            )
				//$("#message").append("<li><a target='_blank' onclick=\"toXtMessageDetail('"+xt_message_id+"')\"><span class='time'>"+ctime+"</span><span class='details' title='"+xt_meesage_content+"'><span class='label label-sm label-icon label-success'><i class='fa fa-plus'></i></span>"+sub_xt_meesage_content+"</span></a></li>")
			}
			$("#message").append("<li class='text-center'><a href='../xtMessageController/loadXtMessage' target='_blank'>查看更多</a></li>")
		}else{
			for(var i=0;i<xtMessageList.length;i++){
				var xtMessage = xtMessageList[i];
				var xt_message_id=xtMessage.xt_message_id;
				var xt_meesage_content=xtMessage.xt_meesage_content;
				var fromName = xtMessage.fromName;
				if(xt_meesage_content.length>10){
					sub_xt_meesage_content=xt_meesage_content.substr(0,10)+"...";
				}else{
					sub_xt_meesage_content=xt_meesage_content;
				};
				var ctime=new Date(xtMessage.ctime);
				ctime = ctime.toLocaleStringMDHM();
				$("#message").append(
					"<li>"+
		                "<a href='#' target='_blank' onclick=\"toXtMessageDetail('"+xt_message_id+"')\">"+
		                    '<span class="photo"><img src="..../../deng/images/default/user.png" class="img-circle" alt=""> </span>'+
		                    '<span class="subject">'+
		                        '<span class="from"> '+fromName+' </span>'+
		                        '<span class="time">'+ctime+' </span>'+
		                    '</span>'+
		                    '<span class="message">'+sub_xt_meesage_content+'</span>'+
		                '</a>'+
		            '</li>'
	            )
				//$("#message").append("<li><a target='_blank' onclick=\"toXtMessageDetail('"+xt_message_id+"')\"><span class='time'>"+ctime+"</span><span class='details' title='"+xt_meesage_content+"'><span class='label label-sm label-icon label-success'><i class='fa fa-plus'></i></span>"+sub_xt_meesage_content+"</span></a></li>")
			}
		}
		////////////////我的消息结束////////////////
	},{isShowWating:false});
}
geDyRemind();
setInterval(geDyRemind,50000);

Date.prototype.toLocaleStringMDHM = function() {
    return (this.getMonth() + 1) + "-" + this.getDate() + " " + this.getHours() + ":" + this.getMinutes();
};*/
function toXtReceiverDetail(xt_notify_receiver_id){
	// window.open('xtNotifyReceiver/toXtReceiverDetail?xt_notify_receiver_id='+xt_notify_receiver_id,"_blank");
	// setTimeout(geDyRemind,500)
}

function toXtMessageDetail(xt_message_id){
	// window.open('xtMessage/toXtMessageDetail?type=1&xt_message_id='+xt_message_id,"_blank");
	// setTimeout(geDyRemind,500)
}

var dashBoardHtml = "<li class=\"m-menu__item \" aria-haspopup=\"true\"><a href=\"../../index.html\" class=\"m-menu__link \"><i class=\"m-menu__link-icon flaticon-line-graph\"></i><span class=\"m-menu__link-title\"> <span class=\"m-menu__link-wrap\"> <span class=\"m-menu__link-text\">首页</span>\n" +
    "<span class=\"m-menu__link-badge\"><span class=\"m-badge m-badge--danger\">2</span></span> </span></span></a></li>"

var menuEntitys = [];
function initData(){
    var dialogWating = showWating({msg:'正在初始化首页数据...'});
    // $("#MenuList").append(dashBoardHtml);
    $.ajax({
        url:oauthModules+'/init',
        type:'POST',//PUT DELETE POST
        success:function(result){
            closeWating(null,dialogWating);
            result = result.data;
            try{
                //设置AdminMenuList
                $("#AdminMenuList").append(result.adminMenuList);
                menuEntitys = result.menuEntity;
                if(null != menuEntitys && menuEntitys.length >= 1){
                    //设置MenuList
                    $("#MenuList").append(result.menuEntity[0].menuList);
				}
                if(menuEntitys.length > 1){
                    // initSysModeHtml(menuEntitys);//扁平化暂时废弃
                    initSysMode(menuEntitys);
                    $("#AdminMenus").show();
                }else{
                    $("#AdminMenus").hide();
				}
                initSysModeStyle();
                //给用户赋值
                if(null != result.oauthAccountEntity){
                    $("#IndexUserRealName").append(result.oauthAccountEntity.name);
                    $("#IndexUserName").append(result.oauthAccountEntity.account);
                    sys_pt_user_name = result.oauthAccountEntity.account;
                    xt_userinfo_id = result.oauthAccountEntity.account_id;
                    // $("#tUserName").append("你好，"+result.oauthAccountEntity.name);
                    initMyPic(sys_pt_user_name);
                }
                includContabsJS();
			}catch (e){

			}

        },
        error:function(){
            console.log('首页设置属性出现异常');
            closeWating(null,dialogWating);
            includContabsJS();
        }
    })
}

function includContabsJS(){
    $("script[src='/deng/source/plugins/newAdmin/tab/js/contabs.min.js']").remove();
    $.getScript("/deng/source/plugins/newAdmin/tab/js/contabs.min.js").done(function() {
        console.log('contabs.min.js加载成功')
    }).error(function(err){
        console.log('contabs.min.js加载失败')
    });
}
function initPage() {
    var colorTheme="dark";
    var mini = "NO";
    var classshrink = "NO";
    var bigTextTheme = "NO";
    var cookieC = getCookie("css");
    var theme5 = false;
    if(null != cookieC && '' != cookieC){
        if(cookieC.indexOf("whiteClass")>=0 || cookieC.indexOf("Whitemini")>=0 || cookieC.indexOf("bigtextClass")>=0){
            colorTheme = "light";
        }
        if(cookieC.indexOf("miniClass")>=0){
            mini = "YES";
            if(cookieC.indexOf("Classshrink")>=0){
                classshrink = "YES";
            }
        }
        if(cookieC.indexOf("BlackBigtextClass")>=0){
            bigTextTheme = "YES";
        }
        if(cookieC.indexOf("WhiteBigtextClass")>=0){
            bigTextTheme = "YES";
            colorTheme = "light";
        }
        if(cookieC.indexOf("theme5")>=0){
            theme5 = true;
            mini = "YES";
            colorTheme = "light";
        }
    }
    $("script[src='/deng/source/plugins/newAdmin/using/base/theme/smallleft/scripts.bundle.js']").remove();
    $("script[src='/deng/source/plugins/newAdmin/using/base/theme/bigleft/scripts.bundle.js']").remove();
    $("script[src='/deng/source/plugins/newAdmin/using/base/theme/bigtextleft/scripts.bundle.js']").remove();
    $("script[src='/deng/source/plugins/newAdmin/using/base/theme/theme5/scripts.bundle.js']").remove();
    if(theme5){
        $("body").attr("class","m--skin- m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default");
        $("#m_aside_left_close_btn").attr("class","m-aside-left-close  m-aside-left-close--skin-dark ");
        $("#m_aside_left").attr("class","m-aside-left  m-aside-left--skin-dark ");
        $("#m_header_topbar").attr("class","m-topbar  m-stack m-stack--ver m-stack--general ");
        // $("#m_header_nav").attr("class","m-stack__item m-stack__item--right ");
        $(".m-topbar").css({"background-color":"#fff","margin-top":"-80px"});
        window.document.getElementsByTagName("link")[12].href = "/deng/source/plugins/newAdmin/using/base/theme/theme5/style.bundle.css";
        //动态加载JS
        $.getScript("/deng/source/plugins/newAdmin/using/base/theme/theme5/scripts.bundle.js").done(function() {
            console.log('scripts.bundle.js加载成功')
        }).error(function(err){
            console.log('scripts.bundle.js加载失败')
        });
    }else{
        if(bigTextTheme == "YES"){
            $("#brandSkin").attr("class","m-stack__item m-brand  m-brand--skin-");
            //动态加载CSS
            $("#m_aside_left").attr("class","m-grid__item m-aside-left  m-aside-left--skin-"+colorTheme);
            //设置m_ver_menu
            $("#m_ver_menu").attr("class","m-aside-menu  m-aside-menu--skin-"+colorTheme+" m-aside-menu--submenu-skin-"+colorTheme+" m-aside-menu--dropdown ");
            $("#m_ver_menu").attr("data-menu-vertical","true");
            $("#m_ver_menu").attr("m-menu-dropdown","1");
            $("#m_ver_menu").attr("m-menu-scrollable","0");
            //设置body class
            $("body").attr("class","m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-"+colorTheme+" m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default");
            //设置m_header_nav
            if(phoneLogin() == "pc"){
                $("#m_header_nav_title").empty();
                $("#m_header_nav_title").show();
                $("#m_header_nav_title").append("<h3 class='m-header__title-text' style='font-weight: 500;font-size: 1.4rem;color:#4c4c50;padding-left:30px;overflow: hidden;left: 50%;height: 100%; top: 50%;transform: translateX(0%)  translateY(150%); float: left;text-align: center;'>"+sys_pt_index_top+"</h3>");
            }
            //设置m_header_menu
            $("#m_header_menu").attr("class","m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-"+colorTheme+" m-header-menu--submenu-skin-"+colorTheme+" m-aside-header-menu-mobile--skin-"+colorTheme+" m-aside-header-menu-mobile--submenu-skin-"+colorTheme+"");
            window.document.getElementsByTagName("link")[12].href = "/deng/source/plugins/newAdmin/using/base/theme/bigtextleft/style.bundle.css";
            //动态加载JS
            $.getScript("/deng/source/plugins/newAdmin/using/base/theme/bigtextleft/scripts.bundle.js").done(function() {
                console.log('scripts.bundle.js加载成功')
            }).error(function(err){
                console.log('scripts.bundle.js加载失败')
            });
        }else{
            //设置brandSkin
            $("#brandSkin").attr("class","m-stack__item m-brand  m-brand--skin-"+colorTheme);
            //设置m_aside_left
            $("#m_aside_left").attr("class","m-grid__item m-aside-left  m-aside-left--skin-"+colorTheme+"");
            //设置m_ver_menu
            $("#m_ver_menu").attr("class","m-aside-menu  m-aside-menu--skin-"+colorTheme+" m-aside-menu--submenu-skin-"+colorTheme+"");
            //设置m_aside_header_menu_mobile_close_btn
            $("#m_aside_header_menu_mobile_close_btn").attr("class","m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-"+colorTheme);
            if(mini == "YES"){
                //动态加载CSS
                window.document.getElementsByTagName("link")[12].href = "/deng/source/plugins/newAdmin/using/base/theme/bigleft/style.bundle.css";
                if(classshrink == "YES"){
                    //设置body class
                    $("body").attr("class","m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-"+colorTheme+" m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--fixed m-footer--push m-aside--offcanvas-default");
                }else{
                    //设置body class
                    $("body").attr("class","m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default");
                }
                //设置m_header_menu
                $("#m_header_menu").attr("class","m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-"+colorTheme+" m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark");
                if(phoneLogin() == "pc"){
                    $("#m_header_nav_title").empty();
                    $("#m_header_nav_title").show();
                    $("#m_header_nav_title").append("<h3 class='m-header__title-text'>"+sys_pt_index_top+"</h3>");
                }
                //动态加载JS
                $.getScript("/deng/source/plugins/newAdmin/using/base/theme/bigleft/scripts.bundle.js").done(function() {
                    console.log('scripts.bundle.js加载成功')
                }).error(function(err){
                    console.log('scripts.bundle.js加载失败')
                });
            }else {
                //动态加载CSS
                window.document.getElementsByTagName("link")[12].href = "/deng/source/plugins/newAdmin/using/base/theme/smallleft/style.bundle.css";
                //设置body class
                $("body").attr("class","m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--fixed m-footer--push m-aside--offcanvas-default");
                //设置m_header_nav
                $("#m_header_nav_title").empty();
                $("#m_header_nav_title").show();
                $("#m_header_nav_title").append("<h3 class='m-header__title-text'>"+sys_pt_index_top+"</h3>");
                //设置m_header_menu
                $("#m_header_menu").attr("class","m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-"+colorTheme+" m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light");
                //动态加载JS
                $.getScript("/deng/source/plugins/newAdmin/using/base/theme/bigleft/scripts.bundle.js").done(function() {
                    console.log('scripts.bundle.js加载成功')
                }).error(function(err){
                    console.log('scripts.bundle.js加载失败')
                });
            }
        }
	}
}
$(document).ready(function() {
    // var objects = {};
    // $("#keywords").typeahead({
    //     source: function(query, process) { //query是输入框输入的文本内容, process是一个回调函数
    //         $.get(rest_url_prefix_oauth+"/xtKnowledge/query", {keywords: query}, function(data) {
    //             if (data == "" || data.length == 0) { console.log("没有查询到相关结果"); };
    //             var results = [];
    //             for (var i = 0; i < data.length; i++) {
    //                 objects[data[i].xt_knowledge_title] = data[i].xt_knowledge_id;
    //                 results.push(data[i].xt_knowledge_title);
    //             }
    //             process(results);
    //         });
    //     },
    //     afterSelect: function (item) {       //选择项之后的事件，item是当前选中的选项
    //         $("#customer_id").val(objects[item]); //为隐藏输入框赋值
    //     },
    // });
});


function initSysModeStyle(){
    var themecss = getCookie("css");
    if( themecss == 'whiteClass'){
        $("#m_header_nav").css('background','#fff');
    }else if(themecss == 'defaultClass'){
        $("#m_header_nav").css('background','#fff');
    }else{
        $("#m_header_nav").css('background','#fff');
    }
}

//扁平化（暂时废弃）
function initSysModeHtml(menuEntitys){
    var themecss = getCookie("css");
    var sysModeHtml = [];
    var sysModeHtml = '<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">';
    if(null == themecss || themecss == 'defaultClass' || themecss == 'whiteClass'){
        sysModeHtml += '<div class="btn-group btn-group-lg" role="group" aria-label="Large button group">';
    }else{
        sysModeHtml += '<div class="btn-group" role="group" aria-label="Default button group">';
    }
	for(var i =0; i < menuEntitys.length; i++){
		var menuEntity = menuEntitys[i];
        var menuList = menuEntity.menuList;
        if(null == themecss || themecss == 'defaultClass' || themecss == 'whiteClass'){//btn btn-sys btn-lg m-btn m-btn m-btn--icon
            sysModeHtml += '<a href=javascript:changeMenu(this,"'+menuEntity.sys_mode_id+'"); class="btn btn-sys btn-lg m-btn m-btn m-btn--icon" style="border-radius:0px;">';
            sysModeHtml += '<i class="m-menu__link-icon '+ menuEntity.sys_mode_icon +'" id="icon'+menuEntity.sys_mode_id+'" style="font-size: 1.8rem;"></i>';
            sysModeHtml += '<span class="m-menu__link-text" style="padding-top:0px;font-size: 18px;font-family: monospace;" id="text'+menuEntity.sys_mode_id+'">';
            sysModeHtml += menuEntity.sysname;
            sysModeHtml += '</span>';
            sysModeHtml += '</a>';
        }else{//btn btn-sys m-btn m-btn m-btn--icon
            sysModeHtml += '<a href=javascript:changeMenu(this,"'+menuEntity.sys_mode_id+'"); class="btn btn-sys btn-lg m-btn m-btn m-btn--icon" style="border-radius:0px;">';
            sysModeHtml += '<i class="m-menu__link-icon '+ menuEntity.sys_mode_icon +'" id="icon'+menuEntity.sys_mode_id+'" style="font-size: 1.4rem;"></i>';
            sysModeHtml += '<span class="m-menu__link-text" style="padding-top:2px;font-size: 1.02rem;font-family: normal;font-weight: 400;text-transform: initial;" id="text'+menuEntity.sys_mode_id+'" >';
            sysModeHtml += menuEntity.sysname;
            sysModeHtml += '</span>';
            sysModeHtml += '</a>';
		}
	}
    sysModeHtml += '</div>';
    sysModeHtml += '</li>';
    $("#AdminMenuList").append(sysModeHtml);

    if(menuEntitys.length >= 1){
        $("#text"+menuEntitys[0].sys_mode_id).css('color','#36a3f7');
        $("#icon"+menuEntitys[0].sys_mode_id).css('color','#36a3f7');
	}
}



//经典
function initSysMode(menuEntitys){
    var sysModeHtml="";
    for(var i =0; i < menuEntitys.length; i++){
        var menuEntity = menuEntitys[i];
        var sysModeHtml = '<li class="m-menu__item ">';
        sysModeHtml += '<a href=javascript:changeMenu(this,"'+menuEntity.sys_mode_id+'"); class="m-menu__link" style="border-radius:0px;">';
        sysModeHtml += '<i class="m-menu__link-icon '+ menuEntity.sys_mode_icon +'" id="icon'+menuEntity.sys_mode_id+'" style="font-size: 1.4rem;"></i>';
        sysModeHtml += '<span class="m-menu__link-text" id="text'+menuEntity.sys_mode_id+'" >';
        sysModeHtml += menuEntity.sysname;
        sysModeHtml += '</span>';
        sysModeHtml += '</a>';
        sysModeHtml += '</li>';
        $("#AdminMenuSubList").append(sysModeHtml);
    }

    if(menuEntitys.length >= 1){
        $("#text"+menuEntitys[0].sys_mode_id).css('color','#36a3f7');
        $("#icon"+menuEntitys[0].sys_mode_id).css('color','#36a3f7');
    }
}

/**
 *
 * @param sys_mode_id
 */
function changeMenu(thiz,sys_mode_id){
    $("#MenuList").html("")
    for(var i =0; i < menuEntitys.length; i++){
        var menuEntity = menuEntitys[i];
        $("#text"+menuEntity.sys_mode_id).css('color','#676c7b');
        $("#icon"+menuEntity.sys_mode_id).css('color','#676c7b');
        if(sys_mode_id == menuEntity.sys_mode_id){
            var menuList = menuEntity.menuList;//设置MenuList
            $("#MenuList").append(menuList);
		}
	}
    $("#text"+sys_mode_id).css('color','#36a3f7');
    $("#icon"+sys_mode_id).css('color','#36a3f7');
    includContabsJS();
}


/**
 * 跳入到在线聊天
 */
function goIM(){
    window.open(base_html_redirect+'/im/chat.html');
    // document.location.href=base_html_redirect+'/im/chat.html';
}


//进行切换
var fullScreenClickCount=0;
//调用各个浏览器提供的全屏方法
var handleFullScreen = function () {
    var de = document.documentElement;

    if (de.requestFullscreen) {
        de.requestFullscreen();
    } else if (de.mozRequestFullScreen) {
        de.mozRequestFullScreen();
    } else if (de.webkitRequestFullScreen) {
        de.webkitRequestFullScreen();
    } else if (de.msRequestFullscreen) {
        de.msRequestFullscreen();
    }
    else {
        wtx.info("当前浏览器不支持全屏！");
    }

};
//调用各个浏览器提供的退出全屏方法
var exitFullscreen=function() {
    if(document.exitFullscreen) {
        document.exitFullscreen();
    } else if(document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if(document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}

function clickFull() {
    if (fullScreenClickCount % 2 == 0) {
        handleFullScreen();
        $("#fullScreen").attr("class","m-nav__link-icon la la-compress");
        $("#fullScreenText").html("恢复显示");
    } else {
        exitFullscreen();
        $("#fullScreen").attr("class","m-nav__link-icon la la-expand");
        $("#fullScreenText").html("全屏显示");

    }
    fullScreenClickCount++;
}

/**
 * 上传头像
 */
var userPicFileUploadResultArray = [];
function uploadMyPic() {
    $('#userPicFileModal').modal({backdrop: 'static', keyboard: false});
    var userPicModalCount = 0 ;
    $('#userPicFileModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++userPicModalCount == 1) {
            userPicFileUploadResultArray.push('0');
            //使用bootstrap-fileinput渲染
            $('#userPicFile').fileinput({
                language:'zh',//设置语言
                uploadUrl:sysModules+'/xtUserinfo/pic/upload',//上传的地址
                type: 'POST',
                contentType: "application/json;charset=utf-8",
                enctype: 'multipart/form-data',
                allowedFileExtensions:["png","jpg","bmp","gif"],//接收的文件后缀
                theme:'fa',//主题设置
                dropZoneTitle:'可以将文件拖放到这里',
                autoReplace: true,//布尔值，当上传文件达到maxFileCount限制并且一些新的文件被选择后，是否自动替换预览中的文件。如果maxFileCount值有效，那么这个参数会起作用。默认为false
                overwriteInitial: true,//布尔值，是否要覆盖初始预览内容和标题设置。默认为true，因此当新文件上传或文件被清除时任何initialPreview内容集将被覆盖。将其设置为false将有助于始终显示从数据库中保存的图像或文件，尤其是在使用多文件上传功能时尤其有用。
                uploadAsync:true,//默认异步上传
                showPreview:true,//是否显示预览
                // showUpload: true,//是否显示上传按钮
                showRemove: true,//显示移除按钮
                showCancel:true,//是否显示文件上传取消按钮。默认为true。只有在AJAX上传过程中，才会启用和显示
                showCaption: true,//是否显示文件标题，默认为true
                browseClass: "btn btn-light-primary font-weight-bold mr-2", //文件选择器/浏览按钮的CSS类。默认为btn btn-primary
                dropZoneEnabled: true,//是否显示拖拽区域
                validateInitialCount: true,
                maxFileSize: 0,//最大上传文件数限制，单位为kb，如果为0表示不限制文件大小
                minFileCount: 1, //每次上传允许的最少文件数。如果设置为0，则表示文件数是可选的。默认为0
                maxFileCount: 1, //每次上传允许的最大文件数。如果设置为0，则表示允许的文件数是无限制的。默认为0
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",//当检测到用于预览的不可读文件类型时，将在每个预览文件缩略图中显示的图标。默认为<i class="glyphicon glyphicon-file"></i>
                previewFileIconSettings: {
                    'docx': '<i ass="fa fa-file-word-o text-primary"></i>',
                    'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                    'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                    'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                    'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                    'pdf': '<i class="fa fa-file-archive-o text-muted"></i>',
                    'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                },
                uploadExtraData: function () {
                    return {
                        name: 'test'
                    }; //上传时额外附加参数
                },
                // msgSizeTooLarge: "文件 '{name}' (<b>{size} KB</b>) 超过了允许大小 <b>{maxSize} KB</b>.",
                msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",//字符串，当文件数超过设置的最大计数时显示的消息 maxFileCount。默认为：选择上传的文件数（{n}）超出了允许的最大限制{m}。请重试您的上传！
                // elErrorContainer:'#kartik-file-errors',
                ajaxSettings:{
                    headers:{
                        "token":getToken()
                    }
                }
            }).on('change',function () {
                // 清除掉上次上传的图片
                $(".uploadPreview").find(".file-preview-frame:first").remove();
                $(".uploadPreview").find(".kv-zoom-cache:first").remove();
            }).on('fileuploaded',function (event,data,previewId,index) {//异步上传成功处理
                console.log('单个上传成功，并清空上传控件内容',data,previewId,index);
                for(var i = 0; i < userPicFileUploadResultArray.length; i++){
                    if(i == 0){
                        //清除文件输入 此方法清除所有未上传文件的预览，清除ajax文件堆栈，还清除本机文件输入
                        $('#userPicFile').fileinput('clear');
                        $('#userPicFile').fileinput('clear').fileinput('disable');
                        window.parent.toastrBoot(3,data.response.message);
                        //关闭文件上传的模态对话框
                        $('#userPicFileModal').modal('hide');
                        try{
                            var result = data.response.data;
                            if(null == result || undefined === result || "" == result || null == result.xt_userinfo_image || undefined === result.xt_userinfo_image || "" == result.xt_userinfo_image){
                                return;
                            }
                            $('#mySmallPic').attr("src", result.xt_userinfo_image);
                            $('#myPic').attr("src", result.xt_userinfo_image);
                        }catch (e){

                        }
                        userPicFileUploadResultArray.splice(0,userPicFileUploadResultArray.length);
                        i--;
                        break;
                    }
                }
            }).on('fileerror',function (event,data,msg) { //异步上传失败处理
                console.log('单个上传失败，并清空上传控件内容',data,msg);
                window.parent.toastrBoot(4,"上传文件失败！");
            }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
                console.log('批量上传成功，并清空上传控件内容',data,previewId,index);
            }).on('filebatchuploaderror', function(event, data, msg) {
                //批量上传失败，并清空上传控件内容
                console.log('批量上传失败，并清空上传控件内容',data,msg);
            }).on("filebatchselected", function (event, data, previewId, index) {
                console.log('选择文件',data,previewId,index);
            })/*.on('filepreajax', function(event, previewId, index) {
                var container = $("#divId");
                var processDiv = container.find('.kv-upload-progress');
                processDiv.hide();
                $('#lcProcessFile').fileinput('enable');
                return false;
            })*/;
        }
    });
}

/**
 * 关闭窗体
 */
function closeUserPicFileWin() {
    $("#userPicFile").fileinput('reset'); //重置上传控件（清空已文件）
    $('#userPicFile').fileinput('clear');
    //关闭上传窗口
    $('#userPicFileModal').modal('hide');
}

/**
 * 初始化我的头像
 * @param userName
 */
function initMyPic(userName) {
    if(null == userName || undefined === userName || userName == "" ){
        return;
    }
    $.ajax({
        url:sysModules+'/xtUserinfo/pic/'+userName,
        type:'GET',//PUT DELETE POST
        success:function(result){
            try{
                result = result.data;
                if(null == result || undefined === result || "" == result || null == result.xt_userinfo_image || undefined === result.xt_userinfo_image || "" == result.xt_userinfo_image){
                    return;
                }
                $('#mySmallPic').attr("src", result.xt_userinfo_image);
                $('#myPic').attr("src", result.xt_userinfo_image);
            }catch (e){

            }
        },
        error:function(){

        }
    })
}