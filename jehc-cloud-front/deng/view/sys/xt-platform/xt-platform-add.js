//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-platform/xt-platform-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function addXtPlatform(){
	submitBForm('defaultForm',sysModules+'/xtPlatform/add',base_html_redirect+'/sys/xt-platform/xt-platform-list.html');
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});

function addXtPlatformFeedbackItems(){
	validatorDestroy('defaultForm');
	var numbers = $('#xtPlatformFeedbackFormNumber').val();
	numbers = parseInt(numbers)+1;
	$('#xtPlatformFeedbackFormNumber').val(numbers);
	//点击添加新一行
	var removeBtn = '<a class="btn btn-secondary m-btn m-btn--custom m-btn--label-danger m-btn--bolder m-btn--uppercase" href="javascript:delXtPlatformFeedbackItems(this,'+numbers+')" ><span><i class="fa fa-times"></i><span>移除</span></span></a>';
	var form = 
		'<div id="form_xtPlatformFeedback_'+numbers+'">'+
			'<fieldset>'+
			'<legend><h5>序号'+numbers+'</h5></legend>'+
			'<div class="form-group m-form__group row"><div class="col-lg-3">'+removeBtn+'</div></div>'+
			'<div class="form-group m-form__group row">'+
				'<label class="col-md-1 col-form-label">平台发布信息编号</label>'+
				'<div class="col-md-3">'+
					'<input class="form-control" type="text" maxlength="32"  id="xtPlatformFeedback_'+numbers+'_xt_platform_id" name="xtPlatformFeedback[][xt_platform_id]"  placeholder="请输入">'+
				'</div>'+
			'</div>'+
			'<div class="form-group m-form__group row">'+
				'<label class="col-md-1 col-form-label">评论内容</label>'+
				'<div class="col-md-3">'+
					'<textarea class="form-control" maxlength="500"  id="xtPlatformFeedback_'+numbers+'_content" name="xtPlatformFeedback[][content]"  placeholder="请输入"></textarea>'+
				'</div>'+
			'</div>'+
			'<div class="form-group m-form__group row">'+
				'<label class="col-md-1 col-form-label">状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态</label>'+
				'<div class="col-md-3">'+
					'<input class="form-control" maxlength="10" value="0"  data-bv-numeric data-bv-numeric-message="状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态为数字类型"  id="xtPlatformFeedback_'+numbers+'_status" name="xtPlatformFeedback[][status]" placeholder="请输入">'+
				'</div>'+
			'</div>'+
			'<div class="form-group m-form__group row">'+
				'<label class="col-md-1 col-form-label">创&nbsp;建&nbsp;&nbsp;人</label>'+
				'<div class="col-md-3">'+
					'<input class="form-control" type="text" maxlength="32"  id="xtPlatformFeedback_'+numbers+'_create_id" name="xtPlatformFeedback[][create_id]"  placeholder="请输入">'+
				'</div>'+
			'</div>'+
			'<div class="form-group m-form__group row" style="display:none;">'+
				'<label class="col-md-1 col-form-label">修&nbsp;改&nbsp;&nbsp;人</label>'+
				'<div class="col-md-3">'+
					'<input class="form-control" type="hidden" id="xtPlatformFeedback_'+numbers+'_update_id" name="xtPlatformFeedback[][update_id]"  placeholder="请输入">'+
				'</div>'+
			'</div>'+
			'<div class="form-group m-form__group row" style="display:none;">'+
				'<label class="col-md-1 col-form-label">删除标记</label>'+
				'<div class="col-md-3">'+
					'<input class="form-control" type="hidden" id="xtPlatformFeedback_'+numbers+'_del_flag" name="xtPlatformFeedback[][del_flag]"  placeholder="请输入">'+
				'</div>'+
			'</div>'+
			'<div class="form-group m-form__group row" style="display:none;">'+
				'<label class="col-md-1 col-form-label">创建时间</label>'+
				'<div class="col-md-3">'+
					'<input class="form-control" type="hidden" id="xtPlatformFeedback_'+numbers+'_create_time" name="xtPlatformFeedback[][create_time]"  placeholder="请输入">'+
				'</div>'+
			'</div>'+
			'<div class="form-group m-form__group row" style="display:none;">'+
				'<label class="col-md-1 col-form-label">修改时间</label>'+
				'<div class="col-md-3">'+
					'<input class="form-control" type="hidden" id="xtPlatformFeedback_'+numbers+'_update_time" name="xtPlatformFeedback[][update_time]"  placeholder="请输入">'+
				'</div>'+
			'</div>'+
				'</fieldset>'+
		'</div>'
	$(".form_xtPlatformFeedback").append(form);
	datetimeInit();
	reValidator('defaultForm');
}

/**
 *
 * @param thiz
 * @param numbers
 */
function delXtPlatformFeedbackItems(thiz,numbers){
	validatorDestroy('defaultForm');
	$("#form_xtPlatformFeedback_"+numbers).remove();
	var xtPlatformFeedback_removed_flag = $('#xtPlatformFeedback_removed_flag').val()
	if(null == xtPlatformFeedback_removed_flag || '' == xtPlatformFeedback_removed_flag){
		$('#xtPlatformFeedback_removed_flag').val(','+numbers+',');
	}else{
		$('#xtPlatformFeedback_removed_flag').val(xtPlatformFeedback_removed_flag+numbers+',')
	}
	reValidator('defaultForm');
}
