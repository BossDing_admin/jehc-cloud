//返回
function goback(){
    tlocation(base_html_redirect+'/sys/xt-notice/xt-notice-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function updateXtNotice(){
	var systemUID = $('#xt_userinfo_id').val();
	submitBForm('defaultForm',sysModules+'/xtNotice/update?systemUID='+systemUID,base_html_redirect+'/sys/xt-notice/xt-notice-list.html',null,"PUT");
}

/**初始化附件右键菜单开始 参数4为1表示拥有上传和删除功能 即新增和编辑页面使用**/
initBFileRight('xt_attachment_id','xt_attachment_id_pic',1);
initBFileRight('xt_attachment_id_','xt_attachment_id__pic',1);
/**初始化附件右键菜单结束**/

$(document).ready(function(){
    var xt_notice_id = GetQueryString("xt_notice_id");
    //加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtNotice/get/"+xt_notice_id,{},function(result){
        $("#xt_notice_id").val(result.data.xt_notice_id);
        $("#xt_title").val(result.data.xt_title);
        $("#xt_content").val(result.data.xt_content);
        $("#xt_state").val(result.data.xt_state);
        $("#xt_attachment_id").val(result.data.xt_attachment_id);
        $("#xt_attachment_id_").val(result.data.xt_attachment_id_);
        /**配置附件回显方法开始**/
        var params = {xt_attachment_id:$('#xt_attachment_id').val()+','+$('#xt_attachment_id_').val(),field_name:'xt_attachment_id,xt_attachment_id_'};
        ajaxBFilePathBackRequest(fileModules+'/attAchmentPathpp',params);
        /**配置附件回显方法结束**/
    });
});
