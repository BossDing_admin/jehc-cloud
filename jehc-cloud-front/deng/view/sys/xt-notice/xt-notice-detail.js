//返回
function goback(){
    tlocation(base_html_redirect+'/sys/xt-notice/xt-notice-list.html');
}

/**初始化附件右键菜单开始 参数4为1表示不拥有上传和删除功能 即明细页面使用**/
initBFileRight('xt_attachment_id','xt_attachment_id_pic',2);
initBFileRight('xt_attachment_id_','xt_attachment_id__pic',2);
/**初始化附件右键菜单结束**/

$(document).ready(function(){
    var xt_notice_id = GetQueryString("xt_notice_id");
    //加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtNotice/get/"+xt_notice_id,{},function(result){

        $("#xt_notice_id").val(result.data.xt_notice_id);
        $("#xt_title").val(result.data.xt_title);
        $("#xt_content").val(result.data.xt_content);
        $("#xt_state").val(result.data.xt_state);
        $("#xt_userinfo_id").append(result.data.xt_userinfo_realName);
        $("#xt_createTime").append(result.data.xt_createTime);
        $("#xt_attachment_id").val(result.data.xt_attachment_id);
        $("#xt_attachment_id_").val(result.data.xt_attachment_id_);

        /**配置附件回显方法开始**/
        var params = {xt_attachment_id:$('#xt_attachment_id').val()+','+$('#xt_attachment_id_').val(),field_name:'xt_attachment_id,xt_attachment_id_'};
        ajaxBFilePathBackRequest(fileModules+'/attAchmentPathpp',params);
        /**配置附件回显方法结束**/
    });
})


