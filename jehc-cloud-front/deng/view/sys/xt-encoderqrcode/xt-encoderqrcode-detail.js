//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-encoderqrcode/xt-encoderqrcode-list.html');
}
$(document).ready(function(){
	var xt_encoderqrcode_id = GetQueryString("xt_encoderqrcode_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtEncoderqrcode/get/"+xt_encoderqrcode_id,{},function(result){
        $("#xt_encoderqrcode_id").val(result.data.xt_encoderqrcode_id);
        $("#title").val(result.data.title);
        $("#url").val(result.data.url);
        $("#xt_attachment_id").val(result.data.xt_attachment_id);
        $("#content").val(result.data.content);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
        $('#xt_attachment_id_pic').attr("src",result.data.qrImage);
    });
});