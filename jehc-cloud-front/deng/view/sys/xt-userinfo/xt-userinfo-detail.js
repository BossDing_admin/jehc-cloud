//返回r
function goback(){
	tlocation(base_html_redirect+'/sys/xt-userinfo/xt-userinfo-list.html');
}


//初始化日期选择器
$(document).ready(function(){
	var xt_userinfo_id = GetQueryString("xt_userinfo_id");
	//加载表单数据
	ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+xt_userinfo_id,{},function(result){
	      $("#xt_userinfo_name").append(result.data.xt_userinfo_name);
	      $("#xt_departinfo_id").val(result.data.xt_departinfo_id);
	      $("#xt_departinfo_name").val(result.data.xt_departinfo_name);
	      $("#xt_post_id").val(result.data.xt_post_id);
	      $("#xt_post_name").val(result.data.xt_post_name);
	      $("#xt_userinfo_name_").val(result.data.xt_userinfo_name);
	      $("#xt_userinfo_name").val(result.data.xt_userinfo_name);
	      $("#xt_userinfo_sex_").val(result.data.xt_userinfo_sex);
	      $("#xt_userinfo_birthday").val(result.data.xt_userinfo_birthday);
	      $("#xt_userinfo_realName").val(result.data.xt_userinfo_realName);
	      $("#xt_userinfo_ismarried_").val(result.data.xt_userinfo_ismarried);
	      $("#xt_userinfo_card").val(result.data.xt_userinfo_card);
	      $("#xt_userinfo_nation_").val(result.data.xt_userinfo_nation);
	      $("#xt_userinfo_origo").val(result.data.xt_userinfo_origo);
	      $("#xt_userinfo_schoolName").val(result.data.xt_userinfo_schoolName);
	      $("#xt_userinfo_phone").val(result.data.xt_userinfo_phone);
	      $("#xt_userinfo_mobile").val(result.data.xt_userinfo_mobile);
	      $("#xt_userinfo_ortherTel").val(result.data.xt_userinfo_ortherTel);
	      $("#xt_userinfo_remark").val(result.data.xt_userinfo_remark);
	      $("#xt_userinfo_address").val(result.data.xt_userinfo_address);
	      $("#xt_userinfo_intime").val(result.data.xt_userinfo_intime);
	      $("#xt_userinfo_outTime").val(result.data.xt_userinfo_outTime);
	      $("#xt_userinfo_contractTime").val(result.data.xt_userinfo_contractTime);
	      $("#xt_userinfo_qq").val(result.data.xt_userinfo_qq);
	      $("#xt_userinfo_email").val(result.data.xt_userinfo_email);
	      $("#xt_userinfo_state_").val(result.data.xt_userinfo_state);
	      $("#xt_userinfo_politicalStatus").val(result.data.xt_userinfo_politicalStatus);
	      $("#xt_userinfo_highestDegree_").val(result.data.xt_userinfo_highestDegree);
	      $("#xt_userinfo_workYear_").val(result.data.xt_userinfo_workYear);
	      $('#xt_userinfo_id').val(result.data.xt_userinfo_id)
	});
	datetimeInit();
	InitBDataComboSetV('gender','xt_userinfo_sex','xt_userinfo_sex_');//读取性别数据字典
	InitBDataComboSetV('xt_userinfo_nation','xt_userinfo_nation','xt_userinfo_nation_');//读取民族数据字典
	InitBDataComboSetV('xt_userinfo_highestDegree','xt_userinfo_highestDegree','xt_userinfo_highestDegree_');//读取文化程度数据字典
	InitBDataComboSetV('xt_userinfo_workYear','xt_userinfo_workYear','xt_userinfo_workYear_');//读取工作年限数据字典
	InitBDataComboSetV('xt_userinfo_state','xt_userinfo_state','xt_userinfo_state_');//读取状态数据字典
	InitBDataComboSetV('xt_userinfo_ismarried','xt_userinfo_ismarried','xt_userinfo_ismarried_');//读取是否已婚数据字典
});
