//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-concordat/xt-concordat-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function updateXtConcordat(){
	submitBForm('defaultForm',sysModules+'/xtConcordat/update',base_html_redirect+'/sys/xt-concordat/xt-concordat-list.html',null,"PUT");
}

$(document).ready(function(){
	var xt_concordat_id = GetQueryString("xt_concordat_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtConcordat/get/"+xt_concordat_id,{},function(result){
        $("#xt_concordat_id").val(result.data.xt_concordat_id);
        $("#name").val(result.data.name);
        $("#content").val(result.data.content);
    });
});