//返回r
function goback(){
	tlocation(base_html_redirect+'/job/job-log/job-log-list.html');
}


$(document).ready(function(){
	var job_log_id = GetQueryString("job_log_id");
	//加载表单数据
    ajaxBRequestCallFn(jobModules+"/jobLog/get/"+job_log_id,{},function(result){
        $("#job_log_name").val(result.data.job_log_name);
        $("#job_log_key").val(result.data.job_log_key);
        $("#job_log_content").val(result.data.job_log_content);
        $("#job_log_ctime").val(result.data.job_log_ctime);
        $("#job_log_etime").val(result.data.job_log_etime);
        $("#flag").val(result.data.flag);
    });
});
