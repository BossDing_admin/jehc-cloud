var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,ompModules+'/omp/redisConfig/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"id"
            },
            {
                data:"id",
                render:function(data, type, row, meta) {
                    var host  = row.host;
                    var btn = '<button onclick=toOPMRedisConfigDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情">' +
                        '<i class="fa flaticon-eye"></i>' +
                        '</button>'
                    return btn+'<button onclick=initRedisInfo("'+data+'","'+host+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="监控信息">' +
                        '<i class="fa flaticon-graph"></i>' +
                        '</button>';
                }
            },
            {
                data:'name'
            },
            {
                data:'host',
                width:"220px"
            },
            {
                data:'port'
            },
            {
                data:'password'
            },
            {
                data:'runStatus',
                render:function(data, type, row, meta) {
                    if(data == true){
                        return "<span class='m-badge m-badge--info'>运行中</span>";
                    }
                    return "<span class='m-badge m-badge--danger'>中断</span>";
                }
            },
            {
                data:'redis_version'
            },
            {
                data:'connected_clients'
            },
            {
                data:'os'
            },
            {
                data:'redis_mode'
            },
            {
                data:"createBy",
                render:function(data, type, row, meta) {
                    if(null == data || data == ''){
                        return "<font color='#6495ed'>\</font>";
                    }
                    return data;
                }
            },
            {
                data:"create_time",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:"modifiedBy",
                render:function(data, type, row, meta) {
                    if(null == data || data == ''){
                        return "<font color='#6495ed'>\</font>";
                    }
                    return data;
                }
            },
            {
                data:"update_time",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            }
        ],
        fixedColumns:{
            leftColumns:7
        }
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
    //定时拉表格数据
    setInterval(function (args) {
        search('datatables');
    }, 50000);
});

//新增
function toOPMRedisConfigAdd(){
    tlocation(base_html_redirect+'/omp/rds/omp-redis-config/omp-redis-config-add.html');
}

//修改
function toOPMRedisConfigUpdate(){
    if($(".checkchild:checked").length != 1){
        toastrBoot(4,"选择数据非法");
        return;
    }
    var id = $(".checkchild:checked").val();
    tlocation(base_html_redirect+'/omp/rds/omp-redis-config/omp-redis-config-update.html?id='+id);
}

//详情
function toOPMRedisConfigDetail(id){
    tlocation(base_html_redirect+'/omp/rds/omp-redis-config/omp-redis-config-detail.html?id='+id);
}

//删除
function delOPMRedisConfig(){
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要删除的数据");
        return;
    }
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {id:id, _method:'DELETE'};
        ajaxBReq(ompModules+'/omp/redisConfig/delete',params,['datatables'],null,"DELETE");
    })
}

/**
 *
 */
function getTableContent(){
    var ids = "";
    var nTrs = grid.fnGetNodes();//fnGetNodes获取表格所有行，nTrs[i]表示第i行tr对象
    for(var i = 0; i < nTrs.length; i++){
        // console.log('[获取数据]' , grid.fnGetData(nTrs[i]));//fnGetData获取一行的数据
        if(null == ids || ids == ""){
            ids = grid.fnGetData(nTrs[i]).id;
        }else{
            ids = ids + "," + grid.fnGetData(nTrs[i]).id;
        }
    }
    check(ids);
}

/**
 * check状态
 */
function check(id) {
    var table = $('#datatables').DataTable();
    table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        var data = this.data();
        // data[rowIdx] = '* ' + data[rowIdx];
        console.log(rowLoop);
        this.data(data);
    });
    // var params = {id:id};
    // ajaxBRequestCallFn(ompModules+'/omp/redisConfig/check',params,function(result){
    //     if(undefined != result && null != result && null != result.data){
    //         var dataList = result.data;
    //         var nTrs = grid.fnGetNodes();//fnGetNodes获取表格所有行，nTrs[i]表示第i行tr对象
    //         for(var i = 0; i < nTrs.length; i++){
    //             var idRes = grid.fnGetData(nTrs[i]).id;
    //             for(var j in dataList){
    //                 if(idRes == dataList[j].id){
    //                     console.log('[data]' , dataList[j].id);//返回check数据
    //                     break;
    //                 }
    //             }
    //         }
    //     }
    // });
}

var configId = "";
//Redis信息监控
function initRedisInfo(id,host){
    $('#redisInfoBody').height(reGetBodyHeight()-128);
    var redisInfoModalCount = 0;
    $('#redisInfoModal').modal({backdrop: 'static', keyboard: false});
    $("#redisInfoModalLabel").html("实时信息 "+host+"");
    $('#redisInfoModal').on("shown.bs.modal",function(){
        var $modal_dialog = $("#redisInfoModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        configId = id;
        if(++redisInfoModalCount == 1){
            initRedisInfoData();
            initMemChart();
            initMemData();
        }
    });
}

/**
 *
 */
function initRedisInfoData() {
    if(configId == null || configId == ""){
        return;
    }
    $.ajax({
        url:ompModules+'/omp/redisMain/info',
        type:"GET",//PUT DELETE POST
        // contentType:"application/json;charset=utf-8",
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        data:{config_id:configId},//新版本写法
        success:function(result){
            // closeWating(null,dialogWating);
            if(complateAuth(result)){
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            $("#version").html(result.data.redis_version);
                            $("#redis_version").html(result.data.redis_version);
                            $("#used_memory_human").html(result.data.used_memory_human);
                            $("#connected_clients").html(result.data.connected_clients);
                            $("#total_commands_processed").html(result.data.total_commands_processed);
                            $("#uptimeInDays").html(result.data.uptime_in_days +" days");
                            $("#uptime_in_days").html(result.data.uptime_in_days +" days");
                            $("#uptime_in_seconds").html(result.data.uptime_in_seconds);
                            $("#max_wait_millis").html(result.data.max_wait_millis);
                            $("#arch_bits").html(result.data.arch_bits);
                            $("#config_file").html(result.data.config_file);
                            $("#hz").html(result.data.hz);
                            $("#lru_clock").html(result.data.lru_clock);
                            $("#os").html(result.data.os);
                            $("#multiplexing_api").html(result.data.multiplexing_api);
                            $("#process_id").html(result.data.process_id);
                            $("#redis_build_id").html(result.data.redis_build_id);
                            $("#redis_git_dirty").html(result.data.redis_git_dirty);
                            $("#redis_git_sha1").html(result.data.redis_git_sha1);
                            $("#redis_mode").html(result.data.redis_mode);
                            $("#run_id").html(result.data.run_id);
                            $("#tcp_port").html(result.data.tcp_port);
                            $("#uptime_in_seconds").html(result.data.uptime_in_seconds);
                        }else{
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            }
        },
        error:function(){
            // closeWating(null,dialogWating);
        }
    })
    initMemData();
}

/**
 * 定时任务扫码
 */
var timer_ = setInterval(initRedisInfoData,5000);


var memchart = null;
function initMemChart() {
    memchart = echarts.init(document.getElementById('mainMem'));
    memchart.setOption({
        title: {
            show:true,    // 是否显示标题组件,（true/false）
            text:'内存情况',   // 主标题文本，支持使用\n换行
            textAlign:'auto',    //整体水平对齐（包括text和subtext）
            textVerticalAlign:'auto',//整体的垂直对齐（包括text和subtext）
            padding:0,    // 标题内边距 写法如[5,10]||[ 5,6, 7, 8] ,
            left:'auto',    // title组件离容器左侧距离，写法如'5'||'5%'
            right:'auto',    //'title组件离容器右侧距离
            top:'auto',    // title组件离容器上侧距离
            bottom:'auto',    // title组件离容器下侧距离
            borderColor: '#fff',     // 标题边框颜色
            borderWidth: 1,    // 边框宽度（默认单位px）
            textStyle: {    // 标题样式
                color: '#ccc',    //字体颜色
                fontStyle: 'normal',    //字体风格
                fontSize: 13,    //字体大小
                fontWeight: 400,    //字体粗细
                fontFamily: 'sans-serif',    //文字字体
                lineHeight: '13',    //字体行高
                align:'center',//文字水平对齐方式（left/right）
                verticalAlign:'middle',//文字垂直对齐方式（top/bottom）
            },
            subtext: '',    // 副标题
            subtextStyle: {    // 副标题样式
                color: '#ccc',
                fontStyle:'normal',
                fontWeight:'normal',
                fontFamily:'sans-serif',
                fontSize:11,
                lineHeight:11,
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            }
        },
        legend: {
            data: ['used_memory', 'used_memory_rss', 'used_memory_lua', 'used_memory_peak']
        },
        /*toolbox: {
            feature: {
                saveAsImage: {}
            }
        },*/
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                data: []
            }
        ],
        yAxis: [
            {
                // name: '单位（MB）',
                type: 'value',
                axisLabel:{
                    color: "#1e1e1e",
                    formatter: labelformatter,
                    inside: false,
                    fontSize:'0.2rem',
                    fontWeight: 'bold'
                }
            }
        ],
        series: [
            {
                name: 'used_memory',
                type: 'line',
                stack: '总量',
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#e7bcf3' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#e7bcf3' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            },
            {
                name: 'used_memory_rss',
                type: 'line',
                stack: '总量',
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: 'rgba(58,132,255, 0.5)' // 0% 处的颜色
                        }, {
                            offset: 1, color: 'rgba(58,132,255, 0)' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            },
            {
                name: 'used_memory_lua',
                type: 'line',
                stack: '总量',
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#FFDB5C' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#FFDB5C' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            },
            {
                name: 'used_memory_peak',
                type: 'line',
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'top'
                    }
                },
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#9FE6B8' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#9FE6B8' // 100% 处的颜色
                        }],
                        global: false // 缺省为 false
                    }
                },
                data: []
            }
        ]
    });
}

/**
 *
 * @param value
 * @returns {string}
 */
function labelformatter(value) {
    return value+" mb"
}
/**
 * 占用内存情况
 */
function initMemData(){
    if(configId == null || configId == ""){
        return;
    }
    // var dialogWating = showWating("正在检索中...");
    $.ajax({
        url:ompModules+'/omp/redisMain/mems',
        type:"POST",//PUT DELETE POST
        // contentType:"application/json;charset=utf-8",
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        data:{config_id:configId},//新版本写法
        success:function(result){
            // closeWating(null,dialogWating);
            if(complateAuth(result)){
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            var option =  memchart.getOption();
                            option.xAxis[0].data = result.data.xdata;
                            option.series[0].data = result.data.ydata0;
                            option.series[1].data = result.data.ydata1;
                            option.series[2].data = result.data.ydata2;
                            option.series[3].data = result.data.ydata3;
                            memchart.setOption(option);
                        }else{
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            }
        },
        error:function(){
            // closeWating(null,dialogWating);
        }
    })
}

/**
 *
 */
function closeWin() {
    configId = "";
    window.clearInterval(timer_);
    search('datatables');
}