
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// action types
export const ROUTER_STORE = "router";

// mutation types
export const SET_ROUTER_STORE = "setRouter";

const state = {
  router: []
};

const getters = {
  getRoutes(state) {
    return state.router;
  },
};

const actions = {
  [ROUTER_STORE](context, payload) {
    context.commit(SET_ROUTER_STORE, payload);
  },
};

const mutations = {
  [SET_ROUTER_STORE](state, router) {
    state.router = router;
  },
};

export default {
  state,
  actions,
  mutations,
  getters
};
