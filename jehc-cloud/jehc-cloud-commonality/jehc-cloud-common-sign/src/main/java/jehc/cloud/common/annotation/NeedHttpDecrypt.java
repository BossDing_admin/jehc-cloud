package jehc.cloud.common.annotation;

import java.lang.annotation.*;

/**
 * @Desc 参数需要"解密"注解
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NeedHttpDecrypt {
    String desc = "参数需要解密注解";
}