package jehc.cloud.common.base;

import java.io.Serializable;
/**
 * @Desc 基类JSON
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class BaseJson  implements Serializable{
	private static final long serialVersionUID = 7063637441613949288L;
	private Boolean success;/**成功标志**/
	private String message;/**提示消息**/
	private String jsonID;/**临时ID**/
	private String jsonValue;/**临时值**/
	private String fileType;/**返回上传文件之后的类型**/
	
	public BaseJson(){}
	
	public BaseJson(Boolean success,String message){
		this.success =success;
		this.message =message;
	}
	
	public BaseJson(String message,String jsonID,String jsonValue,String fileType){
		this.message =message;
		this.jsonID =jsonID;
		this.jsonValue =jsonValue;
		this.fileType =fileType;
	}
	
	
	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getJsonID() {
		return jsonID;
	}
	public void setJsonID(String jsonID) {
		this.jsonID = jsonID;
	}
	public String getJsonValue() {
		return jsonValue;
	}
	public void setJsonValue(String jsonValue) {
		this.jsonValue = jsonValue;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
}
