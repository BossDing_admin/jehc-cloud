package jehc.cloud.common.util.logger;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import jehc.cloud.common.util.GetClientIp;
import lombok.extern.slf4j.Slf4j;
/**
 * @Desc Logback 内容增加ip记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class IPClassicConverter extends ClassicConverter {

    private String cache;

    @Override
    public String convert(ILoggingEvent event) {
        if (cache != null) {
            return cache;
        }
        String result = GetClientIp.getLocalIp();
        cache = result;
        return result;
    }
}
