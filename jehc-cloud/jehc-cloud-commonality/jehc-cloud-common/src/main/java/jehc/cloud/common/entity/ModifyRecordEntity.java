package jehc.cloud.common.entity;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 操作日志记录实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class ModifyRecordEntity extends BaseEntity{
    private String xt_modify_record_id;/**主键**/
    private String xt_modify_record_ctime;/**创建时间**/
    private String xt_modify_record_beforevalue;/**更变前值**/
    private String xt_modify_record_aftervalue;/**变更后值**/
    private String xt_modify_record_modules;/**模块**/
    private String xt_modify_record_field;/**字段**/
    private String xt_userinfo_id;/**创建人**/
    private String business_id;/**业务编号**/
}
