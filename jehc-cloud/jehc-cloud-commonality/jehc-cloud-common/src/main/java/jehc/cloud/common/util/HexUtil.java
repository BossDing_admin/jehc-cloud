package jehc.cloud.common.util;
/**
 * @Desc 进制转换
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class HexUtil {
    /**
     * 在进制表示中的字符集合
     */
    final static char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

    ////////////////////////////////十进制数字转换指定字符串和指定进制数字转换为10进制的数字 开始///////////////////////
    /**
     * 将十进制的数字转换为指定进制的字符串
     * @param hexN 十进制的数字
     * @param hexType  任意进制类型(如2,8,10,16等进制)
     * @return
     */
    public static String convertDecimalismToHexString(long hexN, int hexType) {
        long num = 0;
        if (hexN < 0) {
            num = ((long) 2 * 0x7fffffff) + hexN + 2;
        } else {
            num = hexN;
        }
        char[] buf = new char[32];
        int charPos = 32;
        while ((num / hexType) > 0) {
            buf[--charPos] = digits[(int) (num % hexType)];
            num /= hexType;
        }
        buf[--charPos] = digits[(int) (num % hexType)];
        return new String(buf, charPos, (32 - charPos));
    }

    /**
     * 将其它进制的数字（字符串形式）转换为十进制的数字
     * @param str 其它进制的数字（字符串形式）
     * @param hexType 任意进制类型(如2,8,10,16等进制)
     * @return
     */
    public static long convertToDecimalism(String str, int hexType) {
        char[] buf = new char[str.length()];
        str.getChars(0, str.length(), buf, 0);
        long num = 0;
        for (int i = 0; i < buf.length; i++) {
            for (int j = 0; j < digits.length; j++) {
                if (digits[j] == buf[i]) {
                    num += j * Math.pow(hexType, buf.length - i - 1);
                    break;
                }
            }
        }
        return num;
    }
    ////////////////////////////////十进制数字转换指定字符串和指定进制数字转换为10进制的数字 结束///////////////////////
}
