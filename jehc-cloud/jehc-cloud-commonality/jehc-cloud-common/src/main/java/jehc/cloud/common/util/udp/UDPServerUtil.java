package jehc.cloud.common.util.udp;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.constant.StatusConstant;
import jehc.cloud.common.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * @Desc UDPServer工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class UDPServerUtil {

    /**
     * 接收UDP数据（字符集缺省）
     * @param port
     * @return
     */
    public static BaseResult receive(int port){
        BaseResult baseResult = new BaseResult();
        DatagramSocket ds = null;
        try {
            //新建一个DatagramSocket服务，建立端点。
            ds = new DatagramSocket(port);
            //定义数据包，用于存储数据
            byte [] b = new byte[1024];
            DatagramPacket dp = new DatagramPacket(b,b.length);
            //通过服务的receive方法将收到的数据存到数据包
            ds.receive(dp);
            //通过数据包的方法获取其中的数据
            String ip = dp.getAddress().getHostAddress();
            String data = new String(dp.getData(),0,dp.getLength());
            log.info("数据源来源ip:"+ip+"："+data);
            baseResult.setData(data);
        }catch (Exception e){
            log.info("接收出现异常,{}",e);
            baseResult.setMessage("接收UDP异常");
            baseResult.setStatus(StatusConstant.XT_PT_STATUS_VAL_500);
        }finally {
            if(null != ds){
                ds.close();//关闭SOCKET
            }
        }
        return baseResult;
    }


    /**
     * 接收UDP数据（字符集指定）
     * @param port 对外暴露接口
     * @param charset 编码格式：如GBK或UTF-8或ASCII
     * @return
     */
    public static BaseResult receive(int port,String charset){
        BaseResult baseResult = new BaseResult();
        DatagramSocket ds = null;
        try {
            if(StringUtil.isEmpty(charset)){
                charset = "UTF-8";
            }
            //新建一个DatagramSocket服务，建立端点。
            ds = new DatagramSocket(port);
            //定义数据包，用于存储数据
            byte [] b = new byte[1024];
            DatagramPacket dp = new DatagramPacket(b,b.length);
            //通过服务的receive方法将收到的数据存到数据包
            ds.receive(dp);
            //通过数据包的方法获取其中的数据
            String ip = dp.getAddress().getHostAddress();
            String data = new String(dp.getData(),0,dp.getLength(),charset);
            log.info("数据源来源ip:"+ip+"："+data);
            baseResult.setData(data);
        }catch (Exception e){
            baseResult.setMessage("接收UDP异常");
            baseResult.setStatus(StatusConstant.XT_PT_STATUS_VAL_500);
            log.info("接收出现异常,{}",e);
        }finally {
            if(null != ds){
                ds.close();//关闭SOCKET
            }
        }
        return baseResult;
    }

//    /**
//     * 测试
//     * @param args
//     * @throws Exception
//     */
//    public static void main(String[] args) throws Exception {
//        log.info("接收数据---");
//        BaseResult baseResult = UDPServerUtil.receive(10000);
//        log.info("baseResult:{0}",baseResult);
//    }
}
