package jehc.cloud.common.entity;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 附件实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class AttachmentEntity  extends BaseEntity {
    private String xt_attachment_id;/**附件编号**/
    private String xt_attachmentType;/**件文类型**/
    private String xt_attachmentCtime;/**文件上传时间**/
    private String xt_attachmentSize;/**件文大小**/
    private String xt_attachmentPath;/**文件相对路径**/
    private int xt_attachmentIsDelete;/**0正常1删除**/
    private String xt_attachmentName;/**附件名称**/
    private String xt_userinfo_id;/**传上者编号**/
    private String xt_modules_id;/**传上模块编号**/
    private int xt_modules_order;/**顺序**/
    private String xt_attachmentTitle;/**附件原名称**/
    private String xt_path_absolutek;/**平台路径配置中心键（自定义上传绝对路径使用）**/
    private String xt_path_relativek;/**平台路径配置中心键（自定义上传相对路径使用）**/
    private String xt_path_urlk;/**平台路径配置中心键（自定义上传路径 自定义URL地址）**/
    private String field_name;/**字段名称**/
    private String fullUrl;/**整个路径如http://www.jehc.com/images/img.png**/
}
