package jehc.cloud.common.base;
import cn.hutool.core.collection.CollectionUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc TreeGrid封装树（Extjs风格）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseTreeGridEntity {
	private String id;/**ID编号**/
	private String pid;/**父级编号**/
	private String text;/**显示值**/
	private String icon;/**图片**/
	private String qtip;/**提示**/
	private Boolean leaf;/**叶子**/
	private Boolean expanded = true;/**是否展开默认展开**/
    private Boolean checked = false;/**是否支持全选**/
    private Boolean singleClickExpand = false;/**单击展开默认否false为双击节点展开 true单击节点展开**/
	private String tempObject;/**临时值**/
	private String content;/**描述**/
	private String integerappend;/**int类型拼接使用可以使用@或逗号隔开**/
    private List<BaseTreeGridEntity> children;/**子节点集合**/

	/**
	 *
	 * @param list
	 * @param parent
	 * @return
	 */
	public List<BaseTreeGridEntity> buildTree(List<BaseTreeGridEntity> list, String parent){
		List<BaseTreeGridEntity> baseTreeGridEntityList =new ArrayList<BaseTreeGridEntity>();
		for(BaseTreeGridEntity baseTreeGridEntity : list) {
			String nodeId = baseTreeGridEntity.getId();
			String pid = baseTreeGridEntity.getPid();
			if (parent.equals(pid)) {
				List<BaseTreeGridEntity> baseTreeEntities = buildTree(list, nodeId);
				if(!CollectionUtil.isEmpty(baseTreeEntities)){
					if(null == baseTreeGridEntity.getChildren()){
						children = new ArrayList<>();
						baseTreeGridEntity.setLeaf(true);
						children.addAll(baseTreeEntities);
						baseTreeGridEntity.setChildren(children);
					}

				}
				baseTreeGridEntityList.add(baseTreeGridEntity);
			}
		}
		return baseTreeGridEntityList;
	}
//	  public static String buildTree(List<BaseTreeGridEntity> list,boolean isHasChecbox){
//    	StringBuffer html = new StringBuffer();
//    	html.append("[");
//        for(int i = 0; i < list.size(); i++) {
//        	BaseTreeGridEntity node = list.get(i);
//            if ("0".equals(node.getPid())) {
//            	if(isHasChecbox){
//            		html.append("{text:'" + node.getText() + "',id:'" + node.getId()+ "',checked:"+node.getChecked()+",singleClickExpand:"+node.getSingleClickExpand()+",pid:'"+node.getPid()+"',leaf:"+isLeaf(list,node)+",icon:'"+node.getIcon()+"',qtip:'"+node.getText()+"',expanded:"+node.getExpanded()+",tempObject:'"+node.getTempObject()+"',content:'"+node.getContent()+"',integerappend:'"+node.getIntegerappend()+"'");
//            	}else{
//            		html.append("{text:'" + node.getText() + "',id:'" + node.getId()+ "',singleClickExpand:"+node.getSingleClickExpand()+",pid:'"+node.getPid()+"',leaf:"+isLeaf(list,node)+",icon:'"+node.getIcon()+"',qtip:'"+node.getText()+"',expanded:"+node.getExpanded()+",tempObject:'"+node.getTempObject()+"',content:'"+node.getContent()+"',integerappend:'"+node.getIntegerappend()+"'");
//            	}
//            	html.append(build(list,node,isHasChecbox));
//            	html.append("},");
//            }
//        }
//        html.append("]");
//        String jsonStr = html.toString().replaceAll("},]", "}]");
//        return jsonStr;
//    }
//
//
//	/**
//	 *
//	 * @param list
//	 * @param node
//	 * @param isHasChecbox
//	 * @return
//	 */
//	private static String build(List<BaseTreeGridEntity> list,BaseTreeGridEntity node,boolean isHasChecbox){
//    	StringBuffer html = new StringBuffer();
//    	List<BaseTreeGridEntity> children = getChildren(list,node,isHasChecbox);
//        if (!children.isEmpty() && children.size()>0) {
//        	html.append(",children:[");
//        	for(int i = 0; i < children.size(); i++){
//        		BaseTreeGridEntity child = children.get(i);
//        		if(isHasChecbox){
//        			//如果当前节点下面不存在节点则设置leaf为true 否则设置false
//        			html.append("{text:'" + child.getText() + "',id:'" + child.getId()+ "',checked:"+child.getChecked()+",singleClickExpand:"+child.getSingleClickExpand()+",pid:'"+child.getPid()+"',leaf:"+isLeaf(list,child)+",icon:'"+child.getIcon()+"',qtip:'"+child.getText()+"',expanded:"+child.getExpanded()+",tempObject:'"+child.getTempObject()+"',content:'"+child.getContent()+"',integerappend:'"+child.getIntegerappend()+"'");
//        		}else{
//        			//如果当前节点下面不存在节点则设置leaf为true 否则设置false
//        			html.append("{text:'" + child.getText() + "',id:'" + child.getId()+ "',singleClickExpand:"+child.getSingleClickExpand()+",pid:'"+child.getPid()+"',leaf:"+isLeaf(list,child)+",icon:'"+child.getIcon()+"',qtip:'"+child.getText()+"',expanded:"+child.getExpanded()+",tempObject:'"+child.getTempObject()+"',content:'"+child.getContent()+"',integerappend:'"+child.getIntegerappend()+"'");
//        		}
//            	html.append(build(list,child,isHasChecbox));
//                if(i < children.size()-1){
//            		html.append("},");
//            	}else{
//            		html.append("}");
//            	}
//            }
//            html.append("]");
//        }
//        return html.toString();
//    }
//
//	/**
//	 * 获取子节点
//	 * @param list
//	 * @param node
//	 * @param isHasChecbox
//	 * @return
//	 */
//	private static List<BaseTreeGridEntity> getChildren(List<BaseTreeGridEntity> list,BaseTreeGridEntity node,boolean isHasChecbox){
//        List<BaseTreeGridEntity> children = new ArrayList<BaseTreeGridEntity>();
//        for(BaseTreeGridEntity child : list) {
//        	if (node.getId().equals(child.getPid())) {
//                children.add(child);
//            }
//        }
//        return children;
//    }
//
//    /**
//     * 判断是否存在子叶子
//     * @param list
//     * @param node
//     * @return
//     */
//    public static String isLeaf(List<BaseTreeGridEntity> list,BaseTreeGridEntity node){
//    	for(BaseTreeGridEntity child : list) {
//        	if (node.getId().equals(child.getPid())) {
//        		return "false";
//            }
//        }
//    	return "true";
//    }
//	public String getContent() {
//		return content;
//	}
//	public void setContent(String content) {
//		this.content = content;
//	}
//	public String getIntegerappend() {
//		return integerappend;
//	}
//	public void setIntegerappend(String integerappend) {
//		this.integerappend = integerappend;
//	}
}
