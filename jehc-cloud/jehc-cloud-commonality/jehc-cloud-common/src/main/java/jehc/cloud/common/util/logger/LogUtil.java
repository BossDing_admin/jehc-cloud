package jehc.cloud.common.util.logger;

import org.apache.log4j.Logger;
/**
 * @Desc log4j日志工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class LogUtil{
	private Logger logger = Logger.getLogger(this.getClass());
	public LogUtil() {
	}
	public void debug(String className, String msg) {
		logger.debug(className + " - " + msg);
	}

	public void info(String className, String msg) {
		logger.info(className + " - " + msg);
	}

	public void warn(String className, String msg) {
		logger.warn(className + " - " + msg);
	}

	public void error(String className, String msg) {
		logger.error(className + " - " + msg);
	}

	public void fatal(String className, String msg) {
		logger.fatal(className + " - " + msg);
	}
}
