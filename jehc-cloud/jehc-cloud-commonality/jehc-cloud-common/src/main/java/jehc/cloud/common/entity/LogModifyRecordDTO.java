package jehc.cloud.common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* @Desc 修改记录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:24:23
*/
@Data
public class LogModifyRecordDTO extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String before_value;/**更变前值**/
	private String after_value;/**变更后值**/
	private String modules;/**模块**/
	private String field;/**字段**/
	private String business_id;/**业务编号**/
	private String create_id;/**创建人id**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	private String update_id;/**修改人id**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
	private Integer del_flag;/**删除标记：0正常1已删除**/
	private String batch;/**批次**/
}
