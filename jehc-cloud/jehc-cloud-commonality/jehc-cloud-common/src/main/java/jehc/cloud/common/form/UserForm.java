package jehc.cloud.common.form;

import lombok.Data;

import java.util.List;

/**
 * @Desc 员工信息查询参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class UserForm {

    private List<String> userInfoIdList;//用户id

    private List<String>  postIdList;//岗位id

    private List<String>  departinfoIdList;//部门id
}
