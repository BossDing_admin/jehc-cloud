package jehc.cloud.common.threadlocalutil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @Desc 监听BaseController中ThreadLocal变量并销毁
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class ServletRequestListener implements ServletContextListener{

	public void contextDestroyed(ServletContextEvent arg0) {
		/**
		BaseAction.closeThreadLocal();
		ThreadLocalUtil.destroy();
		**/
	}

	public void contextInitialized(ServletContextEvent arg0) {
	}
}
