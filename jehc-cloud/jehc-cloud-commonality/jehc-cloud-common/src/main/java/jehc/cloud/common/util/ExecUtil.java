package jehc.cloud.common.util;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @Desc exec工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class ExecUtil {

    /**
     *
     * @param command
     */
    public static int exec(String command) {
        try {
            Process process = Runtime.getRuntime().exec(command);
            InputStream instream = process.getInputStream();
            BufferedReader bufferReader = new BufferedReader(new InputStreamReader(instream, "GBK"));
            String readline;
            List results = new ArrayList();
            while ((readline = bufferReader.readLine()) != null) {
                results.add(readline);
                log.info("execute command result : " + readline);
            }
            int status = process.waitFor();
            return status;
        }catch (Exception e){
            log.error("execute command error:{}",e);
        }
        return -1;
    }
}
