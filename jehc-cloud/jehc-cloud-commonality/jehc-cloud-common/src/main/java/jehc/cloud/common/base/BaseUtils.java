package jehc.cloud.common.base;

import java.util.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import cn.hutool.core.collection.CollectionUtil;
import jehc.cloud.common.form.UserForm;
import jehc.cloud.common.idgeneration.UUID;
import jehc.cloud.common.cache.redis.RedisUtil;
import jehc.cloud.common.entity.*;
import jehc.cloud.common.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @Desc 公用类获取平台信息方法如获取登录用户信息,当前用户部门,当前用户岗位等一系列信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Slf4j
@Primary
public class BaseUtils extends UUID {

	@Autowired
	RestTemplateUtil restTemplateUtil;

	@Autowired
	RedisUtil redisUtil;

	/**
	 * 当前登录者姓名
	 * @return
	 */
	public String getXtUname() {
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/uName",BaseResult.class,request);
			if(null == baseResult){
				return null;
			}
			return ""+baseResult;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 当前登录账户
	 * @return
	 */
	public String getXtUlname() {
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/account",BaseResult.class,request);
			if(null == baseResult){
				return null;
			}
			return ""+baseResult.getData();
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 当前账号编号
	 * @return
	 */
	public String getXtUid() {
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/accountId",BaseResult.class,request);
			if(null == baseResult){
				return null;
			}
			return ""+baseResult.getData();
		} catch (Exception e) {
			log.error("获取系统用户id出现异常：{0}",e);
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 当前账号所在部门编号
	 * @return
	 */
	public String getUdId() {
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/accountId",BaseResult.class,request);
			if(null == baseResult){
				return null;
			}
			return ""+baseResult.getData();
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 获取当前用户对象信息
	 *
	 * @return
	 */
	public BaseHttpSessionEntity getBaseHttpSessionEntity() {
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			BaseHttpSessionEntity baseHttpSessionEntity = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/httpSessionEntity",BaseHttpSessionEntity.class,request);
			return baseHttpSessionEntity;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 当前登录者信息
	 * @return
	 */
	public OauthAccountEntity getXtU() {
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			OauthAccountEntity oauthAccountEntity = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/accountInfo",OauthAccountEntity.class,request);
			return oauthAccountEntity;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}


	/**
	 * 获取数据权限
	 * @return
	 */
	public List<String> systemUandM(){
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			List<String> list = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/systemUandM",List.class,request);
			return list;
		} catch (Exception e) {
			throw new ExceptionUtil("获取systemUandM出现异常："+e.getMessage());
		}
	}

	/**
	 * 判断当前用户是否为超级管理员
	 *
	 * @return
	 */
	public boolean isAdmin() {
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			Boolean aBoolean = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/isAdmin",Boolean.class,request);
			return aBoolean;
		} catch (Exception e) {
			throw new ExceptionUtil("获取systemUandM出现异常："+e.getMessage());
		}
	}

	/**
	 * 获取缓存值
	 *
	 * @return
	 */
	public static Object getCache(String key) {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
		ServletContext sc = request.getSession(false).getServletContext();
//		Map<String, Object> map = (Map<String, Object>) sc.getAttribute("sys_message");
//		return map.get(key);
		return (String)sc.getAttribute(key);
	}

	/**
	 * 获取缓存值
	 *
	 * @return
	 */
	public static String getCacheStr(String key) {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
		ServletContext sc = request.getSession().getServletContext();
//		Map<String, Object> map = (Map<String, Object>) sc.getAttribute("sys_message");
//		return (String) map.get(key);
		return (String)sc.getAttribute(key);
	}

	/**
	 * 根据KEY获取平台路径
	 *
	 * @param key
	 * @return
	 */
	public List<PathEntity> getXtPathCache(String key) {
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			BaseResult baseResult= restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/path/list?key="+key,BaseResult.class,request);
			List<PathEntity> list =JsonUtil.toList(""+baseResult.getData(),PathEntity.class);
			return list;
		} catch (Exception e) {
			throw new ExceptionUtil("获取PathEntity出现异常："+e.getMessage());
		}
	}

	/**
	 * 统一验证错误 通过注解捕捉字段验证错误信息
	 * @param bindingResult
	 * @return
	 */
	public String backFem(BindingResult bindingResult){
		List<FieldError> fieldErrorList = bindingResult.getFieldErrors();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < fieldErrorList.size(); i++) {
			FieldError fieldError =fieldErrorList.get(i);
			sb.append("错误字段消息："+fieldError.getField() +" : "+fieldError.getDefaultMessage()+"<br>");
		}
		return sb.toString();
	}

	/**
	 * 判断当前请求是否异步
	 * @param request
	 * @return
	 */
	public static boolean isAjaxReq(HttpServletRequest request){
		String head = request.getHeader("x-requested-with");
		//XMLHttpRequest为异步 Ext.basex为同步 则Ajax拦截
		if((null != head && (head.equalsIgnoreCase("XMLHttpRequest")|| "Ext.basex".equalsIgnoreCase(head)))) {
			return true;
		}
		return false;
	}

//	/**
//	 * 根据KEY获取平台常量
//	 * @param key
//	 * @return
//	 */
//	public ConstantEntity getXtConstantCache(String key) {
//		String data = redisUtil.hget(CacheConstant.XTCONSTANTCACHE,CacheConstant.XTCONSTANTCACHE);
//		List<ConstantEntity> xtConstantList = JsonUtil.toFList(data,ConstantEntity.class);
//		for (int i = 0; i < xtConstantList.size(); i++) {
//			ConstantEntity xtConstant = xtConstantList.get(i);
//			if (key.equals(xtConstant.getXt_constantName())) {
//				return xtConstant;
//			}
//		}
//		return null;
//	}
//
//	/**
//	 * 根据key获取行政区域集合
//	 * @param key
//	 * @return
//	 */
//	public List<AreaRegionEntity> getXtAreaRegionCache(String key) {
//		String data = redisUtil.hget(CacheConstant.XTAREAREGIONCACHE,CacheConstant.XTAREAREGIONCACHE);
//		List<AreaRegionEntity> list = JsonUtil.toFList(data,AreaRegionEntity.class);
//		List<AreaRegionEntity> arList = new ArrayList<AreaRegionEntity>();
//		if(StringUtil.isEmpty(key)){
//			for (int i = 0; i < list.size(); i++) {
//				AreaRegionEntity xtAreaRegion = list.get(i);
//				if (xtAreaRegion.getPARENT_ID() ==  1) {
//					arList.add(xtAreaRegion);
//				}
//			}
//		}else{
//			for (int i = 0; i < list.size(); i++) {
//				AreaRegionEntity xtAreaRegion = list.get(i);
//				if (key.equals(""+xtAreaRegion.getPARENT_ID())) {
//					arList.add(xtAreaRegion);
//				}
//			}
//		}
//		return arList;
//	}

	/**
	 * 根据KEY获取平台字典
	 * @param key
	 * @return
	 */
	public List<DataDictionaryEntity> getXtDataDictionaryCache(String key) {
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			List<DataDictionaryEntity> baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/dataDictionary/list/"+key,List.class,request);
			return baseResult;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 判断IP是否为黑户
	 * @param ip
	 * @return
	 */
	public boolean getXtIpFrozenCache(String ip){
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			Boolean baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/ipFrozen/validate/"+ip,Boolean.class,request);
			return baseResult;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 获取公共功能缓存
	 * @return
	 */
	public String getXtFunctioninfoCommonCache(){
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			String baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/functioninfoCommonommon/list",String.class,request);
			if(null == baseResult){
				return null;
			}
			return ""+baseResult;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 随机生成唯一client.id方法
	 * @return
	 */
	public static String getClientId(){
		String nums = "";
		String[] codeChars = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
				"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
		for (int i = 0; i < 23; i++)
		{
			int charNum = (int)Math.floor(Math.random() * codeChars.length);
			nums = nums + codeChars[charNum];
		}
		return nums;
	}

	/**
	 * 获取账户更多信息
	 * @param userParamInfo
	 * @return
	 */
	public UserParamInfo infoList(UserParamInfo userParamInfo) {
		try {
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			BaseResult baseResult= restTemplateUtil.post(restTemplateUtil.restOauthURL() + "/oauthAccount/infoList",BaseResult.class,userParamInfo,request);
			userParamInfo =JsonUtil.fromAliFastJson(""+baseResult.getData(),UserParamInfo.class);
			return userParamInfo;
		} catch (Exception e) {
			throw new ExceptionUtil("获取UserParamInfo出现异常："+e.getMessage());
		}
	}

	/**
	 * 根据账号id查找账户信息
	 * @param accountId
	 * @return
	 */
	public OauthAccountEntity getAccount(String accountId) {
		try {
			if(StringUtil.isEmpty(accountId)){
				return null;
			}
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/oauthAccount/get/"+accountId,BaseResult.class,request);
			if(null == baseResult){
				return null;
			}
			OauthAccountEntity oauthAccount = JsonUtil.fromFastJson(baseResult.getData(),OauthAccountEntity.class);
			return oauthAccount;
		} catch (Exception e) {
			log.error("获取账户异常：{0}",e);
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 根据 Token查找在线用户
	 * @param token
	 * @return
	 */
	public OauthAccountEntity getOnlineAccount(String token) {
		try {
			if(StringUtil.isEmpty(token)){
				return null;
			}
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			BaseHttpSessionEntity baseHttpSessionEntity = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/oauthAccount/tokenInfoByToken?token="+token,BaseHttpSessionEntity.class,request);
			if(null == baseHttpSessionEntity){
				return null;
			}
			return baseHttpSessionEntity.getOauthAccountEntity();
		} catch (Exception e) {
			log.error("获取在线账户异常：{0}",e);
			return null;
		}
	}

	/**
	 * 根据 Token查找在线用户
	 * @param token
	 * @return
	 */
	public OauthAccountEntity getOnlineAccount(String token,HttpServletRequest request) {
		try {
			if(StringUtil.isEmpty(token)){
				return null;
			}
			BaseHttpSessionEntity baseHttpSessionEntity = restTemplateUtil.get(restTemplateUtil.restOauthURL() + "/oauthAccount/tokenInfoByToken?token="+token,BaseHttpSessionEntity.class,request);
			if(null == baseHttpSessionEntity){
				return null;
			}
			return baseHttpSessionEntity.getOauthAccountEntity();
		} catch (Exception e) {
			log.error("获取在线账户异常：{0}",e);
			return null;
		}
	}

	/**
	 * 根据 Token查找在线用户
	 * @param token
	 * @return
	 */
	public OauthAccountEntity getOnlineAccount(String token,HttpHeaders headers) {
		try {
			if(StringUtil.isEmpty(token)){
				return null;
			}
			BaseHttpSessionEntity baseHttpSessionEntity = restTemplateUtil.getUnRequest(restTemplateUtil.restOauthURL() + "/tokenInfoByToken?token="+token,BaseHttpSessionEntity.class,headers);
			if(null == baseHttpSessionEntity){
				return null;
			}
			return baseHttpSessionEntity.getOauthAccountEntity();
		} catch (Exception e) {
			log.error("获取在线账户异常：{0}",e);
			return null;
		}
	}

	/**
	 * 根据 account_id查找在线用户Token
	 * @param account_id
	 * @return
	 */
	public BaseResult getOnlineAccountToken(String account_id,HttpHeaders headers) {
		try {
			if(StringUtil.isEmpty(account_id)){
				return null;
			}
			BaseResult baseResult = restTemplateUtil.getUnRequest(restTemplateUtil.restOauthURL() + "/tokenByAccountId?account_id="+account_id,BaseResult.class,headers);
			return baseResult;
		} catch (Exception e) {
			log.error("获取在线账户Token异常：{0}",e);
			return null;
		}
	}

	/**
	 * 查询行政区域单条记录
	 * @param id
	 * @return
	 */
	public AreaRegionEntity getAreaRegionEntity(String id){
		try {
			if(StringUtil.isEmpty(id)){
				return null;
			}
			RequestAttributes ra = RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtAreaRegion/get/"+id,BaseResult.class,request);
			if(null == baseResult){
				return null;
			}
			return JsonUtil.fromFastJson(baseResult.getData(),AreaRegionEntity.class);
		} catch (Exception e) {
			log.error("查询行政区域单条记录出现异常：{0}",e);
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 根据条件查找用户集合（包含部门编号逗号分隔，岗位编号逗号分隔，用户编号逗号分隔）
	 * @param userForm
	 * @return
	 */
	public List<UserinfoEntity> getUserinfoEntityList(UserForm userForm){
		List<UserinfoEntity> userinfoEntities = null;
		try {
			BaseResult<List<UserinfoEntity>> baseResult = restTemplateUtil.post(restTemplateUtil.restSysURL() + "/xtUserinfo/getUserinfoList",BaseResult.class,userForm);
			if(null == baseResult){
				return userinfoEntities;
			}
			userinfoEntities = JsonUtil.toFastList(baseResult.getData(),UserinfoEntity.class);
			return userinfoEntities;
		} catch (Exception e) {
			log.error("查找用户集合出现异常：{0}",e);
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 创建站内通知
	 * @param baseNotifyEntity
	 * @return
	 */
	public int createNotify(BaseNotifyEntity baseNotifyEntity){
		try {
			BaseResult baseResult = restTemplateUtil.post(restTemplateUtil.restSysURL() + "/xtNotify/createNotify",BaseResult.class,baseNotifyEntity);
			if(null == baseResult){
				return -1;
			}
			return 1;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
