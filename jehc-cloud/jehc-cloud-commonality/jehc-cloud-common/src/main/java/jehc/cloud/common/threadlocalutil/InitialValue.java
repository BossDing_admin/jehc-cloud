package jehc.cloud.common.threadlocalutil;

/**
 * @Desc InitialValue
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public abstract class InitialValue {
    public abstract Object create();
}
