package jehc.cloud.common.base;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Desc 通知基类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseNotifyEntity extends BaseEntity {
    private String notify_id;/**主键**/
    private String title;/**标题**/
    private String content;/**内容**/
    private int type;/**通知类型0默认1平台通知(系统自动通知）**/
    private List<BaseNotifyReceiverEntity> notifyReceivers;
}
