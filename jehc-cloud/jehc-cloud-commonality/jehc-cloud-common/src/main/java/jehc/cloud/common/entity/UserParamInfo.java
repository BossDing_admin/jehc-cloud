package jehc.cloud.common.entity;

import lombok.Data;
import java.util.List;

/**
 * @Desc UserParamInfo
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class UserParamInfo {
    private String account;
    private String account_id;
    private List<UserinfoEntity> userinfoEntities;
    public UserParamInfo(){ }
    public UserParamInfo(String account_id){
        this.account_id = account_id;
    }
}
