package jehc.cloud.common.base;

import java.io.Serializable;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import jehc.cloud.common.util.FastJsonUtils;
import jehc.cloud.common.util.MapUtils;
import jehc.cloud.common.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import net.sf.json.JSONObject;
/**
 * @Desc 基础搜索
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class BaseSearch extends BasePage implements Serializable {
	private static final long serialVersionUID = 5216143475139692132L;
	private String searchJson;
//	private String pageJson;

	private String definedJson;/**自定义参数**/

	public String getSearchJson() {
		return searchJson;
	}

	public void setSearchJson(String searchJson) {
		this.searchJson = searchJson;
	}

	/**
	 *
	 * @return
	 */
	public Map<String, Object> convert(){
		try {
			if(!StringUtil.isEmpty(searchJson)){
				Map<String, Object> map = JSONObject.fromObject(URLDecoder.decode(getSearchJson(), "UTF-8"));
				map = MapUtils.resetMap(map);
				return map;
			}
		} catch (Exception e) {
			super.error("BaseSearch","查询条件参数转换出现异常："+e.getMessage());
			new HashMap<String, Object>();
		}
		return new HashMap<String, Object>();
	}

//	public String getPageJson() {
//		return pageJson;
//	}
//
//	public void setPageJson(String pageJson) {
//		this.pageJson = pageJson;
//	}


	public String getDefinedJson() {
		return definedJson;
	}

	public void setDefinedJson(String definedJson) {
		this.definedJson = definedJson;
	}

	/**
	 * 分页json转对象
	 * @return
	 */
//	public BasePage ConvertToBasePage() {
//		if(!StringUtils.isEmpty(getPageJson())){
//			return FastJsonUtils.toBean(getPageJson(), BasePage.class);
//		}
//		return new BasePage();
//	}

//	/**
//	 * 对象转json
//	 * @return
//	 */
//	public String convertToPageJson(){
//		return FastJsonUtils.toJSONString(new BasePage(getStart(),getPage(), getPageSize(), getTotal(), getData(), getSuccess()));
//	}

	/**
	 * 自定义参数JSON转Map
	 * @return
	 */
	public Map<String,Object> ConvertToMap(){
		if(!StringUtils.isEmpty(getDefinedJson())){
			return FastJsonUtils.toBean(getDefinedJson(), Map.class);
		}
		return new HashMap<String,Object>();
	}
}
