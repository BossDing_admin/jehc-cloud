package jehc.cloud.common.base;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc JsTree 根目录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseJSTree {
    private String text;/**显示值**/
    private String id = "-1";/**id编号**/
    private BaseJSTreeStateEntity state = new BaseJSTreeStateEntity();
    private List<BaseJSTreeEntity> children = new ArrayList<BaseJSTreeEntity>();/**子节点集合**/

    /**
     *
     */
    public BaseJSTree(){

    }

    /**
     *
     * @param text
     * @param children
     */
    public BaseJSTree(String text,List<BaseJSTreeEntity> children){
        this.text = text;
        this.children = children;
    }

    /**
     *
     * @param text
     * @param id
     * @param children
     */
    public BaseJSTree(String text,String id,List<BaseJSTreeEntity> children){
        this.text = text;
        this.id = id;
        this.children = children;
    }

    /**
     *
     * @param text
     * @param children
     * @param state
     */
    public BaseJSTree(String text,List<BaseJSTreeEntity> children,BaseJSTreeStateEntity state){
        this.text = text;
        this.children = children;
        this.state = state;
    }

    /**
     *
     * @param text
     * @param children
     * @param state
     */
    public BaseJSTree(String text,String id,List<BaseJSTreeEntity> children,BaseJSTreeStateEntity state){
        this.text = text;
        this.id = id;
        this.children = children;
        this.state = state;
    }
}
