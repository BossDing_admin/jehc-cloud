package jehc.cloud.common.base;

import cn.hutool.core.collection.CollectionUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Desc 通用树实体类（Extjs风格）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseTreeEntity implements Serializable {
	private static final long serialVersionUID = 7261712903068621559L;
	private String id;/**ID编号**/
	private String pid;/**父级编号**/
	private String text;/**显示值**/
	private String icon;/**图片**/
	private String qtip;/**提示**/
	private Boolean leaf;/**叶子**/
	private Boolean expanded = true;/**是否展开默认展开**/
    private String checked;/**是否支持全选**/
    private Boolean singleClickExpand = false;/**单击展开默认否false为双击节点展开 true单击节点展开**/
	private ArrayList<BaseTreeEntity> children = new ArrayList<BaseTreeEntity>();/**子节点集合**/

	/**
	 *
	 * @param list
	 * @param parent
	 * @return
	 */
	public List<BaseTreeEntity> buildTree(List<BaseTreeEntity> list, String parent){
		List<BaseTreeEntity> baseTreeEntitieList =new ArrayList<BaseTreeEntity>();
		for(BaseTreeEntity baseTreeEntity : list) {
			String nodeId = baseTreeEntity.getId();
			String pid = baseTreeEntity.getPid();
			if (parent.equals(pid)) {
				List<BaseTreeEntity> baseTreeEntities = buildTree(list, nodeId);
				if(!CollectionUtil.isEmpty(baseTreeEntities)){
					if(null == baseTreeEntity.getChildren()){
						children = new ArrayList<>();
						baseTreeEntity.setLeaf(true);
						children.addAll(baseTreeEntities);
						baseTreeEntity.setChildren(children);
					}

				}
				baseTreeEntitieList.add(baseTreeEntity);
			}
		}
		return baseTreeEntitieList;
	}

//
//	public static String buildTree(List<BaseTreeEntity> list){
//    	StringBuffer html = new StringBuffer();
//    	html.append("[");
//        for(int i = 0; i < list.size(); i++) {
//        	BaseTreeEntity node = list.get(i);
//            if ("0".equals(node.getPid())) {
//            	html.append("{text:'" + node.getText() + "',id:'" + node.getId()+ "',checked:"+node.getChecked()+",singleClickExpand:"+node.getSingleClickExpand()+",pid:'"+node.getPid()+"',leaf:"+node.getLeaf()+",icon:'"+node.getIcon()+"',qtip:'"+node.getText()+"',expanded:"+node.getExpanded());
//            	html.append(build(list,node));
//            	html.append("},");
//            }
//        }
//        html.append("]");
//        String jsonStr = html.toString().replaceAll("},]", "}]");
//        return jsonStr;
//    }
//
//	/**
//	 *
//	 * @param list
//	 * @param node
//	 * @return
//	 */
//	private static String build(List<BaseTreeEntity> list,BaseTreeEntity node){
//    	StringBuffer html = new StringBuffer();
//    	List<BaseTreeEntity> children = getChildren(list,node);
//        if (!children.isEmpty()) {
//        	html.append(",children:[");
//        	for(int i = 0; i < children.size(); i++){
//        		BaseTreeEntity child = children.get(i);
//            	html.append("{text:'" + child.getText() + "',id:'" + child.getId()+ "',checked:"+child.getChecked()+",singleClickExpand:"+child.getSingleClickExpand()+",pid:'"+child.getPid()+"',leaf:"+child.getLeaf()+",icon:'"+child.getIcon()+"',qtip:'"+child.getText()+"',expanded:"+child.getExpanded());
//            	html.append(build(list,child));
//                if(i < children.size()-1){
//            		html.append("},");
//            	}else{
//            		html.append("}");
//            	}
//            }
//            html.append("]");
//        }
//        return html.toString();
//    }
//
//	/**
//	 *
//	 * @param list
//	 * @param node
//	 * @return
//	 */
//	private static List<BaseTreeEntity> getChildren(List<BaseTreeEntity> list,BaseTreeEntity node){
//        List<BaseTreeEntity> children = new ArrayList<BaseTreeEntity>();
//        for(BaseTreeEntity child : list) {
//            if (node.getId().equals(child.getPid())) {
//                children.add(child);
//            }
//        }
//        return children;
//    }
}
