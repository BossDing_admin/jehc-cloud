package jehc.cloud.common.base;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import jehc.cloud.common.base.binder.DoubleEditor;
import jehc.cloud.common.base.binder.FloatEditor;
import jehc.cloud.common.base.binder.IntegerEditor;
import jehc.cloud.common.base.binder.LongEditor;
import jehc.cloud.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import net.sf.json.JSONArray;


/**
 * @Desc 控制层基类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@CrossOrigin(origins = "*", maxAge = 3600)
public class BaseAction extends BaseUtils {

	@Autowired
	public BaseUtils baseUtils;

	/**
	 * 封装共同的数据权限获取参数
	 * @param condition
	 */
	public void dataAuthForXtUID(HttpServletRequest request,String key,Map<String, Object> condition){
		List<String> sysUID = (List<String>)request.getAttribute("sysUID");
		if(null != sysUID && !sysUID.isEmpty() && sysUID.size() > 0){
			condition.put(key,sysUID);
		}else{
			condition.put(key,new String[]{"0"});
		}
	}
	/**
	 * 获取Request对象
	 * @return
	 */
	protected static HttpServletRequest getReq(){
		return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
	}

	/**
	 * 采用PageHelper分页方式一
	 * 输出带分页的JSON格式字符串
	 * @param list
	 */
	protected BasePage outPageStr(List list,Long total,BaseSearch baseSearch,HttpServletRequest request){
		if(baseSearch.getUsePageNo() == false){
			Integer start = baseSearch.getStart();
			Integer pageSize = baseSearch.getPageSize();
			return new BasePage(start,pageSize,total,list,true);
		}else{
			Integer pageNo = baseSearch.getPageNo();
			Integer pageSize = baseSearch.getPageSize();
			return new BasePage(pageNo,pageSize,total,list,true);
		}
	}
	/**
	 * 采用PageHelper分页方式二
	 * 输出带分页的JSON格式字符串
	 * @param object
	 * @param total
	 * @param baseSearch
	 */

	protected BasePage outPageStr(Object object,long total,BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			return new BasePage(baseSearch.getStart(),baseSearch.getPageSize(),total,object,true);
		}else{
			return new BasePage(baseSearch.getPageNo(),baseSearch.getPageSize(),total,object,true);
		}
	}
	/**
	 * 采用PageHelper分页方式三
	 * 输出带分页的JSON格式字符串
	 * @param page
	 * @param baseSearch
	 */
	protected BasePage outPageStr(PageInfo page,BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			return new BasePage(baseSearch.getStart(),baseSearch.getPageSize(),page.getTotal(),page.getList(),true);
		}else{
			return new BasePage(baseSearch.getPageNo(),baseSearch.getPageSize(),page.getTotal(),page.getList(),true);
		}
	}

	/**
	 * 采用PageHelper分页方式三-------（Bootstrap分页）
	 * 输出带分页的JSON格式字符串
	 */
	protected BasePage outPageBootStr(PageInfo page,BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			return new BasePage(baseSearch.getStart(),baseSearch.getPageSize(),page.getTotal(),page.getList(),true);
		}else{
			return new BasePage(baseSearch.getPageNo(),baseSearch.getPageSize(),page.getTotal(),page.getList(),true);
		}
	}

	/**
	 * 采用PageHelper分页方式三
	 */
	protected BasePage outPage(PageInfo page){
		Integer start = page.getPageNum();
		Integer pageSize = page.getPageSize();
		return new BasePage(start,pageSize,page.getTotal(),page.getList(),true);
	}

	/**
	 * 分页方式一
	 * 输出带分页的JSON格式字符串
	 */
	protected BasePage outPageStr(List list,int total,BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			return new BasePage(baseSearch.getStart(),baseSearch.getPageSize(),new Long(total),list,true);
		}else{
			return new BasePage(baseSearch.getPageNo(),baseSearch.getPageSize(),new Long(total),list,true);
		}
	}

	/**
	 * 分页方式二
	 * 输出带分页的JSON格式字符串
	 * @param jsonArray
	 */
	protected BasePage outPageStr(JSONArray jsonArray,int total,BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			return new BasePage(baseSearch.getStart(),baseSearch.getPageSize(),new Long(total),jsonArray,true);
		}else{
			return new BasePage(baseSearch.getPageNo(),baseSearch.getPageSize(),new Long(total),jsonArray,true);
		}
	}
	/**
	 * 分页方式二（bootstrap风格）
	 * 输出带分页的JSON格式字符串
	 * @param jsonArray
	 */
	protected BasePage outPageBootStr(JSONArray jsonArray,int total,BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			return new BasePage(baseSearch.getStart(),baseSearch.getPageSize(),new Long(total),jsonArray,true);
		}else{
			return new BasePage(baseSearch.getPageNo(),baseSearch.getPageSize(),new Long(total),jsonArray,true);
		}
	}

	/**
	 * 分页方式三
	 * DataGrid
	 * 输出带分页的JSON格式字符串
	 */
	protected BasePage outPageStr(List list,Integer total,BaseSearch baseSearch,String items,HttpServletRequest request){
		if(baseSearch.getUsePageNo() == false){
			return new BasePage(baseSearch.getStart(),baseSearch.getPageSize(),new Long(total),list,true);
		}else{
			return new BasePage(baseSearch.getPageNo(),baseSearch.getPageSize(),new Long(total),list,true);
		}
	}

	/**
	 * 输出添加删除修改结果JSON格式字符串
	 * @param flag
	 * @param msg
	 */
	protected BaseResult outAudStr(boolean flag,String msg){
		return new BaseResult(msg,flag);
	}

	/**
	 * 输出添加删除修改结果JSON格式字符串
	 * @param flag
	 * @param msg
	 */
	protected BaseResult outAudStr(boolean flag,String msg,Object data){
		return new BaseResult(msg,flag,data);
	}

	/**
	 * 输出添加删除修改结果JSON格式字符串
	 * @param flag
	 */
	protected BaseResult outAudStr(boolean flag){
		if(flag){
			return BaseResult.success();
		}else{
			return BaseResult.fail();
		}
	}

	/**
	 * 输出对象
	 * @param obj
	 */
	protected BaseResult outDataStr(Object obj){
		return new BaseResult(obj);
	}

	/**
	 * 方式一
	 * 输出查找对象的JSON格式字符串 或集合JSON格式字符串
	 * @param obj
	 */
	protected BaseResult outItemsStr(Object obj){
		return new BaseResult(obj);
	}

	/**
	 * @param obj
	 */
	protected BaseResult outComboDataStr(Object obj){
		return new BaseResult(obj);
	}

	/**
	 * 输出普通的字符串
	 * @param jsonStr
	 */
	protected BaseResult outStr(String jsonStr){
		return new BaseResult(jsonStr);
	}


//	/**
//	 * 封装共同的数据权限获取参数
//	 * @param condition
//	 */
//	protected void commonSysUID(String key,Map<String, Object> condition,HttpServletRequest request){
//		String sysUID = (String)request.getSession().getAttribute("sysUID");
//		if(null != sysUID && !"".equals(sysUID)){
//			condition.put(key,sysUID);
//		}else{
//			condition.put(key,"0");
//		}
//	}

	/**
	 * 封装共同分页参数
	 */
	@Deprecated
	protected void commonPager(Map<String,Object> condition,BaseSearch baseSearch){
		Integer start = baseSearch.getStart();
		Integer pageSize = baseSearch.getPageSize();
		condition.put("offset", start);
		condition.put("pageSize", pageSize);
	}

	/**
	 * 封装共同分页参数（采用PageHelper offsetPage方式）
	 * @param baseSearch
	 */
	protected void commonHPager(BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			Integer start = baseSearch.getStart();
			Integer pageSize = baseSearch.getPageSize();
			PageHelper.offsetPage(start, pageSize);
		}else{
			Integer pageNo = baseSearch.getPageNo();
			Integer pageSize = baseSearch.getPageSize();
			if(null == pageNo){
				pageNo = 1;
			}
			if(null == pageSize){
				pageSize = 30;
			}
			PageHelper.startPage(pageNo, pageSize);
		}
	}

	/**
	 * 封装共同分页参数（采用PageHelper startPage方式）
	 * @param start
	 * @param pageSize
	 */
	protected void commonHPagerByStartPage(Integer start,Integer pageSize){
		if(null == start){
			start = 1;
		}
		if(null == pageSize){
			pageSize = 30;
		}
		PageHelper.startPage(start, pageSize);
	}

	/**
	 * 初始化数据绑定
	 * 1. 将所有传递进来的String进行HTML编码
	 * 2. 将字段中Date类型转换为String类型
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
		binder.registerCustomEditor(int.class, new IntegerEditor());
		binder.registerCustomEditor(long.class, new LongEditor());
		binder.registerCustomEditor(double.class, new DoubleEditor());
		binder.registerCustomEditor(float.class, new FloatEditor());
	}
}
