package jehc.cloud.common;

import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * @Desc 心跳
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
public class Heartbeat extends BaseAction {
    /**
     * 心跳
     * @return
     */
    @AuthUneedLogin
    @GetMapping(value="/heartbeat")
    public BaseResult heartbeat(){
        return outAudStr(true);
    }
}
