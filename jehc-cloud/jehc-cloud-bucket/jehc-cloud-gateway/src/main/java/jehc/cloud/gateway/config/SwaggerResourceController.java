package jehc.cloud.gateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger.web.*;
import java.util.List;
/**
 * @Desc swagger聚合接口，三个接口都是swagger-ui.html需要访问的接口
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/swagger-resources")
public class SwaggerResourceController {
    private GwSwaggerResourceProvider gwSwaggerResourceProvider;

    @Autowired
    public SwaggerResourceController(GwSwaggerResourceProvider gwSwaggerResourceProvider) {
        this.gwSwaggerResourceProvider = gwSwaggerResourceProvider;
    }

    @RequestMapping(value = "/configuration/security")
    public ResponseEntity<SecurityConfiguration> securityConfiguration() {
        return new ResponseEntity<>(SecurityConfigurationBuilder.builder().build(), HttpStatus.OK);
    }

    @RequestMapping(value = "/configuration/ui")
    public ResponseEntity<UiConfiguration> uiConfiguration() {
        return new ResponseEntity<>(UiConfigurationBuilder.builder().build(), HttpStatus.OK);
    }

    @RequestMapping
    public ResponseEntity<List<SwaggerResource>> swaggerResources() {
        return new ResponseEntity<>(gwSwaggerResourceProvider.get(), HttpStatus.OK);
    }
}

