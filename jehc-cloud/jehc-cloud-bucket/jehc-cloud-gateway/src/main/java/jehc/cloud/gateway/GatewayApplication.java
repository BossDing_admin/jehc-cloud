package jehc.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
/**
 * @Desc 启动应用
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
@ComponentScan("jehc")
@RefreshScope
public class GatewayApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(GatewayApplication.class, args);
	}

}
