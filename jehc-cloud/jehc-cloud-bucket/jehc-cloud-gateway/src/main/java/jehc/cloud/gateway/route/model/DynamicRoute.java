package jehc.cloud.gateway.route.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Desc 路由信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@ApiModel(value = "路由信息")
@Data
public class DynamicRoute{
    @ApiModelProperty(value = "名称")
    private String title;/**名称**/

    @ApiModelProperty(value = "路由id")
    private String route_id;/**路由id**/

    @ApiModelProperty(value = "转发目标地址")
    private String route_uri;/**转发目标地址**/

    @ApiModelProperty(value = "路由转发顺序")
    private Integer route_order;/**路由转发顺序**/

    @ApiModelProperty(value = "断言")
    private String route_predicates;/**断言**/

    @ApiModelProperty(value = "过滤器")
    private String route_filters;/**过滤器**/

    @ApiModelProperty(value = "附加数据")
    private String route_metadata;/**附加数据**/

    @ApiModelProperty(value = "状态：0启用1关闭")
    private Integer status;/**状态：0启用1关闭**/

    private Integer del_flag;/**删除标记：0正常1已删除**/

    @ApiModelProperty(value = "断言参数")
    private List<GatewayPredicateDefinition> gatewayPredicateDefinitions;/**断言参数**/

    @ApiModelProperty(value = "过滤器参数")
    private List<GatewayFilterDefinition> gatewayFilterDefinitions;/**过滤器参数**/
}
