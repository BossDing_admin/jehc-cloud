package jehc.cloud.gateway.util;
import com.alibaba.csp.sentinel.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.connection.RedisClusterConnection;
import org.springframework.data.redis.connection.RedisClusterNode;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisClusterConnection;
import org.springframework.data.redis.connection.jedis.JedisConnection;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @Desc redis工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Lazy
@Slf4j
public class GatewayRedisUtil {

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	StringRedisTemplate stringRedisTemplate;

	/**
	 * 指定缓存失效时间
	 * @param key 键
	 * @param time 时间(秒)
	 * @return
	 */
	public boolean expire(String key,long time){
		try {
			if(time>0){
				redisTemplate.expire(key, time, TimeUnit.SECONDS);
			}
			return true;
		} catch (Exception e) {
			log.error("RedisUtil","执行expire方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 根据key 获取过期时间
	 * @param key 键 不能为null
	 * @return 时间(秒) 返回0代表为永久有效
	 */
	public long getExpire(String key){
		return redisTemplate.getExpire(key, TimeUnit.SECONDS);
	}

	/**
	 * 判断key是否存在
	 * @param key 键
	 * @return true 存在 false不存在
	 */
	public boolean hasKey(String key){
		try {
			return redisTemplate.hasKey(key);
		} catch (Exception e) {
			log.error("RedisUtil","执行hasKey方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 删除指定的key,也可以传入一个包含key的数组
	 * @param key
	 * @return 返回删除成功的个数
	 */
	public void del(String ... key){
		if(key!=null&&key.length>0){
			if(key.length==1){
				redisTemplate.delete(key[0]);
			}else{
				redisTemplate.delete(CollectionUtils.arrayToList(key));
			}
		}
	}

	/////////////////////////////////////String开始///////////////////////////////////
	/**
	 * String 通过key获取储存在redis中的value 并释放连接
	 * @param key
	 * @return 成功返回value 失败返回null
	 */
	public String get(String key) {
		Object object = redisTemplate.opsForValue().get(key);
		if(null == object){
			return null;
		}
		return ""+object;
	}

	/**
	 * string 向redis存入key和value,并释放连接资源 如果key已经存在 则覆盖
	 * @param key
	 * @param value
	 * @return 成功 返回OK 失败返回 0
	 */
	public boolean set(String key, String value) {
		try {
			redisTemplate.opsForValue().set(key,value);
			return true;
		} catch (Exception e) {
			log.error("RedisUtil","执行set方法,异常信息："+e.getMessage());
			return false;
		}

	}
	/**
	 * 普通缓存放入并设置时间
	 * @param key 键
	 * @param value 值
	 * @param time 时间(秒) time要大于0 如果time小于等于0 将设置无限期
	 * @return true成功 false 失败
	 */
	public boolean set(String key,Object value,long time){
		try {
			if(time>0){
				redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
			}else{
				redisTemplate.opsForValue().set(key, value);
			}
			return true;
		} catch (Exception e) {
			log.error("RedisUtil","执行set方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 递增
	 * @param key 键
	 * @param delta 要增加几(大于0)
	 * @return
	 */
	public long incr(String key, long delta){
		if(delta<0){
			throw new RuntimeException("递增因子必须大于0");
		}
		return redisTemplate.opsForValue().increment(key, delta);
	}

	/**
	 * 递减
	 * @param key 键
	 * @param delta 要减少几(小于0)
	 * @return
	 */
	public long decr(String key, long delta){
		if(delta<0){
			throw new RuntimeException("递减因子必须大于0");
		}
		return redisTemplate.opsForValue().increment(key, -delta);
	}
	/////////////////////////////////////String结束///////////////////////////////////

	/////////////////////////////////////HASH集合开始///////////////////////////////////
	/**
	 * hash 通过key给field设置指定的值,如果key不存在,则先创建 ,存在会覆盖原来的值
	 * @param key
	 * @param field 字段
	 * @param value
	 * @return 如果不存在，新建的返回1，存在返回0, 异常返回null
	 * 
	 */
	public boolean hset(String key, String field, String value) {
		try {
			redisTemplate.opsForHash().put(key, field, value);
			return true;
		} catch (Exception e) {
			log.error("RedisUtil","执行hset方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 向一张hash表中放入数据,如果不存在将创建
	 * @param key 大key
	 * @param field 字段（小key）
	 * @param value 值
	 * @param time 时间(秒)  注意:如果已存在的hash表有时间,这里将会替换原有的时间
	 * @return true 成功 false失败
	 */
	public boolean hset(String key,String field,Object value,long time) {
		try {
			redisTemplate.opsForHash().put(key, field, value);
			if(time>0){
				expire(key, time);
			}
			return true;
		} catch (Exception e) {
			log.error("RedisUtil","执行hset方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 删除hash表中的值
	 * @param key 大key 不能为null
	 * @param field 字段（小key） 可以使多个 不能为null
	 */
	public boolean hdel(String key, Object... field){
		try {
			redisTemplate.opsForHash().delete(key,field);
			return true;
		} catch (Exception e) {
			log.error("RedisUtil","执行hdel方法,异常信息："+e.getMessage());
			return false;
		}

	}

	/**
	 * 判断hash表中是否有该项的值
	 * @param key 大key 不能为null
	 * @param field 字段（小key） 可以使多个 不能为null
	 * @return true 存在 false不存在
	 */
	public boolean hHasKey(String key, String field){
		return redisTemplate.opsForHash().hasKey(key, field);
	}

	/**
	 * hash递增 如果不存在,就会创建一个 并把新增后的值返回
	 * @param key 大key 不能为null
	 * @param field 字段（小key） 可以使多个 不能为null
	 * @param by 要增加几(大于0)
	 * @return
	 */
	public double hincr(String key, String field,double by){
		return redisTemplate.opsForHash().increment(key, field, by);
	}

	/**
	 * hash递减
	 * @param key 大key
	 * @param field 字段（小key）
	 * @param by 要减少记(小于0)
	 * @return
	 */
	public double hdecr(String key, String field,double by) {
		return redisTemplate.opsForHash().increment(key, field, -by);
	}

	/**
	 * HashSet
	 * @param key 键
	 * @param map 对应多个键值
	 * @return true 成功 false 失败
	 */
	public boolean hmset(String key, Map<String,Object> map){
		try {
			redisTemplate.opsForHash().putAll(key, map);
			return true;
		} catch (Exception e) {
			log.error("RedisUtil","执行hmset方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 通过key 和 field 获取指定的 value
	 * @param key 大key
	 * @param field 小key
	 * @return 没有返回null
	 */
	public Object hget(String key, String field) {
		try {
			return redisTemplate.opsForHash().get(key, field);
		} catch (Exception e) {
			log.error("RedisUtil","执行hget方法,异常信息："+e.getMessage());
			return null;
		}
	}

	/**
	 * 获取hashKey对应的所有键值
	 * @param key 键
	 * @return 对应的多个键值
	 */
	public Map<Object,Object> hmget(String key){
		try {
			return redisTemplate.opsForHash().entries(key);
		} catch (Exception e) {
			log.error("RedisUtil","执行hmget方法,异常信息："+e.getMessage());
			return null;
		}
	}

	/**
	 * 获取hash表中所有key
	 * @param hashName
	 * @return
	 */
	public Set<String> hKeys(String hashName){
		try {
			if(StringUtil.isEmpty(hashName)){
				log.error("RedisUtil","getHashAllKey,未能获取到hashName值");
				throw new RuntimeException("getHashAllKey,未能获取到hashName值");
			}
			return redisTemplate.opsForHash().keys(hashName);
		} catch (Exception e) {
			log.error("RedisUtil","HttpSessionUtils类getHashAllKey方法,Redis Session 读取出现异常"+e.getMessage());
			throw new RuntimeException("getAttribute类getHashAllKey方法，获取Redis出现异常",e.getCause());
		}
	}
	/////////////////////////////////////HASH集合结束///////////////////////////////////

	/////////////////////////////////////SET集合开始///////////////////////////////////
	/**
	 * 根据key获取Set中的所有值
	 * @param key 键
	 * @return
	 */
	public Set<Object> sGet(String key){
		try {
			return redisTemplate.opsForSet().members(key);
		} catch (Exception e) {
			log.error("RedisUtil","执行sGet方法,异常信息："+e.getMessage());
			return null;
		}
	}

	/**
	 * 根据value从一个set中查询,是否存在
	 * @param key 键
	 * @param value 值
	 * @return true 存在 false不存在
	 */
	public boolean sHasKey(String key,Object value){
		try {
			return redisTemplate.opsForSet().isMember(key, value);
		} catch (Exception e) {
			log.error("RedisUtil","执行sHasKey方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 将数据放入set缓存
	 * @param key 键
	 * @param values 值 可以是多个
	 * @return 成功个数
	 */
	public long sSet(String key, Object...values) {
		try {
			return redisTemplate.opsForSet().add(key, values);
		} catch (Exception e) {
			log.error("RedisUtil","执行sSet方法,异常信息："+e.getMessage());
			return 0;
		}
	}

	/**
	 * 将set数据放入缓存
	 * @param key 键
	 * @param time 时间(秒)
	 * @param values 值 可以是多个
	 * @return 成功个数
	 */
	public long sSetAndTime(String key,long time,Object...values) {
		try {
			Long count = redisTemplate.opsForSet().add(key, values);
			if(time>0) expire(key, time);
			return count;
		} catch (Exception e) {
			log.error("RedisUtil","执行sSetAndTime方法,异常信息："+e.getMessage());
			return 0;
		}
	}

	/**
	 * 使用scan遍历key
	 * @param matchKey
	 * @return
	 */
	public Set<String> getKeysByScanMatch(String matchKey) {
		return (Set<String>) redisTemplate.execute((RedisCallback<Set<String>>) connection -> {
			Set<String> keysTmp = new HashSet<>();
			try (Cursor<byte[]> cursor = connection.scan(new ScanOptions.ScanOptionsBuilder()
					.match(matchKey)
					.count(10000).build())) {

				while (cursor.hasNext()) {
					keysTmp.add(new String(cursor.next(), "Utf-8"));
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			return keysTmp;
		});
	}

//	public Object getHashKeyValue(String key) {
//		log.info("获取HashKey key:{}", key);
//		redisTemplate.setKeySerializer(new StringRedisSerializer());
//		HashOperations<String, Object, Object> ops = redisTemplate.opsForHash();
//		log.info("获取HashKey redisTemplate:{}", redisTemplate);
//		return ops.entries(key);
//	}
}
