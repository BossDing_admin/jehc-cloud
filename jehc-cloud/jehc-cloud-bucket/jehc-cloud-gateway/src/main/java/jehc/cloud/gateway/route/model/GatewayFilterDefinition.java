package jehc.cloud.gateway.route.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

/**
 * @Desc 路由过滤器定义
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@ApiModel(value = "路由过滤器定义")
@Data
public class GatewayFilterDefinition {

    @ApiModelProperty(value = "参数")
    private Map<String,String> args;

    @ApiModelProperty(value = "名称")
    private String name;
}
