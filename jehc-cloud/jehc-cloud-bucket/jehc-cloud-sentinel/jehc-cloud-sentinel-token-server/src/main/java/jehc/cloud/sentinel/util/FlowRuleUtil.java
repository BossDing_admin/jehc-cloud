package jehc.cloud.sentinel.util;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@Component
public class FlowRuleUtil {
    @Value("${jehc.nacos.username:nacos}")
    private String username;

    @Value("${jehc.nacos.password:nacos}")
    private String password;

    @Value("${jehc.nacos.nacosNamespace:public}")
    private String nacosNamespace;//Nacos自身命名空间地址 （目的将其它配置都放到该空间下）

    @Value("${jehc.nacos.nacosAddress:localhost:8848}")
    private String nacosAddress;//Nacos配置中心地址

    @Value("${jehc.nacos.groupId:DEFAULT_GROUP}")
    private String groupId;//Nacos配置中心组（默认使用SENTINEL_GROUP 原因Sentinel Dashboard控制台 做持久化操作是通过该组做的）

    @Value("${jehc.nacos.token_server_client_namespace_set:token_server_client_namespace_set}")
    private String tokenServerClientNamespaceSet;//Nacos配置中心 Data-ID：token-server-client-namespace-set

    @Value("${jehc.nacos.tokenServerFlow}")//Nacos配置中心token-server-flow 最大QPS（最大允许QPS的默认30000，是指Token Client对Token Server请求token的QPS，是对Server的保护）
    private String tokenServerFlow;
}
