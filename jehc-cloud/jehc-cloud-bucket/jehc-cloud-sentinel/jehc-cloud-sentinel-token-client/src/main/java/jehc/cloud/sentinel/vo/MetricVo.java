package jehc.cloud.sentinel.vo;

import com.alibaba.csp.sentinel.node.metric.MetricNode;

public class MetricVo extends MetricNode {
    private String appName;

    public MetricVo(){
        super();
    }

    public MetricVo(String appName){

        super();
        this.appName = appName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}
