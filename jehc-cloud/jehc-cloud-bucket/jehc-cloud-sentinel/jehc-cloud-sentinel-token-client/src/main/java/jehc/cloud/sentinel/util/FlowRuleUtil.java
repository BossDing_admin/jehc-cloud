package jehc.cloud.sentinel.util;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@Service
public class FlowRuleUtil {
    @Value("${jehc.nacos.username:nacos}")
    private String username;

    @Value("${jehc.nacos.password:nacos}")
    private String password;

    @Value("${jehc.nacos.nacosNamespace:public}")
    private String nacosNamespace;//Nacos自身命名空间地址 （目的将其它配置都放到该空间下）

    @Value("${jehc.nacos.nacosAddress:localhost:8848}")
    private String nacosAddress;//Nacos配置中心地址

    @Value("${jehc.nacos.groupId:DEFAULT_GROUP}")
    private String groupId;//Nacos配置中心组（默认使用SENTINEL_GROUP 原因Sentinel Dashboard控制台 做持久化操作是通过该组做的）

    @Value("${jehc.nacos.serverAddress:token-server-address}")
    private String serverAddress;//Nacos Data-ID为token_server_address token-sever服务端的地址 用于客户端 这样配置后 无需再客户端页面中集群流控中手动选择地址了

    @Value("${jehc.nacos.tokenClientConfig:}")
    private String tokenClientConfig;

    //Kafka config
    @Value("${jehc.nacos.sentinelTopic:sentinel_log_topic}")
    private String sentinelTopic;//topic

    @Value("${jehc.nacos.sentinelKafkaAddress:}")
    private String sentinelKafkaAddress;//连接地址

    @Value("${jehc.nacos.sentinelKafkaAcks:0}")
    private String sentinelKafkaAcks;//acks

    @Value("${jehc.nacos.sentinelKafkaRetries:0}")
    private String sentinelKafkaRetries;//重试次数

    @Value("${jehc.nacos.sentinelKafkaBatchSize:0}")
    private String sentinelKafkaBatchSize;//

    @Value("${jehc.nacos.sentinelKafkaLingerMs:1}")
    private String sentinelKafkaLingerMs;//

    @Value("${jehc.nacos.sentinelKafkaBufferMemory:33554432}")
    private String sentinelKafkaBufferMemory;

    @Override
    public String toString() {
        return "FlowRuleUtil{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nacosNamespace='" + nacosNamespace + '\'' +
                ", nacosAddress='" + nacosAddress + '\'' +
                ", groupId='" + groupId + '\'' +
                ", serverAddress='" + serverAddress + '\'' +
                ", tokenClientConfig='" + tokenClientConfig + '\'' +
                ", sentinelTopic='" + sentinelTopic + '\'' +
                ", sentinelKafkaAddress='" + sentinelKafkaAddress + '\'' +
                ", sentinelKafkaAcks='" + sentinelKafkaAcks + '\'' +
                ", sentinelKafkaRetries='" + sentinelKafkaRetries + '\'' +
                ", sentinelKafkaBatchSize='" + sentinelKafkaBatchSize + '\'' +
                ", sentinelKafkaLingerMs='" + sentinelKafkaLingerMs + '\'' +
                ", sentinelKafkaBufferMemory='" + sentinelKafkaBufferMemory + '\'' +
                '}';
    }
}
