package jehc.cloud.file.service;
import jehc.cloud.file.model.XtAttachment;

import java.util.List;
import java.util.Map;
/**
 * @Desc 附件管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtAttachmentService {
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtAttachment> getXtAttachmentListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param xt_attachment_id
	* @return
	*/
	XtAttachment getXtAttachmentById(String xt_attachment_id);
	/**
	* 添加
	* @param xtAttachment
	* @return
	*/
	int addXtAttachment(XtAttachment xtAttachment);
	/**
	 * 添加
	 * @param xtAttachmentList
	 * @return
	 */
	int addBatchXtAttachment(List<XtAttachment> xtAttachmentList);
	/**
	* 修改
	* @param xtAttachment
	* @return
	*/
	int updateXtAttachment(XtAttachment xtAttachment);
	/**
	* 删除
	* @param condition
	* @return
	*/
	int delXtAttachment(Map<String, Object> condition);
	/**
	 * 根据如编号数组批量查询集合
	 * @param condition
	 * @return
	 */
	List<XtAttachment> getXtAttachmentList(Map<String, Object> condition);
}
