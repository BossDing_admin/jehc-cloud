package jehc.cloud.file;

import jehc.cloud.common.util.logger.Logback4jUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Desc 启动应用
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@MapperScan("jehc.cloud.*.*dao")
@ServletComponentScan
@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})//取消默认单一数据源注入 采用读写分离
@ComponentScan("jehc")
@EnableScheduling
public class FileApplication extends Logback4jUtil {
    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class, args);
    }
}
