package jehc.cloud.file.service.impl;

import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import jehc.cloud.file.dao.XtAttachmentDao;
import jehc.cloud.file.model.XtAttachment;
import jehc.cloud.file.service.XtAttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 附件管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class XtAttachmentServiceImpl extends BaseService implements XtAttachmentService {
	@Autowired
	private XtAttachmentDao xtAttachmentDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtAttachment> getXtAttachmentListByCondition(Map<String,Object> condition){
		try {
			return xtAttachmentDao.getXtAttachmentListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_attachment_id 
	* @return
	*/
	public XtAttachment getXtAttachmentById(String xt_attachment_id){
		try {
			return xtAttachmentDao.getXtAttachmentById(xt_attachment_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtAttachment
	* @return
	*/
	public int addXtAttachment(XtAttachment xtAttachment){
		int i = 0;
		try {
			i = xtAttachmentDao.addXtAttachment(xtAttachment);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 添加
	 * @param xtAttachmentList
	 * @return
	 */
	public int addBatchXtAttachment(List<XtAttachment> xtAttachmentList){
		int i = 0;
		try {
			i = xtAttachmentDao.addBatchXtAttachment(xtAttachmentList);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtAttachment
	* @return
	*/
	public int updateXtAttachment(XtAttachment xtAttachment){
		int i = 0;
		try {
			i = xtAttachmentDao.updateXtAttachment(xtAttachment);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtAttachment(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtAttachmentDao.delXtAttachment(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 根据如编号数组批量查询集合
	 * @param condition
	 * @return
	 */
	public List<XtAttachment> getXtAttachmentList(Map<String,Object> condition){
		try {
			return xtAttachmentDao.getXtAttachmentList(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
