package jehc.cloud.log.service.impl;
import java.util.List;
import java.util.Map;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.log.service.LogLoadinfoService;
import jehc.cloud.log.dao.LogLoadinfoDao;
import jehc.cloud.log.model.LogLoadinfo;

/**
* @Desc 页面加载信息 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:22:05
*/
@Service("logLoadinfoService")
public class LogLoadinfoServiceImpl extends BaseService implements LogLoadinfoService{
	@Autowired
	private LogLoadinfoDao logLoadinfoDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LogLoadinfo> getLogLoadinfoListByCondition(Map<String,Object> condition){
		try{
			return logLoadinfoDao.getLogLoadinfoListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public LogLoadinfo getLogLoadinfoById(String id){
		try{
			LogLoadinfo logLoadinfo = logLoadinfoDao.getLogLoadinfoById(id);
			return logLoadinfo;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param logLoadinfo 
	* @return
	*/
	public int addLogLoadinfo(LogLoadinfo logLoadinfo){
		int i = 0;
		try {
			i = logLoadinfoDao.addLogLoadinfo(logLoadinfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param logLoadinfo 
	* @return
	*/
	public int updateLogLoadinfo(LogLoadinfo logLoadinfo){
		int i = 0;
		try {
			i = logLoadinfoDao.updateLogLoadinfo(logLoadinfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param logLoadinfo 
	* @return
	*/
	public int updateLogLoadinfoBySelective(LogLoadinfo logLoadinfo){
		int i = 0;
		try {
			i = logLoadinfoDao.updateLogLoadinfoBySelective(logLoadinfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLogLoadinfo(Map<String,Object> condition){
		int i = 0;
		try {
			i = logLoadinfoDao.delLogLoadinfo(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
