package jehc.cloud.log.service.impl;
import java.util.List;
import java.util.Map;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.log.service.LogModifyRecordService;
import jehc.cloud.log.dao.LogModifyRecordDao;
import jehc.cloud.log.model.LogModifyRecord;

/**
* @Desc 修改记录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:24:23
*/
@Service("logModifyRecordService")
public class LogModifyRecordServiceImpl extends BaseService implements LogModifyRecordService{
	@Autowired
	private LogModifyRecordDao logModifyRecordDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LogModifyRecord> getLogModifyRecordListByCondition(Map<String,Object> condition){
		try{
			return logModifyRecordDao.getLogModifyRecordListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public LogModifyRecord getLogModifyRecordById(String id){
		try{
			LogModifyRecord logModifyRecord = logModifyRecordDao.getLogModifyRecordById(id);
			return logModifyRecord;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param logModifyRecord 
	* @return
	*/
	public int addLogModifyRecord(LogModifyRecord logModifyRecord){
		int i = 0;
		try {
			i = logModifyRecordDao.addLogModifyRecord(logModifyRecord);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param logModifyRecord 
	* @return
	*/
	public int updateLogModifyRecord(LogModifyRecord logModifyRecord){
		int i = 0;
		try {
			i = logModifyRecordDao.updateLogModifyRecord(logModifyRecord);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param logModifyRecord 
	* @return
	*/
	public int updateLogModifyRecordBySelective(LogModifyRecord logModifyRecord){
		int i = 0;
		try {
			i = logModifyRecordDao.updateLogModifyRecordBySelective(logModifyRecord);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLogModifyRecord(Map<String,Object> condition){
		int i = 0;
		try {
			i = logModifyRecordDao.delLogModifyRecord(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
