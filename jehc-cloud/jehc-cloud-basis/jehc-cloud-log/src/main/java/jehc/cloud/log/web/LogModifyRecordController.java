package jehc.cloud.log.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.log.model.LogModifyRecord;
import jehc.cloud.log.service.LogModifyRecordService;

/**
* @Desc 修改记录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:24:23
*/
@RestController
@RequestMapping("/logModifyRecord")
public class LogModifyRecordController extends BaseAction{
	@Autowired
	private LogModifyRecordService logModifyRecordService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getLogModifyRecordListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LogModifyRecord> logModifyRecordList = logModifyRecordService.getLogModifyRecordListByCondition(condition);
		PageInfo<LogModifyRecord> page = new PageInfo<LogModifyRecord>(logModifyRecordList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getLogModifyRecordById(@PathVariable("id")String id){
		LogModifyRecord logModifyRecord = logModifyRecordService.getLogModifyRecordById(id);
		return outDataStr(logModifyRecord);
	}
	/**
	* 添加
	* @param logModifyRecord 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	@AuthUneedLogin
	public BaseResult addLogModifyRecord(@RequestBody LogModifyRecord logModifyRecord){
		int i = 0;
		if(null != logModifyRecord){
			logModifyRecord.setId(toUUID());
			i=logModifyRecordService.addLogModifyRecord(logModifyRecord);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLogModifyRecord(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=logModifyRecordService.delLogModifyRecord(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
