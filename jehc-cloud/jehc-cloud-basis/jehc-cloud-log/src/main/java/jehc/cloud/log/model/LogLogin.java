package jehc.cloud.log.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;

/**
* @Desc 登录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:21:01
*/
@Data
public class LogLogin extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String ip;/**登录ip**/
	private String content;/**内容**/
	private String browser_type;/**浏览器类型**/
	private String browser_name;/**浏览器名称**/
	private String browser_version;/**浏览器版本**/
	private String system;/**操作系统**/
}
