package jehc.cloud.sys.dao;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtCompany;
/**
 * @Desc 公司信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtCompanyDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtCompany> getXtCompanyListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_company_id 
	* @return
	*/
	XtCompany getXtCompanyById(String xt_company_id);
	/**
	* 添加
	* @param xtCompany
	* @return
	*/
	int addXtCompany(XtCompany xtCompany);
	/**
	* 修改
	* @param xtCompany
	* @return
	*/
	int updateXtCompany(XtCompany xtCompany);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtCompany(Map<String,Object> condition);
}
