package jehc.cloud.sys.web;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 在线用户监控
 * @author 邓纯杰
 *
 */
@RestController
@RequestMapping("/xtonlineuser")
@Api(value = "在线用户监控 API",tags = "在线用户监控 API",description = "在线用户监控 API")
public class XtOnLineUserController extends BaseAction {

	/**
	 * 读取在线用户
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/list")
	@ApiOperation(value="查询在线用户列表并分页", notes="查询在线用户列表并分页")
	public BasePage getXtOnLineUserList(HttpServletRequest request){
		return null;
	}
	
	
	/**
	 * 移除所有人员
	 * @return
	 */
	@GetMapping(value="/removeall/xtonline/user")
	@ApiOperation(value="移除所有人员", notes="移除所有人员")
	public BaseResult removeAllXtOnLineUser(HttpServletRequest request){
		return null;
	}
	
	/**
	 * 剔除指定人
	 */
	@GetMapping(value="/remove/xtonline/user")
	@ApiOperation(value="剔除指定人", notes="剔除指定人")
	public BaseResult removeXtOnLineUser(HttpServletRequest request, String sessionId){
		return outAudStr(true, "剔除成功");
	}
	
	/**
	 * 时时显示在线人数
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/get")
	@ApiOperation(value="时时显示在线人数", notes="时时显示在线人数")
	public BasePage getXaOnLineUser(HttpServletRequest request){
		return null;
	}
}
