package jehc.cloud.sys.model;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 合同管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtConcordat extends BaseEntity{
	private String xt_concordat_id;/**合同编号**/
	private String name;/**合同名称**/
	private String content;/**合同描述**/
}
