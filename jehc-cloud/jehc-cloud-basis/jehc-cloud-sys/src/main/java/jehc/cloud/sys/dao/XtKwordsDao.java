package jehc.cloud.sys.dao;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtKwords;

/**
 * @Desc 关键词（敏感词）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtKwordsDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtKwords> getXtKwordsListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_kwords_id 
	* @return
	*/
	XtKwords getXtKwordsById(String xt_kwords_id);
	/**
	* 添加
	* @param xtKwords
	* @return
	*/
	int addXtKwords(XtKwords xtKwords);
	/**
	* 修改
	* @param xtKwords
	* @return
	*/
	int updateXtKwords(XtKwords xtKwords);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtKwords(Map<String,Object> condition);
}
