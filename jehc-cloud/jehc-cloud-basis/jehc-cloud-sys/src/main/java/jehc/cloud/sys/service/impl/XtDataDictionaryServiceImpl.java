package jehc.cloud.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.dao.XtDataDictionaryDao;
import jehc.cloud.sys.init.GlobalPersistentComponent;
import jehc.cloud.sys.model.XtDataDictionary;
import jehc.cloud.sys.service.XtDataDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;

import javax.annotation.Resource;

/**
 * @Desc 数据字典
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtDataDictionaryService")
public class XtDataDictionaryServiceImpl extends BaseService implements XtDataDictionaryService {
	@Resource
	private XtDataDictionaryDao xtDataDictionaryDao;

	@Autowired
	GlobalPersistentComponent globalPersistentComponent;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtDataDictionary> getXtDataDictionaryListByCondition(Map<String,Object> condition){
		try {
			return xtDataDictionaryDao.getXtDataDictionaryListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	 * 加载ListAll不分页
	 * @param condition
	 * @return
	 */
	public List<XtDataDictionary> getXtDataDictionaryListAllByCondition(Map<String,Object> condition){
		try {
			return xtDataDictionaryDao.getXtDataDictionaryListAllByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		} 
	}
	/**
	* 查询对象
	* @param xt_data_dictionary_id 
	* @return
	*/
	public XtDataDictionary getXtDataDictionaryById(String xt_data_dictionary_id){
		try {
			return xtDataDictionaryDao.getXtDataDictionaryById(xt_data_dictionary_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtDataDictionary
	* @return
	*/
	public int addXtDataDictionary(XtDataDictionary xtDataDictionary){
		int i = 0;
		try {
			i = xtDataDictionaryDao.addXtDataDictionary(xtDataDictionary);
			globalPersistentComponent.initXtDataDictionary();
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtDataDictionary
	* @return
	*/
	public int updateXtDataDictionary(XtDataDictionary xtDataDictionary){
		int i = 0;
		try {
			i = xtDataDictionaryDao.updateXtDataDictionary(xtDataDictionary);
			globalPersistentComponent.initXtDataDictionary();
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtDataDictionary(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtDataDictionaryDao.delXtDataDictionary(condition);
			globalPersistentComponent.initXtDataDictionary();
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
