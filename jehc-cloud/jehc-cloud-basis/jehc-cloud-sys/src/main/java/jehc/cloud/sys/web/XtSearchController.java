package jehc.cloud.sys.web;

import io.swagger.annotations.Api;
import jehc.cloud.common.base.BaseAction;
import org.springframework.web.bind.annotation.RestController;


/**
 * 平台关键字搜索
 * @author dengcj
 *
 */
@RestController
@Api(value = "平台关键字搜索API",tags = "平台关键字搜索API",description = "平台关键字搜索API")
public class XtSearchController extends BaseAction {

}
