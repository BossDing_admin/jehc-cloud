package jehc.cloud.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.sys.model.XtMessage;
import jehc.cloud.sys.service.XtMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
/**
 * @Desc 短消息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtMessage")
@Api(value = "短消息API",tags = "短消息API",description = "短消息API")
public class XtMessageController extends BaseAction {
	@Autowired
	private XtMessageService xtMessageService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询短消息列表并分页", notes="查询短消息列表并分页")
	public BasePage getXtMessageListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		if("0".equals(condition.get("type"))){//我发送的消息
			condition.put("from_id", getXtUid());
		}else if("1".equals(condition.get("type"))){
			condition.put("to_id", getXtUid());
		}else{
			condition.put("to_id", "-1");
		}
		commonHPager(baseSearch);
		List<XtMessage> xt_MessageList = xtMessageService.getXtMessageListByCondition(condition);
		PageInfo<XtMessage> page = new PageInfo<XtMessage>(xt_MessageList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个短消息
	* @param xt_message_id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_message_id}")
	@ApiOperation(value="查询单个短消息", notes="查询单个短消息")
	public BaseResult getXtMessageById(@PathVariable("xt_message_id")String xt_message_id, String type){
		if("1".equals(type)){
			Map<String, Object> condition = new HashMap<String,Object>();
			condition.put("isread", 1);
			condition.put("readtime", getDate());
			condition.put("xt_message_id", xt_message_id);
			xtMessageService.updateRead(condition);
		}
		XtMessage xt_Message = xtMessageService.getXtMessageById(xt_message_id);
		return outDataStr(xt_Message);
	}
	
	/**
	* 添加
	* @param xtMessage
	* @param bindingResult
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个短消息", notes="创建单个短消息")
	public BaseResult addXtMessage(@Valid @RequestBody XtMessage xtMessage, BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return outAudStr(false,backFem(bindingResult));
		}
		int i = 0;
		if(null != xtMessage){
			xtMessage.setXt_message_id(toUUID());
			xtMessage.setCtime(getDate());
			xtMessage.setFrom_id(getXtUid());
			i=xtMessageService.addXtMessage(xtMessage);
		}
		if(i>0){
			return outAudStr(true,"发送成功");
		}else{
			return outAudStr(false,"发送失败");
		}
	}
	
	/**
	* 修改
	* @param xtMessage
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个短消息", notes="编辑单个短消息")
	public BaseResult updateXtMessage(@RequestBody XtMessage xtMessage){
		int i = 0;
		if(null != xtMessage){
			i=xtMessageService.updateXtMessage(xtMessage);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_message_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除短消息", notes="删除短消息")
	public BaseResult delXtMessage(String xt_message_id,String type ){
		int i = 0;
		if(!StringUtil.isEmpty(xt_message_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_message_id",xt_message_id.split(","));
			condition.put("type",type);
			i=xtMessageService.delXtMessage(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	 * 更新已读状态
	 * @param xtMessage
	 * @return
	 */
	@PutMapping(value="/updateRead")
	@ApiOperation(value="更新已读状态", notes="更新已读状态")
	public BaseResult updateRead(@RequestBody XtMessage xtMessage){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("isread", 1);
		condition.put("from_id", getXtUid());
		condition.put("to_id", xtMessage.getTo_id());
		int i = xtMessageService.updateRead(condition);
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 加载历史列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/his/list")
	@ApiOperation(value="加载历史列表数据并分页", notes="加载历史列表数据并分页")
	public BasePage getXtMessageHisListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = new HashMap<String, Object>();
		commonHPager(baseSearch);
		condition.put("from_id", getXtUid());
		List<XtMessage> xt_MessageList = xtMessageService.getXtMessageListByCondition(condition);
		PageInfo<XtMessage> page = new PageInfo<XtMessage>(xt_MessageList);
		condition = new HashMap<String, Object>();
		condition.put("to_id", baseSearch.ConvertToMap().get("to_id"));
		condition.put("from_id", getXtUid());
		condition.put("isread", 1);
		xtMessageService.updateRead(condition);
		return outPageStr(page,baseSearch);
	}
	
	/**
	 * 获取未读消息个数
	 * @param xtMessage
	 * @return
	 */
	@NeedLoginUnAuth
	@PostMapping(value="/unRead/count")
	@ApiOperation(value="查询未读消息个数", notes="查询未读消息个数")
	public BaseResult getXtMessageUnReadCount(@RequestBody XtMessage xtMessage){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("to_id", getXtUid());
		condition.put("isread", 0);
		List<XtMessage> xtMessageList = xtMessageService.getXtMessageCountByCondition(condition);
		return outItemsStr(xtMessageList);
	}
}
