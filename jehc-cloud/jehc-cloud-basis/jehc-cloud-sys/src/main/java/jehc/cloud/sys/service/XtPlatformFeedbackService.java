package jehc.cloud.sys.service;
import java.util.List;
import java.util.Map;
import jehc.cloud.sys.model.XtPlatformFeedback;

/**
 * @Desc 平台反馈意见
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtPlatformFeedbackService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtPlatformFeedback> getXtPlatformFeedbackListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param xt_platform_feedback_id 
	* @return
	*/
	XtPlatformFeedback getXtPlatformFeedbackById(String xt_platform_feedback_id);
	/**
	* 添加
	* @param xtPlatformFeedback 
	* @return
	*/
	int addXtPlatformFeedback(XtPlatformFeedback xtPlatformFeedback);
	/**
	* 修改
	* @param xtPlatformFeedback 
	* @return
	*/
	int updateXtPlatformFeedback(XtPlatformFeedback xtPlatformFeedback);
	/**
	* 修改（根据动态条件）
	* @param xtPlatformFeedback 
	* @return
	*/
	int updateXtPlatformFeedbackBySelective(XtPlatformFeedback xtPlatformFeedback);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtPlatformFeedback(Map<String, Object> condition);
	/**
	* 批量添加
	* @param xtPlatformFeedbackList 
	* @return
	*/
	int addBatchXtPlatformFeedback(List<XtPlatformFeedback> xtPlatformFeedbackList);
	/**
	* 批量修改
	* @param xtPlatformFeedbackList 
	* @return
	*/
	int updateBatchXtPlatformFeedback(List<XtPlatformFeedback> xtPlatformFeedbackList);
	/**
	* 批量修改（根据动态条件）
	* @param xtPlatformFeedbackList 
	* @return
	*/
	int updateBatchXtPlatformFeedbackBySelective(List<XtPlatformFeedback> xtPlatformFeedbackList);
	/**
	* 根据外键删除
	* @param xt_platform_id
	* @return
	*/
	int delXtPlatformFeedbackByForeignKey(String xt_platform_id);
}
