package jehc.cloud.sys.web;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.cache.redis.RedisNumberUtil;
import jehc.cloud.common.util.date.DateUtils;
import jehc.cloud.sys.model.XtNumber;
import jehc.cloud.sys.service.XtNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
/**
 * @Desc 单号生成
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtNumber")
@Api(value = "单号生成API",tags = "单号生成API",description = "单号生成API")
public class XtNumberController extends BaseAction {
	@Autowired
	private XtNumberService xtNumberService;

	@Autowired
	private RedisNumberUtil redisNumberUtil;
	
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询单号列表并分页", notes="查询单号列表并分页")
	public BasePage getXtNumberListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtNumber> xtNumberList = xtNumberService.getXtNumberListByCondition(condition);
		PageInfo<XtNumber> page = new PageInfo<XtNumber>(xtNumberList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 生成ID号（分布式下全局生成唯一单号）
	 * @param modules
	 * @return
	 */
	@AuthUneedLogin
	@GetMapping(value="/generateNumber/{modules}")
	@ApiOperation(value="生成ID号（分布式下全局生成唯一单号）", notes="生成ID号（分布式下全局生成唯一单号）")
	public BaseResult generateNumber(@PathVariable("modules") String modules){
		Long l = redisNumberUtil.generateNumber(modules, DateUtils.getTodayEndTimes());
		return outDataStr(l);
	}
}
