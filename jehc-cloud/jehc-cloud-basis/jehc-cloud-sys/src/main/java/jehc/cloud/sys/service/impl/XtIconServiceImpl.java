package jehc.cloud.sys.service.impl;

import jehc.cloud.common.base.BaseService;
import jehc.cloud.sys.dao.XtIconDao;
import jehc.cloud.sys.model.XtIcon;
import jehc.cloud.sys.param.XtIconForm;
import jehc.cloud.sys.service.XtIconService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Desc 字体图标库
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class XtIconServiceImpl extends BaseService implements XtIconService {

    @Resource
    XtIconDao xtIconDao;

    /**
     * 初始化分页
     * @param condition
     * @return
     */
    public List<XtIcon> getXtIconListByCondition(Map<String,Object> condition){
        return xtIconDao.getXtIconListByCondition(condition);
    }

    /**
     * 查询单个对象
     * @param id
     * @return
     */
    public XtIcon getXtIconById(String id){
        return xtIconDao.getXtIconById(id);
    }

    /**
     * 新增
     * @param xtIcon
     * @return
     */
    public int addXtIcon(XtIcon xtIcon){
       return xtIconDao.addXtIcon(xtIcon);
    }

    /**
     * 修改
     * @param xtIcon
     * @return
     */
    public int updateXtIcon(XtIcon xtIcon){
        return xtIconDao.updateXtIcon(xtIcon);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delXtIcon(Map<String,Object> condition){
        return xtIconDao.delXtIcon(condition);
    }

    /**
     * 查询集合
     * @param xtIconForm
     * @return
     */
    public  List<XtIcon> getXtIconList(XtIconForm xtIconForm){
        return xtIconDao.getXtIconList(xtIconForm);
    }
}
