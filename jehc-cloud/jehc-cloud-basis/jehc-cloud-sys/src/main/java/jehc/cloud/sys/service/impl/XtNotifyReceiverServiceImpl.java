package jehc.cloud.sys.service.impl;

import java.util.List;
import java.util.Map;

import jehc.cloud.sys.dao.XtNotifyReceiverDao;
import jehc.cloud.sys.model.XtNotifyReceiver;
import jehc.cloud.sys.service.XtNotifyReceiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
/**
 * @Desc 通知接收人
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtNotifyReceiverService")
public class XtNotifyReceiverServiceImpl extends BaseService implements XtNotifyReceiverService {
	@Autowired
	private XtNotifyReceiverDao xtNotifyReceiverDao;
	/**
	 * 初始化分页
	 * @param condition
	 * @return
	 */
	public List<XtNotifyReceiver> getXtNotifyReceiverListByCondition(Map<String, Object> condition){
		try {
			return xtNotifyReceiverDao.getXtNotifyReceiverListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	
	public XtNotifyReceiver getXtNotifyReceiverById(String notify_receiver_id){
		try {
			return xtNotifyReceiverDao.getXtNotifyReceiverById(notify_receiver_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	public int delXtNotifyReceiver(Map<String, Object> condition){
		try {
			return xtNotifyReceiverDao.delXtNotifyReceiver(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 根据通知编号查找集合
	 * @param notify_id
	 * @return
	 */
	public List<XtNotifyReceiver> getXtNotifyReceiverListById(String notify_id){
		return xtNotifyReceiverDao.getXtNotifyReceiverListById(notify_id);
	}
	
	/**
	 * 更新已读
	 * @param condition
	 * @return
	 */
	public int updateXtNotifyReceiver(Map<String, Object> condition){
		try {
			return xtNotifyReceiverDao.updateXtNotifyReceiver(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
