package jehc.cloud.sys.service.impl;

import java.util.List;
import java.util.Map;

import jehc.cloud.sys.dao.XtNumberDao;
import jehc.cloud.sys.model.XtNumber;
import jehc.cloud.sys.service.XtNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;

/**
 * @Desc 单号生成
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtNumberService")
public class XtNumberServiceImpl extends BaseService implements XtNumberService {
	@Autowired
	private XtNumberDao xtNumberDao;
	/**
	 * 分页查询
	 * @param condition
	 * @return
	 */
	public List<XtNumber> getXtNumberListByCondition(Map<String, Object> condition){
		return xtNumberDao.getXtNumberListByCondition(condition);
	}
}
