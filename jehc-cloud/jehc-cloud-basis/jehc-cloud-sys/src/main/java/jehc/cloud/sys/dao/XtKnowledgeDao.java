package jehc.cloud.sys.dao;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtKnowledge;

/**
 * @Desc 平台知识内容
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtKnowledgeDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtKnowledge> getXtKnowledgeListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_knowledge_id 
	* @return
	*/
	XtKnowledge getXtKnowledgeById(String xt_knowledge_id);
	/**
	* 添加
	* @param xtKnowledge
	* @return
	*/
	int addXtKnowledge(XtKnowledge xtKnowledge);
	/**
	* 修改
	* @param xtKnowledge
	* @return
	*/
	int updateXtKnowledge(XtKnowledge xtKnowledge);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtKnowledge(Map<String,Object> condition);
	/**
	 * 统计知识点数
	 * @param condition
	 * @return
	 */
	int getXtKnowledgeCount(Map<String,Object> condition);
}
