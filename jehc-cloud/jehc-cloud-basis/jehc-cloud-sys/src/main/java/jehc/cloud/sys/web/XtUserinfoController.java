package jehc.cloud.sys.web;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.*;
import jehc.cloud.common.entity.ConstantEntity;
import jehc.cloud.common.entity.DataDictionaryEntity;
import jehc.cloud.common.entity.UserinfoEntity;
import jehc.cloud.common.form.UserForm;
import jehc.cloud.common.util.*;
import jehc.cloud.log.client.service.LogsUtil;
import jehc.cloud.sys.model.XtUserinfo;
import jehc.cloud.sys.service.XtUserinfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * @Desc 员工信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtUserinfo")
@Slf4j
@Api(value = "员工信息API",tags = "员工信息API",description = "员工信息API")
public class XtUserinfoController extends BaseAction {

	@Autowired
	XtUserinfoService xtUserinfoService;

	@Autowired
	RestTemplateUtil restTemplateUtil;

	@Autowired
	LogsUtil logsUtil;

	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询员工信息列表并分页", notes="查询员工信息列表并分页")
	public BasePage getXtUserinfoListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtUserinfo>XtUserinfoList = xtUserinfoService.getXtUserinfoListByCondition(condition);
		PageInfo<XtUserinfo> page = new PageInfo<XtUserinfo>(XtUserinfoList);
		return outPageBootStr(page,baseSearch);
	}
	
	/**
	 * 已删除用户
	 * @param baseSearch
	 * @return
	 */
	@PostMapping(value="/deleted/list")
	@NeedLoginUnAuth
	@ApiOperation(value="查询已删除用户列表并分页", notes="查询已删除用户列表并分页")
	public BasePage getXtUserinfoDeletedListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtUserinfo>XtUserinfoList = xtUserinfoService.getXtUserinfoDeletedListByCondition(condition);
		PageInfo<XtUserinfo> page = new PageInfo<XtUserinfo>(XtUserinfoList);
		return outPageBootStr(page,baseSearch);
	}
	
	/**
	* 查询单个用户
	* @param xt_userinfo_id
	*/
	@GetMapping(value="/get/{xt_userinfo_id}")
	@NeedLoginUnAuth
	@ApiOperation(value="查询单个用户", notes="查询单个用户")
	public BaseResult getXtUserinfoById(@PathVariable("xt_userinfo_id")String xt_userinfo_id){
		XtUserinfo xtUserinfo = xtUserinfoService.getXtUserinfoById(xt_userinfo_id);
		return outDataStr(xtUserinfo);
	}
	
	/**
	* 添加
	* @param xtUserinfo
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个用户", notes="创建单个用户")
	public BaseResult addXtUserinfo(@RequestBody XtUserinfo xtUserinfo, HttpServletRequest request){
		int i = 0;
		MD5 md5 = new MD5();
		if(null != xtUserinfo){
			xtUserinfo.setXt_userinfo_id(toUUID());
			ConstantEntity constantEntity = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/constantByKey/XtUserinfoDefaultPwd",ConstantEntity.class,request);
			xtUserinfo.setXt_userinfo_passWord(md5.getMD5ofStr(constantEntity.getValue()));
			i=xtUserinfoService.addXtUserinfo(xtUserinfo);
		}
		if(i>0){
			logsUtil.aBLogs("用户控制层", "添加", "添加用户成功");
			return outAudStr(true);
		}else{
			logsUtil.aBLogs("用户控制层", "添加", "添加用户失败");
			return outAudStr(false);
		}
	}
	
	/**
	* 修改
	* @param xtUserinfo
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个用户", notes="编辑单个用户")
	public BaseResult updateXtUserinfo(@RequestBody XtUserinfo xtUserinfo){
		int i = 0;
		if(null != xtUserinfo){
			i=xtUserinfoService.updateXtUserinfo(xtUserinfo);
		}
		if(i>0){
			logsUtil.aBLogs("用户控制层", "修改", "修改用户成功");
			return outAudStr(true);
		}else{
			logsUtil.aBLogs("用户控制层", "修改", "修改用户失败");
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param xt_userinfo_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除用户", notes="删除用户")
	public BaseResult delXtUserinfo(String xt_userinfo_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_userinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_userinfo_id",xt_userinfo_id.split(","));
			i=xtUserinfoService.delXtUserinfo(condition);
		}
		if(i>0){
			logsUtil.aBLogs("用户控制层", "删除", "删除用户成功");
			return outAudStr(true);
		}else{
			logsUtil.aBLogs("用户控制层", "删除", "删除用户失败");
			return outAudStr(false);
		}
	}
	
	/**
	 * 获取名族数据字典
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/nation/list")
	@ApiOperation(value=" 获取名族数据字典", notes=" 获取名族数据字典")
	public BaseResult getXtUserinfoNationList(){
		BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/xtDataDictionary/list/xt_userinfo_nation",BaseResult.class,null);
		List<DataDictionaryEntity> xtDataDictionaryList = JsonUtil.toFastList(baseResult.getData(),DataDictionaryEntity.class);
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 获取性别数据字典
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/sex/list")
	@ApiOperation(value=" 获取性别数据字典", notes=" 获取性别数据字典")
	public BaseResult getXtUserinfoSexList(){
		BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/xtDataDictionary/list/gender",BaseResult.class,null);
		List<DataDictionaryEntity> xtDataDictionaryList = JsonUtil.toFastList(baseResult.getData(),DataDictionaryEntity.class);
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 获取文化程度数据字典
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/highestdegree/list")
	@ApiOperation(value=" 获取文化程度数据字典", notes=" 获取文化程度数据字典")
	public BaseResult getXtUserinfoHighestDegreeList(){
		BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/xtDataDictionary/list/xt_userinfo_highestDegree",BaseResult.class,null);
		List<DataDictionaryEntity> xtDataDictionaryList = JsonUtil.toFastList(baseResult.getData(),DataDictionaryEntity.class);
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 获取工作年限数据字典
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/workyear/list")
	@ApiOperation(value=" 获取工作年限数据字典", notes=" 获取工作年限数据字典")
	public BaseResult getXtUserinfoWorkYearList(){
		BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/xtDataDictionary/list/xt_userinfo_workYear",BaseResult.class,null);
		List<DataDictionaryEntity> xtDataDictionaryList = JsonUtil.toFastList(baseResult.getData(),DataDictionaryEntity.class);
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 获取是否已婚数据字典
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/ismarried/list")
	@ApiOperation(value=" 获取是否已婚数据字典", notes=" 获取是否已婚数据字典")
	public BaseResult getXtUserinfoIsmarriedList(){
		BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/xtDataDictionary/list/xt_userinfo_ismarried",BaseResult.class,null);
		List<DataDictionaryEntity> xtDataDictionaryList = JsonUtil.toFastList(baseResult.getData(),DataDictionaryEntity.class);
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 获取用户状态数据字典
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/state/list")
	@ApiOperation(value=" 获取用户状态数据字典", notes=" 获取用户状态数据字典")
	public BaseResult getXtUserinfoStateList(){
		BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/xtDataDictionary/list/xt_userinfo_state",BaseResult.class,null);
		List<DataDictionaryEntity> xtDataDictionaryList = JsonUtil.toFastList(baseResult.getData(),DataDictionaryEntity.class);
		return outComboDataStr(xtDataDictionaryList);
	}
	
	/**
	 * 判断用户名即登陆账号是否重复
	 * @param xt_userinfo_name
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/validate/{xt_userinfo_name}")
	@ApiOperation(value=" 判断用户名即登陆账号是否重复", notes=" 判断用户名即登陆账号是否重复")
	public BaseResult validateUser(@PathVariable("xt_userinfo_name")String xt_userinfo_name){
		Map<String, Object> condition = new HashMap<String, Object>();
		if(!StringUtil.isEmpty(xt_userinfo_name)){
			condition.put("xt_userinfo_name", xt_userinfo_name);
			int i = xtUserinfoService.validateUser(condition);
			if(i > 0){
				return outAudStr(true,"1");
			}else{
				return outAudStr(true,"0");
			}
		}else{
			return outAudStr(false,"用户名参数未获取!验证失败!");
		}
	}
	
	/**
	* 恢复数据
	* @param xt_userinfo_id
	*/
	@GetMapping(value="/recover")
	@ApiOperation(value=" 恢复数据", notes=" 恢复数据")
	public BaseResult recoverXtUserinfo(String xt_userinfo_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_userinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_userinfo_id",xt_userinfo_id.split(","));
			i=xtUserinfoService.recoverXtUserinfo(condition);
		}
		if(i>0){
			logsUtil.aBLogs("用户控制层", "恢复用户", "恢复用户成功");
			return outAudStr(true);
		}else{
			logsUtil.aBLogs("用户控制层", "恢复用户", "恢复用户失败");
			return outAudStr(false);
		}
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/chatting/list")
	@ApiOperation(value=" 全部用户", notes=" 全部用户")
	public BaseResult getChattingUserinfoList(HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseTreeGridEntity> list = new ArrayList<BaseTreeGridEntity>();
		List<XtUserinfo> XtUserinfoList = xtUserinfoService.getXtUserinfoListByCondition(condition);
		for(int i = 0; i < XtUserinfoList.size(); i++){
			XtUserinfo Xt_Userinfo = XtUserinfoList.get(i);
			BaseTreeGridEntity BaseTreeGridEntity = new BaseTreeGridEntity();
			BaseTreeGridEntity.setId(Xt_Userinfo.getXt_userinfo_id());
			BaseTreeGridEntity.setPid("0");
			BaseTreeGridEntity.setText(Xt_Userinfo.getXt_userinfo_realName());
			BaseTreeGridEntity.setContent("");
			BaseTreeGridEntity.setLeaf(true);
			BaseTreeGridEntity.setIcon("/deng/images/icons/employee_manager.png");
			list.add(BaseTreeGridEntity);
		}
		BaseTreeGridEntity baseTreeGridEntity = new BaseTreeGridEntity();
		List<BaseTreeGridEntity> baseTreeGridEntityList = baseTreeGridEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseTreeGridEntityList);
		return outStr(json);
	}

	/**
	 * 重置密码
	 * @param xt_userinfo_id
	 * @return
	 */
	@GetMapping(value="/pwd/reset")
	@ApiOperation(value=" 重置密码", notes=" 重置密码")
	public BaseResult resetXtUserinfoPwd(String xt_userinfo_id,HttpServletRequest request){
		int i = 0;
		MD5 md5 = new MD5();
		if(!StringUtils.isEmpty(xt_userinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_userinfo_id",xt_userinfo_id);
			ConstantEntity constantEntity = restTemplateUtil.get(restTemplateUtil.restSysURL() + "/xtCommon/constantByKey/XtUserinfoDefaultPwd",ConstantEntity.class,request);
			condition.put("xt_userinfo_passWord", md5.getMD5ofStr(constantEntity.getValue()));
			i=xtUserinfoService.updatePwd(condition);
		}
		XtUserinfo user = xtUserinfoService.getXtUserinfoById(xt_userinfo_id);
		if(i>0){
			logsUtil.aBLogs("用户控制层", "重置用户密码", "重置用户密码，用户名：【"+user.getXt_userinfo_name()+"】用户姓名：【"+user.getXt_userinfo_realName()+"】成功");
			return outAudStr(true);
		}else{
			logsUtil.aBLogs("用户控制层", "重置用户密码", "重置用户密码，用户名：【"+user.getXt_userinfo_name()+"】用户姓名：【"+user.getXt_userinfo_realName()+"】失败");
			return outAudStr(false);
		}
	}
	
	/**
	 * 根据各种情况查找集合不分页（流程设计器中处理人 发起人 发起人组等使用）
	 * @param xt_userinfo_id
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/lc/list/{xt_userinfo_id}")
	@ApiOperation(value=" 根据各种情况查找集合不分页", notes=" 根据各种情况查找集合不分页")
	public BaseResult getXtUserinfoList(@PathVariable("xt_userinfo_id")String xt_userinfo_id){
		List<XtUserinfo> list = new ArrayList<XtUserinfo>();
		if(!StringUtil.isEmpty(xt_userinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_userinfo_id", xt_userinfo_id.split(","));
			list = xtUserinfoService.getXtUserinfoList(condition);
		}
		return  outItemsStr(list);
	}
	
//	/**
//	* 修改（个人中心）
//	* @param xt_Userinfo
//	*/
//	@NeedLoginUnAuth
//	@PutMapping(value="/myinfo/update")
//	public BaseResult updateMyXtUserinfo(XtUserinfo xt_Userinfo){
//		int i = 0;
//		UserinfoEntity userinfoEntity = getXtU();
//		XtUserinfo xtUserinfo = new XtUserinfo();
//		BeanUtils.copyProperties(userinfoEntity, xtUserinfo);//拷贝
//
//		if(null == xtUserinfo){
//			throw new ExceptionUtil("未能获取到当前用户！");
//		}
//		if(null != xt_Userinfo && !"".equals(xt_Userinfo)){
//			xtUserinfo.setXt_userinfo_phone(xt_Userinfo.getXt_userinfo_phone());
//			xtUserinfo.setXt_userinfo_mobile(xt_Userinfo.getXt_userinfo_mobile());
//			xtUserinfo.setXt_userinfo_ortherTel(xt_Userinfo.getXt_userinfo_ortherTel());
//			xtUserinfo.setXt_userinfo_qq(xt_Userinfo.getXt_userinfo_qq());
//			xtUserinfo.setXt_userinfo_email(xt_Userinfo.getXt_userinfo_email());
//			xtUserinfo.setXt_userinfo_remark(xt_Userinfo.getXt_userinfo_remark());
//			xtUserinfo.setXt_userinfo_address(xt_Userinfo.getXt_userinfo_address());
//			i=xtUserinfoService.updateXtUserinfo(xtUserinfo);
//		}
//		if(i>0){
//			aBLogs("用户控制层", "修改", "修改用户成功");
//			return outAudStr(true);
//		}else{
//			aBLogs("用户控制层", "修改", "修改用户失败");
//			return outAudStr(false);
//		}
//	}
	
	/**
	* myinfo
	*/
	@GetMapping(value="/myinfo")
	@NeedLoginUnAuth
	@ApiOperation(value=" 当前用户信息", notes=" 当前用户信息")
	public BaseResult myinfo(){
		return outDataStr(getXtU());
	}
	
	/**
	 * 流程模块使用（查找用户）
	 * @param baseSearch
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@PostMapping(value="/lc/list")
	@NeedLoginUnAuth
	@ApiOperation(value=" 流程模块使用", notes=" 流程模块使用")
	public BasePage getXtUserinfoListForLcByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtUserinfo>XtUserinfoList = xtUserinfoService.getXtUserinfoListByCondition(condition);
		PageInfo<XtUserinfo> page = new PageInfo<XtUserinfo>(XtUserinfoList);
		return outPageStr(page,baseSearch);
	}

	/**
	 * 同步至授权中心
	 * @param userinfoEntity
	 */
	@PostMapping(value="/syncOauth")
	@ApiOperation(value="同步至授权中心", notes="同步至授权中心")
	public BaseResult syncOauth(@RequestBody UserinfoEntity userinfoEntity, HttpServletRequest request){
		BaseResult baseResult = new BaseResult();
		baseResult.setSuccess(false);
		if(null != userinfoEntity){
			if(StringUtil.isEmpty(userinfoEntity.getXt_userinfo_id())){
				baseResult.setMessage("未能获取到用户id");
				return baseResult;
			}
			Map<String,Object> condition = new HashMap<>();
			condition.put("xt_userinfo_id", userinfoEntity.getXt_userinfo_id().split(","));
			List<XtUserinfo> userinfos = xtUserinfoService.getXtUserinfoList(condition);
			List<UserinfoEntity> userinfoEntities = JsonUtil.toFastList(userinfos,UserinfoEntity.class);
			if(!CollectionUtils.isEmpty(userinfoEntities)) {
				for (UserinfoEntity userinfoEntitys : userinfoEntities) {
					userinfoEntitys.setAccount_type_id(userinfoEntity.getAccount_type_id());
                    userinfoEntitys.setSync(userinfoEntity.getSync());
				}
				baseResult = restTemplateUtil.post(restTemplateUtil.restOauthURL() + "/oauthAccount/sync",BaseResult.class,userinfoEntities,request);
			}
		}
		if(baseResult.getSuccess()){
			logsUtil.aBLogs("用户控制层", "同步至授权中心", "同步至授权中心成功");
			return baseResult;
		}else{
			logsUtil.aBLogs("用户控制层", "同步至授权中心", "同步至授权中心失败");
			return baseResult;
		}
	}

	/**
	 * 上传头像
	 * @param request
	 * @return
	 */
	@PostMapping(value = "/pic/upload")
	@NeedLoginUnAuth
	public BaseResult uploadMyPic(HttpServletRequest request) {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;//转型为MultipartHttpRequest：
		MultiValueMap<String, MultipartFile> multipartFileMultiValueMap =  multipartRequest.getMultiFileMap();//获取所有文件
		MultipartFile multipartFile = multipartRequest.getFile("userPicFile");//获取指定文件即前端file框name标签
		String userName = getXtU().getAccount();
		if(!StringUtil.isEmpty(userName)){
			XtUserinfo xtUserinfo = xtUserinfoService.getXtUserinfoByUserName(userName);
			String pic = null;
			if(null != multipartFile && null != xtUserinfo){
				try {
					pic = Base64Util.toBase64(multipartFile.getBytes());
					xtUserinfo.setXt_userinfo_image(pic);
				}catch (Exception e){
					log.error(e.getMessage());
				}
				int i = xtUserinfoService.updateXtUserinfo(xtUserinfo);
				if(i>0){
					return BaseResult.success(xtUserinfo);
				}
			}
		}
		return BaseResult.fail();
	}

	/**
	 * 查询单个用户头像
	 * @param userName
	 */
	@GetMapping(value="/pic/{userName}")
	@AuthUneedLogin
	@ApiOperation(value="查询单个用户头像", notes="查询单个用户头像")
	public BaseResult getXtUserinfoPicById(@PathVariable("userName")String userName){
		XtUserinfo xtUserinfo = xtUserinfoService.getXtUserinfoByUserName(userName);
		return BaseResult.success(xtUserinfo);
	}

	/**
	 * 查找用户集合不分页
	 * @param userForm
	 * @return
	 */
	@AuthUneedLogin
	@PostMapping(value="/getUserinfoList")
	@ApiOperation(value=" 查找用户集合不分页", notes=" 查找用户集合不分页")
	public BaseResult<List<XtUserinfo>> getUserinfoList(@RequestBody UserForm userForm){
		List<XtUserinfo> list = new ArrayList<XtUserinfo>();
		Map<String,Object> condition = new HashMap<>();
		if(CollectionUtil.isNotEmpty(userForm.getUserInfoIdList())){
			condition.put("xt_userinfo_id", userForm.getUserInfoIdList().toArray(new String[]{}));
		}
		if(CollectionUtil.isNotEmpty(userForm.getPostIdList())){
			condition.put("xt_post_id", userForm.getPostIdList().toArray(new String[]{}));
		}
		if(CollectionUtil.isNotEmpty(userForm.getDepartinfoIdList())){
			condition.put("xt_departinfo_id", userForm.getDepartinfoIdList().toArray(new String[]{}));
		}
		if(CollectionUtil.isNotEmpty(condition)){
			list = xtUserinfoService.getXtUserinfoList(condition);
		}
		return  BaseResult.success(list);
	}
}
