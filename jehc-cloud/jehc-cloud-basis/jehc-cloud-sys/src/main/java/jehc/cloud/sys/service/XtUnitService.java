package jehc.cloud.sys.service;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtUnit;

/**
 * @Desc 商品(产品)单位
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtUnitService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtUnit> getXtUnitListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_unit_id 
	* @return
	*/
	XtUnit getXtUnitById(String xt_unit_id);
	/**
	* 添加
	* @param xtUnit
	* @return
	*/
	int addXtUnit(XtUnit xtUnit);
	/**
	* 修改
	* @param xtUnit
	* @return
	*/
	int updateXtUnit(XtUnit xtUnit);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtUnit(Map<String,Object> condition);
}
