package jehc.cloud.sys.service;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtDataDictionary;

/**
 * @Desc 数据字典
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtDataDictionaryService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtDataDictionary> getXtDataDictionaryListByCondition(Map<String,Object> condition);
	/**
	 * 加载ListAll不分页
	 * @param condition
	 * @return
	 */
	@SuppressWarnings("unchecked")
	List<XtDataDictionary> getXtDataDictionaryListAllByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_data_dictionary_id 
	* @return
	*/
	XtDataDictionary getXtDataDictionaryById(String xt_data_dictionary_id);
	/**
	* 添加
	* @param xtDataDictionary
	* @return
	*/
	int addXtDataDictionary(XtDataDictionary xtDataDictionary);
	/**
	* 修改
	* @param xtDataDictionary
	* @return
	*/
	int updateXtDataDictionary(XtDataDictionary xtDataDictionary);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtDataDictionary(Map<String,Object> condition);
}
