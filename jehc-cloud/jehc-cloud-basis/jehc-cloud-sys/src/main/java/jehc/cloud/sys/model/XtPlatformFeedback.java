package jehc.cloud.sys.model;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * @Desc 平台反馈意见
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtPlatformFeedback extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String xt_platform_feedback_id;/**主键编号**/
	private String xt_platform_id;/**平台发布信息编号**/
	private String content;/**评论内容**/
	private Integer status;/**状态**/
}
