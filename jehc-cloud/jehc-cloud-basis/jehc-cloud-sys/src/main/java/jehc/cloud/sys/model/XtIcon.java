package jehc.cloud.sys.model;

import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 字体图标库
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtIcon extends BaseEntity {
    private String id;/**id**/
    private String name;/**名称**/
    private String categories;/**分类**/
    private String icon;/**字体**/
    private String remark;/**备注**/
}
