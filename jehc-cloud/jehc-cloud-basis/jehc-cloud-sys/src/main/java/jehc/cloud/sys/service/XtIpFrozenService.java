package jehc.cloud.sys.service;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.model.XtIpFrozen;

/**
 * @Desc 平台IP冻结账户
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtIpFrozenService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtIpFrozen> getXtIpFrozenListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_ip_frozen_id 
	* @return
	*/
	XtIpFrozen getXtIpFrozenById(String xt_ip_frozen_id);
	/**
	* 添加
	* @param xtIpFrozen
	* @return
	*/
	int addXtIpFrozen(XtIpFrozen xtIpFrozen);
	/**
	* 修改
	* @param xtIpFrozen
	* @return
	*/
	int updateXtIpFrozen(XtIpFrozen xtIpFrozen);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtIpFrozen(Map<String,Object> condition);
	
	/**
	 * 获取所有集合
	 * @param condition
	 * @return
	 */
	@SuppressWarnings("unchecked")
	List<XtIpFrozen> getXtIpFrozenListAllByCondition(Map<String,Object> condition);
}
