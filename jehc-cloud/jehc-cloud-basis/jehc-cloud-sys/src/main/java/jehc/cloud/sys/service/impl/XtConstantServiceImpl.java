package jehc.cloud.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.sys.dao.XtConstantDao;
import jehc.cloud.sys.init.GlobalPersistentComponent;
import jehc.cloud.sys.model.XtConstant;
import jehc.cloud.sys.service.XtConstantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;

import javax.annotation.Resource;

/**
 * @Desc 台平常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtConstantService")
public class XtConstantServiceImpl extends BaseService implements XtConstantService {
	@Resource
	private XtConstantDao xtConstantDao;

	@Autowired
	GlobalPersistentComponent globalPersistentComponent;

	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtConstant> getXtConstantListByCondition(Map<String,Object> condition){
		try {
			return xtConstantDao.getXtConstantListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_constant_id 
	* @return
	*/
	public XtConstant getXtConstantById(String xt_constant_id){
		try {
			return xtConstantDao.getXtConstantById(xt_constant_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtConstant
	* @return
	*/
	public int addXtConstant(XtConstant xtConstant){
		int i = 0;
		try {
			xtConstant.setCreate_time(getDate());
			xtConstant.setCreate_id(getXtUid());
			i = xtConstantDao.addXtConstant(xtConstant);
			globalPersistentComponent.initXtConstant();
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtConstant
	* @return
	*/
	public int updateXtConstant(XtConstant xtConstant){
		int i = 0;
		try {
			xtConstant.setUpdate_id(getXtUid());
			xtConstant.setUpdate_time(getDate());
			i = xtConstantDao.updateXtConstant(xtConstant);
			globalPersistentComponent.initXtConstant();
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtConstant(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtConstantDao.delXtConstant(condition);
			globalPersistentComponent.initXtConstant();
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 读取所有常量
	 * @param condition
	 * @return
	 */
	public List<XtConstant> getXtConstantListAllByCondition(Map<String,Object> condition){
		try {
			return xtConstantDao.getXtConstantListAllByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
