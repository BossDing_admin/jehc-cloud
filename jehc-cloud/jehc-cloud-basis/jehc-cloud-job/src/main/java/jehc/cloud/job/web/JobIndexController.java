package jehc.cloud.job.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseHttpSessionEntity;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.RestTemplateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Desc 加载首页
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/index")
@Scope("prototype")
@Api(value = "首页API",tags = "首页API",description = "首页API")
public class JobIndexController extends BaseAction {

	@Autowired
	RestTemplateUtil restTemplateUtil;
//	/**
//	 * 载入桌面
//	 * @param request
//	 * @return
//	 */
//	@RequestMapping(value="/desk.html")
//	public ModelAndView loadDesk(HttpServletRequest request,Model model) {
//		Map<String,Object> condition = new HashMap<String,Object>();
//		commonHPager(baseSearch);
//		List<XtNotifyReceiver> xtNotifyReceiverList = xtNotifyReceiverService.getXtNotifyReceiverListByCondition(condition);
//		PageInfo<XtNotifyReceiver> page = new PageInfo<XtNotifyReceiver>(xtNotifyReceiverList);
//		model.addAttribute("xtNotifyCount",page.getTotal());
//		
//		//公告数
//		model.addAttribute("xtNoticeCount", xtNoticeService.getXtNoticeCountByCondition(condition));
//		model.addAttribute("xtNoticeList", xtNoticeService.getXtNoticeListByCondition(condition));
//		//个人登录次数
//		condition.put("xt_userinfo_id", getXtUid());
//		model.addAttribute("xtLoginLogsCount", xtLoginLogsService.getXtLoginLogsCount(condition));
//		//平台知识库数
//		model.addAttribute("xtKnowledgeCount", xtKnowledgeService.getXtKnowledgeCount(condition));
//		
//		condition = new HashMap<String,Object>();
//		//处理短消息
//		condition.put("type", "1");
//		condition.put("to_id", getXtUid());
//		condition.put("isread", 0);
//		List<XtMessage> xt_MessageList = xtMessageService.getXtMessageListByCondition(condition);
//		PageInfo<XtMessage> xtMessage = new PageInfo<XtMessage>(xt_MessageList);
//		model.addAttribute("xtMessageList", xtMessage.getList());
//		return new ModelAndView("pc/sys-view/sys-desk/sys-desk");
//	}
//	
	/**
	 * 封装通过
	 * @param condition
	 */
	private void commonM(Map<String, Object> condition,HttpServletRequest request){
		if(!isAdmin()){
			BaseHttpSessionEntity baseHttpSessionEntity = restTemplateUtil.get(restTemplateUtil.restOauthURL()+"/tokenInfo",BaseHttpSessionEntity.class,request);
			if(null == baseHttpSessionEntity){
				condition.put("xt_role_id", "-1".split(","));
			}else{
				String xt_role_id = baseHttpSessionEntity.getRole_id();
				if(!StringUtils.isEmpty(xt_role_id)){
					condition.put("xt_role_id", xt_role_id.split(","));
				}else{
					condition.put("xt_role_id", "-1".split(","));
				}
			}
		}
	}
	
//	/**
//	 * 修改密码
//	 * @param request
//	 * @return
//	 */
//	@PostMapping(value="/updatePwd")
//	public BaseResult updatePwd(HttpServletRequest request, @RequestBody(required=true)UserEntity userEntity){
//		String oldPwd = userEntity.getOldPwd();
//		String newPwd = userEntity.getNewPwd();
//		UserinfoEntity userinfoEntity = getXtU();
//		XtUserinfo xtUserinfo = new XtUserinfo();
//		BeanUtils.copyProperties(userinfoEntity, xtUserinfo);//拷贝
//		MD5 md5 = new MD5();
//		if(null != oldPwd && !"".equals(oldPwd)){
//			oldPwd = md5.getMD5ofStr(oldPwd.trim());
//			Map<String, Object> condition = new HashMap<String, Object>();
//			condition.put("xt_userinfo_passWord", oldPwd);
//			condition.put("xt_userinfo_name", xtUserinfo.getXt_userinfo_name());
//			if(null != xtUserinfoService.getXtUserinfoByUP(condition)){
//				condition = new HashMap<String, Object>();
//				condition.put("xt_userinfo_passWord", md5.getMD5ofStr(newPwd.trim()));
//				condition.put("xt_userinfo_id", xtUserinfo.getXt_userinfo_id());
//				int i = xtUserinfoService.updatePwd(condition);
//				if(i > 0){
//					BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restOauthURL()+"/loginOut",BaseResult.class, request);
//					if(baseResult.getSuccess()){
//					}else{
//					}
//					return outAudStr(true, "密码修改成功,请重新登录系统!");
//				}else{
//					return outAudStr(false, "密码修改失败!");
//				}
//			}else{
//				return outAudStr(false, "原密码错误!");
//			}
//		}
//		return outAudStr(false, "原密码为空!");
//	}
	
//	/**
//	 * 解锁
//	 * @param request
//	 * @return
//	 */
//	@PostMapping(value="/unlock")
//	public BaseResult unlock(@RequestBody(required=true)LoginEntity loginEntity,HttpServletRequest request){
//		String password = loginEntity.getPassword();
//		if(StringUtil.isEmpty(password)){
//			throw new ExceptionUtil("未能获取到密码！");
//		}
//		UserinfoEntity userinfoEntity = getXtU();
//		XtUserinfo xtUserinfo = new XtUserinfo();
//		BeanUtils.copyProperties(userinfoEntity, xtUserinfo);//拷贝
//		MD5 md5 = new MD5();
//		password = md5.getMD5ofStr(password.trim());
//		Map<String, Object> condition = new HashMap<String, Object>();
//		condition.put("xt_userinfo_passWord", password);
//		condition.put("xt_userinfo_name", xtUserinfo.getXt_userinfo_name());
//		if(null != xtUserinfoService.getXtUserinfoByUP(condition)){
//			return outAudStr(true,"1");
//		}else{
//			return outAudStr(true,"2");
//		}
//	}

	/**
	 * 初始化主页面
	 * @param model
	 * @param request
	 * @return
	 */
	@PostMapping(value="/init")
	@ApiOperation(value="首页数据", notes="首页数据")
	public BaseResult init(Model model, HttpServletRequest request){
		return restTemplateUtil.post(restTemplateUtil.restOauthURL()+"/init",BaseResult.class, request);
	}
}
