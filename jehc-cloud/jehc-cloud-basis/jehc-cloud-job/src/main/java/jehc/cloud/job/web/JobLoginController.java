package jehc.cloud.job.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.AuthUneedLogin;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.entity.LoginEntity;
import jehc.cloud.common.util.RestTemplateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
/**
 * @Desc 登录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@Api(value = "登录API",tags = "登录API",description = "登录API")
public class JobLoginController extends BaseAction {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	RestTemplateUtil restTemplateUtil;
	/**
	 * 登录
	 * @param request
	 */
	@AuthUneedLogin
	@PostMapping(value="/login")
	@ApiOperation(value="登录", notes="登录")
	public BaseResult login(@RequestBody(required=true)LoginEntity loginEntity, HttpServletRequest request, HttpServletResponse response){
		String token = null;
//		Map<String,String> map = new HashMap<>();
//		map.put(CacheConstant.JEHC_CLOUD_KEY,getJehcCloudKey());
//		map.put(CacheConstant.JEHC_CLOUD_SECURITY,getJehcCloudSecurity());
//		BaseResult baseResult = restTemplateUtil.post(restTemplateUtil.restOauthURL()+"/login",BaseResult.class, loginEntity,restTemplateUtil.setHeaders(request,map));
		BaseResult baseResult = restTemplateUtil.post(restTemplateUtil.restOauthURL()+"/login",BaseResult.class, loginEntity,request);
		if(baseResult.getSuccess()){
			token = ""+baseResult.getData();
		}else{
			try{
				restTemplateUtil.get(restTemplateUtil.restOauthURL()+"/loginOut",BaseResult.class, request);
			}catch (Exception e){

			}
		}
		if(baseResult.getSuccess()){
			return outAudStr(true,"登陆成功", token);
		}
		return outAudStr(false, baseResult.getMessage());
	}
	
	/**
	 * 注销
	 * @param request
	 */
	@AuthUneedLogin
	@GetMapping(value="/loginOut")
	@ApiOperation(value="注销", notes="注销")
	public BaseResult loginOut(HttpServletRequest request){
		BaseResult baseResult = restTemplateUtil.get(restTemplateUtil.restOauthURL()+"/loginOut",BaseResult.class, request);
		if(baseResult.getSuccess()){
			return outAudStr(true, "注销平台成功");
		}else{
			return outAudStr(false, "注销平台失败");
		}
	}
}
