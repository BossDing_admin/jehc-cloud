package jehc.cloud.oauth.client.logger;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import jehc.cloud.oauth.client.util.SDKUtil;
import javax.annotation.Resource;

/**
 * @Desc Logback 内容增加ip记录
 * @Author 邓纯杰
 * @CreateTime 2023-04-13 15:13:07
 */
public class IPClassicConverter extends ClassicConverter {

    private String cache;

    @Resource
    SDKUtil sdkUtil;

    @Override
    public String convert(ILoggingEvent event) {
        if (cache != null) {
            return cache;
        }
        String result = sdkUtil.getLocalIp();
        cache = result;
        return result;
    }
}
