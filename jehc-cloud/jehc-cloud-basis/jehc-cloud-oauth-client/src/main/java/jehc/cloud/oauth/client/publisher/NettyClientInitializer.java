package jehc.cloud.oauth.client.publisher;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import jehc.cloud.oauth.client.publisher.handler.NettyClientHandler;

/**
 * @Desc 服务器通道初始化
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class NettyClientInitializer extends ChannelInitializer<SocketChannel> {
    private NettyClient nettyClient;

    private String clientId;//客户端id（每个服务对应一个客户端唯一id）

    private String clientGroupId;//组Id（可以存多个服务共享一个组Id）

    public NettyClientInitializer(){

    }

    public NettyClientInitializer(NettyClient nettyClient,String clientGroupId,String clientId){
        this.nettyClient = nettyClient;
        this.clientGroupId = clientGroupId;
        this.clientId = clientId;
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        socketChannel.pipeline().addLast("decoder", new StringDecoder());
        socketChannel.pipeline().addLast("encoder", new StringEncoder());
        socketChannel.pipeline().addLast(new NettyClientHandler(nettyClient,clientGroupId,clientId));
    }
}