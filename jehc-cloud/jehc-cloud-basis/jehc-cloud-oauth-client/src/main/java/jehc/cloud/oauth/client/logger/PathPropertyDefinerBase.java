package jehc.cloud.oauth.client.logger;

import ch.qos.logback.core.PropertyDefinerBase;
import jehc.cloud.oauth.client.util.SDKUtil;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

/**
 * @Desc Logback 文件存放路径
 * @Author dengcj
 * @CreateTime 2023-04-12 11:14:15
 */
@Component
public class PathPropertyDefinerBase  extends PropertyDefinerBase {
    private String cache;

    @Resource
    SDKUtil sdkUtil;

    public String getPropertyValue() {
        if (cache != null) {
            return cache;
        }
        String result = sdkUtil.getLogPath();
        cache = result;
        return result;
    }
}
