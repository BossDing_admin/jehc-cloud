package jehc.cloud.oauth.client.util;
import jehc.cloud.oauth.client.config.PLConfig;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.net.InetAddress;

/**
 * @Desc SDK工具类
 * @Author dengcj
 * @CreateTime 2023-04-12 11:14:15
 */
@Component
@Data
public class SDKUtil {

    static Logger logger = LoggerFactory.getLogger(SDKUtil.class);

    @Resource
    Environment environment;

    @Resource
    PLConfig plConfig;

    boolean available;

    public String getProjectName(){
        String projectName = environment.getProperty("spring.application.name");
        if(null == projectName || "".equals(projectName)){
            projectName = environment.getProperty("project.name");
        }
        return projectName;
    }

    /**
     * 获取日志存储路径
     * 日志目录存储 路径 优先级按-DlogPath=D:\\logs > nacos配置 > 默认
     * @return
     */
    public String getLogPath(){
        String logPath = environment.getProperty("logPath");
        if(null == logPath || "".equals(logPath)){
            if("".equals(plConfig.getPath())){
            logPath = System.getProperty("user.home")+"/logs/oauth";//硬盘挂在日志
            }
        }
        return logPath;
    }


    /**
     * 获取本机IP
     * @return
     */
    public static String getLocalIp(){
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            return inetAddress.getHostAddress();
        }catch (Exception e){
            logger.error("获取本机IP异常：{}",e);
            return "unknown";
        }
    }

}
