package jehc.cloud.oauth.client.timer.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import jehc.cloud.common.base.BaseHttpSessionEntity;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.CollectionUtil;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.RestTemplateUtil;
import jehc.cloud.oauth.client.util.OauthAttributesUtil;
import jehc.cloud.oauth.client.util.TokenAttributesUtil;
import jehc.cloud.oauth.client.vo.Transfer;
import jehc.cloud.oauth.client.vo.TransferSub;
import lombok.extern.slf4j.Slf4j;
import java.util.*;
import java.util.concurrent.RecursiveTask;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class PullTokenTask  extends RecursiveTask<Integer> {
    private Map<String,Transfer> map;

    int groupNumber;

    RestTemplateUtil restTemplateUtil;

    OauthAttributesUtil oauthAttributesUtil;

    TokenAttributesUtil tokenAttributesUtil;

    public PullTokenTask(Map<String,Transfer> map, int groupNumber){
        this.map = map;
        this.groupNumber = groupNumber;
    }
    public PullTokenTask(Map<String,Transfer> map, int groupNumber, RestTemplateUtil restTemplateUtil){
        this.map = map;
        this.groupNumber = groupNumber;
        this.restTemplateUtil = restTemplateUtil;
    }

    public PullTokenTask(Map<String,Transfer> map, int groupNumber, RestTemplateUtil restTemplateUtil,OauthAttributesUtil oauthAttributesUtil,TokenAttributesUtil tokenAttributesUtil){
        this.map = map;
        this.groupNumber = groupNumber;
        this.restTemplateUtil = restTemplateUtil;
        this.oauthAttributesUtil = oauthAttributesUtil;
        this.tokenAttributesUtil = tokenAttributesUtil;
    }


    @Override
    protected Integer compute() {
        int result=0;
        if(cn.hutool.core.collection.CollectionUtil.isEmpty(map)){
            return result;
        }
        if(map.size()<=groupNumber){
            //一个任务即可
            result+=doCallable(map);
        }else{
            //拆分任务
            List<PullTokenTask> subTaskList =createSubTasks(map);
//            //方法一 采用fork（注意：该效率不是太高）
//            for(FasErrorTask subTask :subTaskList){
//                subTask.fork();//子任务计划执行
//            }

            //方法二 采用invokeAll（注意：该效率高一些）
            invokeAll(subTaskList);

            //合并执行结果
            for(PullTokenTask subTask :subTaskList){
                result+=subTask.join();
            }
        }
        return result;
    }

    /**
     *
     * @param map
     * @return
     */
    private Integer doCallable(Map<String,Transfer> map){
        Integer result = pull(map);
        return result;
    }

    /**
     * 创建子任务
     * @param map
     * @return
     */
    private List<PullTokenTask> createSubTasks(Map<String,Transfer> map) {
        List<PullTokenTask> subTasks = new ArrayList<PullTokenTask>();
        List<Map<String,Transfer>> dataMap = CollectionUtil.mapChunk(map,groupNumber);
        for(Map<String,Transfer> tokenMap:dataMap){
            PullTokenTask pullTokenTask = new PullTokenTask(tokenMap,groupNumber,restTemplateUtil,oauthAttributesUtil,tokenAttributesUtil);
            subTasks.add(pullTokenTask);
        }
        return subTasks;
    }

    /**
     * 推送
     */
    public Integer pull(Map<String,Transfer> tokenMap){
        Integer result=0;
        if(cn.hutool.core.collection.CollectionUtil.isEmpty(tokenMap)){
            log.info("没有可推送的Token信息");
            return 0;
        }
        try{
            BaseResult baseResult = restTemplateUtil.post(restTemplateUtil.restOauthURL() + "/batchToken",BaseResult.class,tokenMap);
            if(null == baseResult || !baseResult.getSuccess()){
                log.error("更新失败，Token：{}",tokenMap);
            }
            //更新本地Token
            Map<String,TransferSub> mapToken = (Map) baseResult.getData();
            if(!cn.hutool.core.collection.CollectionUtil.isEmpty(mapToken)){
                Iterator<Map.Entry<String, TransferSub>> entryIterator = mapToken.entrySet().iterator();
                while (entryIterator.hasNext()) {
                    Map.Entry<String, TransferSub> e = entryIterator.next();
                    if(null != e.getValue()){
                        TransferSub  transferSub = JSON.parseObject(JSON.toJSONString(e.getValue()), new TypeReference<TransferSub>() { });
                        if(transferSub.isStatus()){
                            oauthAttributesUtil.put(e.getKey(), JsonUtil.fromAliFastJson(transferSub.getInfo(), BaseHttpSessionEntity.class));
                            tokenAttributesUtil.remove(e.getKey());
                        }
                        result++;
                    }
                }
            }
            log.info("更新成功，Token：{}-{}",tokenMap,result);
        }catch (Exception e){
            log.info("更新失败，Token：{}，ERROR:{}",tokenMap,e);
        }
        return result;
    }
}
