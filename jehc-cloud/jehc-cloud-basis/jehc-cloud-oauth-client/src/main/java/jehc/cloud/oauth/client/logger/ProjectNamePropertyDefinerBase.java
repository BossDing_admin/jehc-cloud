package jehc.cloud.oauth.client.logger;

import ch.qos.logback.core.PropertyDefinerBase;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

/**
 * @Desc Logback 文件增加工程名记录
 * @Author dengcj
 * @CreateTime 2023-04-12 11:14:15
 */
@Component
public class ProjectNamePropertyDefinerBase extends PropertyDefinerBase {

    private String cache;

    @Resource
    Environment environment;

    public String getPropertyValue() {
        if (cache != null) {
            return cache;
        }
        String result = environment.getProperty("spring.application.name");
        if(null == result || "".equals(result)){
            result = environment.getProperty("project.name");
        }
        cache = result;
        return result;
    }
}
