package jehc.cloud.oauth.client.vo;

import lombok.Data;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class Transfer {
    String token;
    String actionType;
    boolean lock = false;

    public Transfer(){

    }

    public Transfer(String token){
        this.token = token;
    }


    public Transfer(String token, String actionType){
        this.token = token;
        this.actionType = actionType;
    }
}
