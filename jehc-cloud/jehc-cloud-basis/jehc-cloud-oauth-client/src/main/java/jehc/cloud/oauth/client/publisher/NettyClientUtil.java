package jehc.cloud.oauth.client.publisher;

import io.netty.channel.Channel;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.oauth.client.vo.RequestInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Desc Netty 客户端工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Component
public class NettyClientUtil {

    /**
     * 发送消息
     * @param channel
     * @param requestInfo
     * @return
     */
    public boolean sendMessage(Channel channel, RequestInfo requestInfo){
        boolean result = true;
        try {
            if (channel != null && channel.isActive()) {
                channel.writeAndFlush(JsonUtil.toJson(requestInfo));
            } else {
                log.info("消息发送失败,连接尚未建立!");
                result =  false;
            }
        }catch (Exception e){
            log.error("发送消息失败，信息：",e);
            result =  false;
        }
        return result;
    }
}
