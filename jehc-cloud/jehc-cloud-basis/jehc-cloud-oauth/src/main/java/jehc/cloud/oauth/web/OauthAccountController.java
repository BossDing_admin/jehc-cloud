package jehc.cloud.oauth.web;
import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.Auth;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.entity.UserParamInfo;
import jehc.cloud.common.entity.UserinfoEntity;
import jehc.cloud.common.idgeneration.UUID;
import jehc.cloud.common.util.*;
import jehc.cloud.oauth.model.OauthAccount;
import jehc.cloud.oauth.service.OauthAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @Desc 授权中心账户
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/oauthAccount")
@Api(value = "授权中心账户API",tags = "授权中心账户API",description = "授权中心账户API")
public class OauthAccountController extends BaseAction {
    @Autowired
    private OauthAccountService oauthAccountService;

    /**
     * 查询账户集合并分页
     * @param baseSearch
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询账户集合并分页", notes="分页查找")
    public BasePage getOauthAccountListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<OauthAccount> oauthAccountList = oauthAccountService.getOauthAccountList(condition);
        PageInfo<OauthAccount> page = new PageInfo<OauthAccount>(oauthAccountList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个账户
     * @param account_id
     */
    @ApiOperation(value="查询单个账户", notes="根据账户id查找")
    @NeedLoginUnAuth
    @GetMapping(value="/get/{account_id}")
    public BaseResult getOauthAccountById(@PathVariable("account_id")String account_id){
        OauthAccount oauthAccount = oauthAccountService.getOauthAccountById(account_id);
        return outDataStr(oauthAccount);
    }

    /**
     * 添加
     * @param oauthAccount
     */
    @ApiOperation(value="新增账户", notes="创建账户根据相关资料")
    @PostMapping(value="/add")
    public BaseResult addOauthAccount(@RequestBody OauthAccount oauthAccount){
        int i = 0;
        if(null != oauthAccount){
            oauthAccount.setAccount_id(UUID.toUUID());
            oauthAccount.setCreate_time(getDate());
            i=oauthAccountService.addOauthAccount(oauthAccount);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 修改
     * @param oauthAccount
     */
    @ApiOperation(value="编辑账户", notes="编辑账户根据相关资料")
    @PutMapping(value="/update")
    public BaseResult updateOauthAccount(@RequestBody OauthAccount oauthAccount){
        int i = 0;
        if(null != oauthAccount){
            oauthAccount.setUpdate_time(getDate());
            i=oauthAccountService.updateOauthAccount(oauthAccount);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 冻结
     * @param account_id
     */
    @ApiOperation(value="冻结账户", notes="冻结账户")
    @DeleteMapping(value="/freeze/{account_id}")
    @Auth("/oauthAccount/freeze")
    public BaseResult freezeAccount(@PathVariable("account_id")String account_id){
        int i = 0;
        if(!StringUtil.isEmpty(account_id)){
            String[] account_idList = account_id.split(",");
            for(String id: account_idList){
                OauthAccount oauthAccount = oauthAccountService.getOauthAccountById(account_id);
                oauthAccount.setStatus(1);
                i=oauthAccountService.freezeAccount(oauthAccount);
            }
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 解冻
     * @param account_id
     */
    @ApiOperation(value="解冻账户", notes="解冻账户")
    @DeleteMapping(value="/unfreeze/{account_id}")
    @Auth("/oauthAccount/unfreeze")
    public BaseResult unfreezeAccount(@PathVariable("account_id")String account_id){
        int i = 0;
        if(!StringUtil.isEmpty(account_id)){
            String[] account_idList = account_id.split(",");
            for(String id: account_idList){
                OauthAccount oauthAccount = oauthAccountService.getOauthAccountById(account_id);
                oauthAccount.setStatus(0);
                i=oauthAccountService.freezeAccount(oauthAccount);
            }
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 查询账户集合
     * @param userParamInfo
     */
    @NeedLoginUnAuth
    @PostMapping(value="/infoList")
    @ApiOperation(value="查询账户集合", notes="查询账户集合")
    public BaseResult accountInfoList(@RequestBody UserParamInfo userParamInfo){
        Map<String, Object> condition = new HashMap<>();
        if(!StringUtil.isEmpty(userParamInfo.getAccount_id())){
            condition.put("account_id",userParamInfo.getAccount_id().split(","));
            List<OauthAccount> oauthAccountList = oauthAccountService.infoList(condition);
            List<UserinfoEntity> userinfoEntities = new ArrayList<>();
            if(CollectionUtil.isNotEmpty(oauthAccountList)){
                for(OauthAccount oauthAccount:oauthAccountList){
                    UserinfoEntity userinfoEntity = new UserinfoEntity();
                    userinfoEntity.setXt_userinfo_id(oauthAccount.getAccount_id());
                    userinfoEntity.setXt_userinfo_name(oauthAccount.getAccount());
                    userinfoEntity.setXt_userinfo_realName(oauthAccount.getName());
                    userinfoEntities.add(userinfoEntity);
                }
            }
            userParamInfo.setUserinfoEntities(userinfoEntities);
        }
        return new BaseResult(FastJsonUtils.toJSONString(userParamInfo));
    }

    /**
     * 重置密码
     * @param userParamInfo
     */
    @ApiOperation(value="重置密码", notes="重置密码")
    @PostMapping(value="/restPwd")
    public BaseResult updatePassword(@RequestBody UserParamInfo userParamInfo, HttpServletRequest request){
        int i  = oauthAccountService.restPwd(userParamInfo,request);
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 同步至授权中心
     * @param userinfoEntitys
     */
    @PostMapping(value="/sync")
    @NeedLoginUnAuth
    @ApiOperation(value="同步至授权中心", notes="同步至授权中心")
    public BaseResult syncOauth(@RequestBody List<UserinfoEntity> userinfoEntitys,HttpServletRequest request){
        return oauthAccountService.sync(userinfoEntitys,request);
    }
}
