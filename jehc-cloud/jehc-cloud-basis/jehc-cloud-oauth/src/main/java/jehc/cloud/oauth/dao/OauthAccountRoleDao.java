package jehc.cloud.oauth.dao;
import java.util.List;
import java.util.Map;

import jehc.cloud.oauth.model.OauthAccount;
import jehc.cloud.oauth.model.OauthAccountRole;
/**
 * @Desc 授权中心账户对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthAccountRoleDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthAccountRole> getOauthAccountRoleListByCondition(Map<String, Object> condition);

	/**
	* 添加
	* @param oauthAccountRole 
	* @return
	*/
	int addOauthAccountRole(OauthAccountRole oauthAccountRole);

	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthAccountRole(Map<String, Object> condition);

	List<OauthAccount> getOauthARListByCondition(Map<String, Object> condition);

	/**
	 * 批量删除
	 * @param condition
	 * @return
	 */
	int delBatchOauthAccountRole(Map<String, Object> condition);
}
