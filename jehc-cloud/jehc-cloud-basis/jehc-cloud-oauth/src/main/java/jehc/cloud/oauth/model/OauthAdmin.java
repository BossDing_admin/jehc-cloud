package jehc.cloud.oauth.model;

import lombok.Data;

/**
 * @Desc 管理员中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthAdmin extends OauthAccount{
    private String id;
    private String account_id;
    private Integer level;//级别：全平台管理员/子系统管理员
    private String sys_mode_id;//模块ID
}
