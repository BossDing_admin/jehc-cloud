package jehc.cloud.oauth.service.impl;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.entity.RoleinfoEntity;
import jehc.cloud.common.util.ExceptionUtil;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.oauth.model.OauthAccount;
import jehc.cloud.oauth.util.OauthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.oauth.service.OauthAccountRoleService;
import jehc.cloud.oauth.dao.OauthAccountRoleDao;
import jehc.cloud.oauth.model.OauthAccountRole;

/**
 * @Desc 授权中心账户对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("oauthAccountRoleService")
public class OauthAccountRoleServiceImpl extends BaseService implements OauthAccountRoleService{
	@Autowired
	private OauthAccountRoleDao oauthAccountRoleDao;
	@Autowired
	OauthUtil oauthUtil;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OauthAccountRole> getOauthAccountRoleListByCondition(Map<String,Object> condition){
		try{
			return oauthAccountRoleDao.getOauthAccountRoleListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	* 添加
	* @param oauthAccountRoleList
	* @return
	*/
	public int addOauthAccountRole(RoleinfoEntity roleinfoEntity, List<OauthAccountRole> oauthAccountRoleList){
		int i = 0;
		try {
			for(int j = 0; j < oauthAccountRoleList.size(); j++){
				oauthAccountRoleDao.addOauthAccountRole(oauthAccountRoleList.get(j));
			}
			//处理变更功能资源
			if(!StringUtil.isEmpty(roleinfoEntity.getAccount_id())){
				String[] accountIdArray = roleinfoEntity.getAccount_id().split(",");
				List<String> accountIdList = new ArrayList<>();
				if(null != accountIdArray){
					for(String id: accountIdArray){
						accountIdList.add(id);
					}
				}
				oauthUtil.doTokenResources(roleinfoEntity.getRole_id(),accountIdList);
			}
			i = 1;
		} catch (Exception e) {
			i = 0;
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delOauthAccountRole(Map<String,Object> condition){
		int i = 0;
		try {
			i = oauthAccountRoleDao.delOauthAccountRole(condition);
			//处理变更功能资源
			if(null != condition.get("account_id")){
				String[] accountIdArray = (String[])condition.get("account_id");
				List<String> accountIdList = new ArrayList<>();
				if(null != accountIdArray){
					for(String id: accountIdArray){
						accountIdList.add(id);
					}
				}
				oauthUtil.doTokenResources(String.valueOf(condition.get("role_id")),accountIdList);
			}

		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	public List<OauthAccount> getOauthARListByCondition(Map<String, Object> condition){
		try{
			return oauthAccountRoleDao.getOauthARListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 批量删除
	 * @param condition
	 * @return
	 */
	/**
	 * 批量删除
	 * @param condition
	 * @return
	 */
	public int delBatchOauthAccountRole(Map<String, Object> condition){
		int i = 0;
		try {
			i = oauthAccountRoleDao.delBatchOauthAccountRole(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
