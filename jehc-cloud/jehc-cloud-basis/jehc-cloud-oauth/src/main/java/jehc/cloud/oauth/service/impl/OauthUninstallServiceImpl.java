package jehc.cloud.oauth.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.oauth.dao.OauthResourcesDao;
import jehc.cloud.oauth.dao.OauthSysModeDao;
import jehc.cloud.oauth.dao.OauthSysModulesDao;
import jehc.cloud.oauth.dao.OauthUninstallDao;
import jehc.cloud.oauth.model.OauthResources;
import jehc.cloud.oauth.model.OauthSysModules;
import jehc.cloud.oauth.service.OauthUninstallService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 卸载模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class OauthUninstallServiceImpl extends BaseService implements OauthUninstallService {

    @Autowired
    OauthUninstallDao oauthUninstallDao;

    @Autowired
    OauthSysModeDao oauthSysModeDao;

    @Autowired
    OauthSysModulesDao oauthSysModulesDao;

    @Autowired
    OauthResourcesDao oauthResourcesDao;

    /**
     * 卸载模块数据
     * @param id
     * @return
     */
    public BaseResult delForUninstall(String id){
        if(StringUtil.isEmpty(id)){
            return BaseResult.fail("未能获取到id");
        }
        List<String> sys_modules_idList = new ArrayList<>();
        List<String> menuIdList = new ArrayList<>();
        Map<String,Object> condition = new HashMap<>();
        condition.put("sysmode_id",id.split(","));
        List<OauthSysModules> oauthSysModules = oauthSysModulesDao.getOauthSysModulesListByCondition(condition);
        if(!CollectionUtil.isEmpty(oauthSysModules)){
            for(OauthSysModules sysModules: oauthSysModules){
                if(!StringUtil.isEmpty(sysModules.getSys_modules_id())){
                    sys_modules_idList.add(sysModules.getSys_modules_id());
                }
            }
        }
        if(!CollectionUtil.isEmpty(sys_modules_idList)){
            Map<String,Object> map = new HashMap<>();
            map.put("module_id",sys_modules_idList.toArray());
            oauthUninstallDao.delOauthResourcesByModuleId(map);//1.删除模块

            condition = new HashMap<>();
            condition.put("resources_module_id",sys_modules_idList.toArray());
            List<OauthResources> oauthResources = oauthResourcesDao.getOauthResourcesListByCondition(condition);
            if(!CollectionUtil.isEmpty(oauthResources)){
                for(OauthResources resources: oauthResources){
                    menuIdList.add(resources.getResources_id());
                }
            }
        }
        oauthUninstallDao.delOauthAdminSysByModeId(id);//2.删除子系统
        if(!CollectionUtil.isEmpty(menuIdList)){
            Map<String,Object> map = new HashMap<>();
            map.put("menu_id",menuIdList.toArray());
            oauthUninstallDao.delOauthFunctionInfoByMenuId(map);//3.删除功能
            oauthUninstallDao.delOauthFunctionRoleByMenuId(map);//4.删除授权中心功能对角色
            oauthUninstallDao.delOauthResourcesRoleByMenuId(map);//5.删除授权中心资源对角色
        }
        oauthUninstallDao.delOauthSysModeById(id);//6.根据id删除子系统
        oauthUninstallDao.delOauthSysModulesByModeId(id);//7.删除模块
        oauthUninstallDao.delOauthRoleByModeId(id);//8.删除授权中心角色模块
        oauthUninstallDao.delOauthRAccountTypeByModeId(id);//9.删除账号类型
        return BaseResult.success();
    }
}
