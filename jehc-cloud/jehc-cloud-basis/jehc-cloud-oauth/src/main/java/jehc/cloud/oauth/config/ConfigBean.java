package jehc.cloud.oauth.config;
import jehc.cloud.oauth.command.InitData;
import jehc.cloud.oauth.interceptor.AuthHandler;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Configuration
public class ConfigBean implements WebMvcConfigurer {

    /**
     *
     * @return
     */
    @Bean
    public InitData initData() {
        return new InitData();
    }

    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }


    //由于在拦截器中注解无效，需要提前注入bean
    @Bean
    public AuthHandler authHandler(){
        return new AuthHandler();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(authHandler());
        registration.addPathPatterns("/**");
        registration.excludePathPatterns(
                "/favicon.ico",
                "/js/**",
                "/html/**",
                "/resources/*",
                "/webjars/**",
                "/v2/api-docs/**",
                "/swagger-ui.html/**",
                "/swagger-resources/**",
                "/doc.html/**",
                "/oauth",
                "/isAdmin",
                "/accountInfo",
                "/httpSessionEntity",
                "/actuator/*"
        );
    }
}
