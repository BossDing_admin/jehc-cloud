package jehc.cloud.oauth.vo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@Component
public class ServerInfo {
    @Value("${jehc.oauth.serverHost:}")
    private String serverHost;
    @Value("${jehc.oauth.serverPort:22088}")
    private Integer serverPort;
    @Value("${jehc.oauth.serverId:oauthServer}")
    private String serverId;
    @Value("${jehc.oauth.serverGroupId:oauthServer}")
    private String serverGroupId;
    public ServerInfo(){

    }
}
