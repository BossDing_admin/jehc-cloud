package jehc.cloud.oauth.service.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jehc.cloud.common.session.HttpSessionUtils;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.oauth.dao.OauthFunctionRoleDao;
import jehc.cloud.oauth.model.OauthFunctionRole;
import jehc.cloud.oauth.service.OauthResourcesRoleService;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import jehc.cloud.oauth.util.OauthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.oauth.dao.OauthResourcesRoleDao;
import jehc.cloud.oauth.model.OauthResourcesRole;
/**
 * @Desc 授权中心资源对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("oauthResourcesRoleService")
public class OauthResourcesRoleServiceImpl extends BaseService implements OauthResourcesRoleService {
	@Autowired
	private OauthResourcesRoleDao oauthResourcesRoleDao;
	@Autowired
	private OauthFunctionRoleDao oauthFunctionRoleDao;
	@Autowired
	HttpSessionUtils httpSessionUtils;
	@Autowired
	OauthUtil oauthUtil;

	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OauthResourcesRole> getOauthResourcesRoleListByCondition(Map<String,Object> condition){
		try{
			return oauthResourcesRoleDao.getOauthResourcesRoleListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 分配资源
	 * @param oauthResourcesRoleList
	 * @param oauthFunctionRoleList
	 * @param role_id
	 * @return
	 */
	public int addOauthResourcesRole(List<OauthResourcesRole> oauthResourcesRoleList, List<OauthFunctionRole> oauthFunctionRoleList, String role_id){
		int i = 0;
		try {
			if(!StringUtil.isEmpty(role_id)){
				Map<String, Object> condition = new HashMap<String, Object>();
				condition.put("role_id", role_id);
				i = oauthResourcesRoleDao.delOauthResourcesRole(condition);
				i = oauthFunctionRoleDao.delOauthFunctionRole(condition);
			}
			for(OauthResourcesRole oauthResourcesRole:oauthResourcesRoleList){
				oauthResourcesRoleDao.addOauthResourcesRole(oauthResourcesRole);
			}
			if(null != oauthFunctionRoleList && !oauthFunctionRoleList.isEmpty()){
				for(OauthFunctionRole oauthFunctionRole:oauthFunctionRoleList){
					oauthFunctionRoleDao.addOauthFunctionRole(oauthFunctionRole);
				}
			}
			oauthUtil.doTokenResources(role_id,null);//处理变更功能资源
			i = 1;
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

}
