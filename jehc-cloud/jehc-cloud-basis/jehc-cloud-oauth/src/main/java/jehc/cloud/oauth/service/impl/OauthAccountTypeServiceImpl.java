package jehc.cloud.oauth.service.impl;

import jehc.cloud.common.base.BaseService;
import jehc.cloud.oauth.dao.OauthAccountTypeDao;
import jehc.cloud.oauth.model.OauthAccountType;
import jehc.cloud.oauth.service.OauthAccountTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * @Desc 账号类型
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class OauthAccountTypeServiceImpl extends BaseService implements OauthAccountTypeService {
    @Autowired
    OauthAccountTypeDao oauthAccountTypeDao;
    /**
     * 分页
     * @param condition
     * @return
     */
    public List<OauthAccountType> getOauthAccountTypeList(Map<String,Object> condition){
        return oauthAccountTypeDao.getOauthAccountTypeList(condition);
    }
    /**
     * 查询对象
     * @param account_type_id
     * @return
     */
    public OauthAccountType getOauthAccountTypeById(String account_type_id){
        return oauthAccountTypeDao.getOauthAccountTypeById(account_type_id);
    }


    /**
     * 添加
     * @param oauthAccountType
     * @return
     */
    public int addOauthAccountType(OauthAccountType oauthAccountType){
        oauthAccountType.setCreate_id(getXtUid());
        oauthAccountType.setCreate_time(getDate());
        return oauthAccountTypeDao.addOauthAccountType(oauthAccountType);
    }

    /**
     * 修改（根据动态条件）
     * @param oauthAccountType
     * @return
     */
    public int updateOauthAccountType(OauthAccountType oauthAccountType){
        oauthAccountType.setUpdate_id(getXtUid());
        oauthAccountType.setUpdate_time(getDate());
        return oauthAccountTypeDao.updateOauthAccountType(oauthAccountType);
    }

    /**
     * 删除类型
     * @param oauthAccountType
     * @return
     */
    public int delAccountType(OauthAccountType oauthAccountType){
        return oauthAccountTypeDao.delAccountType(oauthAccountType);
    }

    /**
     * 根据角色获取当前系统中隶属于其账户类型
     * @param condition
     * @return
     */
    public List<OauthAccountType> getAccountTypeListByRoleId(Map<String,Object> condition){
        return oauthAccountTypeDao.getAccountTypeListByRoleId(condition);
    }

    /**
     * 根据账号类型及子系统获取有多少个
     * @param condition
     * @return
     */
    public int getAccountTypeInSysMode(Map<String,Object> condition){
        return oauthAccountTypeDao.getAccountTypeInSysMode(condition);
    }
}
