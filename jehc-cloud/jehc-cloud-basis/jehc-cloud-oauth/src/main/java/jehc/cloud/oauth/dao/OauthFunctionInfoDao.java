package jehc.cloud.oauth.dao;
import java.util.List;
import java.util.Map;
import jehc.cloud.oauth.model.OauthFunctionInfo;

/**
* 功能中心 
* 2019-06-20 14:23:48  邓纯杰
*/
public interface OauthFunctionInfoDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthFunctionInfo> getOauthFunctionInfoListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param function_info_id 
	* @return
	*/
	OauthFunctionInfo getOauthFunctionInfoById(String function_info_id);
	/**
	* 添加
	* @param oauthFunctionInfo 
	* @return
	*/
	int addOauthFunctionInfo(OauthFunctionInfo oauthFunctionInfo);
	/**
	* 修改
	* @param oauthFunctionInfo 
	* @return
	*/
	int updateOauthFunctionInfo(OauthFunctionInfo oauthFunctionInfo);
	/**
	* 修改（根据动态条件）
	* @param oauthFunctionInfo 
	* @return
	*/
	int updateOauthFunctionInfoBySelective(OauthFunctionInfo oauthFunctionInfo);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthFunctionInfo(Map<String, Object> condition);
	/**
	* 批量修改
	* @param oauthFunctionInfoList 
	* @return
	*/
	int updateBatchOauthFunctionInfo(List<OauthFunctionInfo> oauthFunctionInfoList);
	/**
	* 批量修改（根据动态条件）
	* @param oauthFunctionInfoList 
	* @return
	*/
	int updateBatchOauthFunctionInfoBySelective(List<OauthFunctionInfo> oauthFunctionInfoList);

	/**
	 * 初始化集合（for admin all function）
	 * @param condition
	 * @return
	 */
	List<OauthFunctionInfo> getFunctioninfoListForAdmin(Map<String, Object> condition);

	/**
	 * 读取全部功能
	 * @param condition
	 * @return
	 */
	List<OauthFunctionInfo> getOauthFunctionInfoList(Map<String, Object> condition);
}
