package jehc.cloud.oauth.dao;
import java.util.List;
import java.util.Map;
import jehc.cloud.oauth.model.OauthSysModules;

/**
* 授权中心子系统模块 
* 2019-06-20 15:17:14  邓纯杰
*/
public interface OauthSysModulesDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthSysModules> getOauthSysModulesListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param sys_modules_id 
	* @return
	*/
	OauthSysModules getOauthSysModulesById(String sys_modules_id);
	/**
	* 添加
	* @param oauthSysModules 
	* @return
	*/
	int addOauthSysModules(OauthSysModules oauthSysModules);
	/**
	* 修改
	* @param oauthSysModules 
	* @return
	*/
	int updateOauthSysModules(OauthSysModules oauthSysModules);
	/**
	* 修改（根据动态条件）
	* @param oauthSysModules 
	* @return
	*/
	int updateOauthSysModulesBySelective(OauthSysModules oauthSysModules);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthSysModules(Map<String, Object> condition);
	/**
	* 批量修改
	* @param oauthSysModulesList 
	* @return
	*/
	int updateBatchOauthSysModules(List<OauthSysModules> oauthSysModulesList);
	/**
	* 批量修改（根据动态条件）
	* @param oauthSysModulesList 
	* @return
	*/
	int updateBatchOauthSysModulesBySelective(List<OauthSysModules> oauthSysModulesList);
}
