package jehc.cloud.oauth.service.impl;
import java.util.List;
import java.util.Map;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.common.util.file.FileUtil;
import jehc.cloud.oauth.service.OauthResourcesService;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.common.util.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.cloud.oauth.dao.OauthResourcesDao;
import jehc.cloud.oauth.model.OauthResources;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Desc 授权中心资源中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("oauthResourcesService")
@Slf4j
public class OauthResourcesServiceImpl extends BaseService implements OauthResourcesService {
	@Autowired
	private OauthResourcesDao oauthResourcesDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OauthResources> getOauthResourcesListByCondition(Map<String,Object> condition){
		try{
			return oauthResourcesDao.getOauthResourcesListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param resources_id 
	* @return
	*/
	public OauthResources getOauthResourcesById(String resources_id){
		try{
			OauthResources oauthResources = oauthResourcesDao.getOauthResourcesById(resources_id);
			return oauthResources;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param oauthResources 
	* @return
	*/
	public int addOauthResources(OauthResources oauthResources){
		int i = 0;
		try {
			oauthResources.setCreate_id(getXtUid());
			oauthResources.setCreate_time(getDate());
			i = oauthResourcesDao.addOauthResources(oauthResources);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param oauthResources 
	* @return
	*/
	public int updateOauthResources(OauthResources oauthResources){
		int i = 0;
		try {
			oauthResources.setUpdate_id(getXtUid());
			oauthResources.setUpdate_time(getDate());
			i = oauthResourcesDao.updateOauthResources(oauthResources);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param oauthResources 
	* @return
	*/
	public int updateOauthResourcesBySelective(OauthResources oauthResources){
		int i = 0;
		try {
			oauthResources.setUpdate_id(getXtUid());
			oauthResources.setUpdate_time(getDate());
			i = oauthResourcesDao.updateOauthResourcesBySelective(oauthResources);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delOauthResources(Map<String,Object> condition){
		int i = 0;
		try {
			i = oauthResourcesDao.delOauthResources(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param oauthResourcesList 
	* @return
	*/
	public int updateBatchOauthResources(List<OauthResources> oauthResourcesList){
		int i = 0;
		try {
			i = oauthResourcesDao.updateBatchOauthResources(oauthResourcesList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param oauthResourcesList 
	* @return
	*/
	public int updateBatchOauthResourcesBySelective(List<OauthResources> oauthResourcesList){
		int i = 0;
		try {
			i = oauthResourcesDao.updateBatchOauthResourcesBySelective(oauthResourcesList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 根据Role查资源
	 * @param condition
	 * @return
	 */
	public List<OauthResources> getResourcesListForRole(Map<String, Object> condition){
		return oauthResourcesDao.getResourcesListForRole(condition);
	}

	/**
	 * 获取全部资源
	 * @param condition
	 * @return
	 */
	public List<OauthResources> getOauthResourcesListAll(Map<String, Object> condition){
		return oauthResourcesDao.getOauthResourcesListAll(condition);
	}

	/**
	 * 导出资源
	 * @param response
	 * @param resources_id
	 */
	public void exportOauthResources(HttpServletResponse response, String resources_id){
		try{
			if(StringUtil.isEmpty(resources_id)){
				log.warn("未能获取到资源id");
				throw new ExceptionUtil("未能获取到资源id");
			}
			OauthResources oauthResources = oauthResourcesDao.getOauthResourcesById(resources_id);
			if(null == oauthResources){
				log.warn("未能获取到资源信息");
				throw new ExceptionUtil("未能获取到资源信息");
			}

			String json = JsonUtil.toFastJson(oauthResources);
			FileUtil.exportJsonFile(response,json,"-"+oauthResources.getResources_title()+".json");
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 导入资源
	 * @param multipartFile
	 * @return
	 */
	public BaseResult importOauthResources(HttpServletRequest request, HttpServletResponse response, MultipartFile multipartFile){
		String jsonText = FileUtil.readJson(multipartFile);
		if(StringUtil.isEmpty(jsonText)){
			return BaseResult.fail("导入内容为空！");
		}
		OauthResources oauthResources = JsonUtil.fromAliFastJson(jsonText,OauthResources.class);
		if(null == oauthResources){
			return BaseResult.fail("未能解析到资源信息！");
		}
		if(StringUtil.isEmpty(oauthResources.getResources_id())){
			return BaseResult.fail("未能解析到资源id！");
		}
		if(StringUtil.isEmpty(oauthResources.getResources_title())){
			return BaseResult.fail("未能解析到资源名称！");
		}
		OauthResources resources = getOauthResourcesById(oauthResources.getResources_id());
		if(null == resources){//新增
			addOauthResources(oauthResources);
		}else{
			updateOauthResourcesBySelective(oauthResources);
		}
		return BaseResult.success();
	}
}
