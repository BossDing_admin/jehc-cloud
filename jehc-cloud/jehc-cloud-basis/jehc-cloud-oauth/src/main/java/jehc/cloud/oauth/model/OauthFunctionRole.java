package jehc.cloud.oauth.model;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 授权中心功能对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthFunctionRole extends BaseEntity{
	private String function_role_id;/**主键**/
	private String function_info_id;/**功能编号外键（即功能表主键）**/
	private String role_id;/**角色编号外键（即角色表主键）**/
	private String resources_id;/**资源编号外键（即资源表主键）**/

	private String function_info_url;
	private String function_info_method;
	private int isfilter;
	private int isAuthority;
}
