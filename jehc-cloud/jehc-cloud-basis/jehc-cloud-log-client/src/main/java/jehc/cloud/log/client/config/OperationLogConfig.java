package jehc.cloud.log.client.config;
import jehc.cloud.log.client.handler.OpeationHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <p>
 * 系统日志拦截器生效
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Configuration
public class OperationLogConfig implements WebMvcConfigurer {
    public OperationLogConfig() {
    }

    @Bean
    public OpeationHandler opHandler() {
        return new OpeationHandler();
    }

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.opHandler()).addPathPatterns(new String[]{"/**"});
    }
}
