package jehc.cloud.log.client.worker;

import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.util.RestTemplateUtil;
import jehc.cloud.log.client.entity.LogOperateDTO;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 操作日志工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogOperateWorker implements Runnable {

    private LogOperateDTO logOperateDTO;//操作日志传输对象

    private HttpServletRequest request;//操作日志传输对象

    RestTemplateUtil restTemplateUtil;//restTemplateUtil工具类

    public LogOperateWorker(LogOperateDTO logOperateDTO){
        this.logOperateDTO = logOperateDTO;
    }

    public LogOperateWorker(LogOperateDTO logOperateDTO, HttpServletRequest request, RestTemplateUtil restTemplateUtil){
        this.logOperateDTO = logOperateDTO;
        this.request = request;
        this.restTemplateUtil = restTemplateUtil;
    }

    /**
     *
     */
    public void run(){
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录日志失败:"+logOperateDTO.toString()+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
		try {
			log.info("记录操作日志开始");
            restTemplateUtil.post(restTemplateUtil.restLogUrl()+"/logOperate/add", BaseResult.class,logOperateDTO);
            log.info("记录操作日志结束");
		} catch (Exception e) {
			log.info("记录操作日志失败:"+logOperateDTO.toString()+"，异常信息：{}",e);
		}
    }
}
