package jehc.cloud.log.client.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Desc 全局参数记录日志注解
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Target(value = {java.lang.annotation.ElementType.METHOD})
@Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface OperationLogAnnotation {
    String field() default  "";

    Class<?> response() default Void.class;

    boolean ignoreJsonView() default false;
} 
