package jehc.cloud.rds.dao;

import jehc.cloud.rds.model.OMPRedisConfig;

import java.util.List;
import java.util.Map;

/**
 * @Desc Redis配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OMPRedisConfigDao {
    /**
     * 初始化分页
     * @param condition
     * @return
     */
    List<OMPRedisConfig> getRedisConfigListByCondition(Map<String, Object> condition);

    /**
     * 查询对象
     * @param id
     * @return
     */
    OMPRedisConfig getRedisConfigById(String id);

    /**
     * 添加
     * @param redisConfig
     * @return
     */
    int addRedisConfig(OMPRedisConfig redisConfig);

    /**
     * 修改
     * @param redisConfig
     * @return
     */
    int updateRedisConfig(OMPRedisConfig redisConfig);

    /**
     * delRedisConfig
     * @param condition
     * @return
     */
    int delRedisConfig(Map<String, Object> condition);

    /**
     * 根据id查找列表
     * @param condition
     * @return
     */
    List<OMPRedisConfig> getRedisConfigList(Map<String, Object> condition);
}
