package jehc.cloud.rds.web;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.rds.service.OMPRedisInfoService;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.rds.model.OMPRedisInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc Redis监控日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/omp/redisInfo")
@Api(value = "Redis监控日志",tags = "Redis监控日志",description = "Redis监控日志")
public class OMPRedisInfoController extends BaseAction {
    @Autowired
    OMPRedisInfoService redisInfoService;

    /**
     * 查询Redis监控日志列表并分页
     * @param baseSearch
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询Redis监控日志列表并分页", notes="查询Redis监控日志列表并分页")
    public BasePage getRedisInfoListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<OMPRedisInfo> redisInfoList = redisInfoService.getRedisInfoListByCondition(condition);
        PageInfo<OMPRedisInfo> page = new PageInfo<OMPRedisInfo>(redisInfoList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个Redis监控日志
     * @param id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    @ApiOperation(value="查询单个Redis监控日志", notes="查询单个Redis监控日志")
    public BaseResult<OMPRedisInfo> getRedisInfoById(@PathVariable("id")String id){
        OMPRedisInfo ompRedisInfo = redisInfoService.getRedisInfoById(id);
        return outDataStr(ompRedisInfo);
    }

    /**
     * 删除
     * @param id
     */
    @DeleteMapping(value="/delete")
    @ApiOperation(value="删除", notes="删除")
    public BaseResult delRedisInfo(String id){
        int i = 0;
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id.split(","));
            i=redisInfoService.delRedisInfo(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
}
