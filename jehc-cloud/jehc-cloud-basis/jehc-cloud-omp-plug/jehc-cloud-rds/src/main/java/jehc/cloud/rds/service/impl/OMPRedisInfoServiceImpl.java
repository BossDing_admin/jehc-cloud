package jehc.cloud.rds.service.impl;
import jehc.cloud.rds.dao.OMPRedisInfoDao;
import jehc.cloud.common.base.BaseService;
import jehc.cloud.rds.service.OMPRedisInfoService;
import jehc.cloud.rds.model.OMPRedisInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc Redis信息监控
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class OMPRedisInfoServiceImpl extends BaseService implements OMPRedisInfoService {
    @Autowired
    private OMPRedisInfoDao ompRedisInfoDao;

    /**
     * 初始化分页
     * @param condition
     * @return
     */
    public List<OMPRedisInfo> getRedisInfoListByCondition(Map<String,Object> condition){
        return ompRedisInfoDao.getRedisInfoListByCondition(condition);
    }

    /**
     * 查询对象
     * @param id
     * @return
     */
    public OMPRedisInfo getRedisInfoById(String id){
        return ompRedisInfoDao.getRedisInfoById(id);
    }

    /**
     * 添加
     * @param ompRedisInfo
     * @return
     */
    public int addRedisInfo(OMPRedisInfo ompRedisInfo){
        return ompRedisInfoDao.addRedisInfo(ompRedisInfo);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delRedisInfo(Map<String,Object> condition){
        return ompRedisInfoDao.delRedisInfo(condition);
    }
}
