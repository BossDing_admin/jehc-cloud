package jehc.cloud.rds.init;

import jehc.cloud.rds.service.OMPRedisConfigService;
import jehc.cloud.rds.model.OMPRedisConfig;
import jehc.cloud.rds.util.JedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc Redis监控配置初始化（监控）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Slf4j
@Order(0)
public class RedisConfigInit implements CommandLineRunner {

    @Autowired
    JedisUtil jedisUtil;

    @Autowired
    OMPRedisConfigService redisConfigService;

    @Override
    public void run(String... args) throws Exception {
        try {
            Map<String,Object> condition = new HashMap<>();
            List<OMPRedisConfig> ompRedisConfigs = redisConfigService.getRedisConfigListByCondition(condition);
            for(OMPRedisConfig ompRedisConfig: ompRedisConfigs){
                jedisUtil.initJedisPoolMap(ompRedisConfig);
            }
        }catch (Exception e){
            log.error("采集Reids配置初始化异常：{}",e);
        }
    }
}
