package jehc.cloud.rds.service;

import jehc.cloud.rds.model.OMPRedisInfo;
import java.util.List;
import java.util.Map;

/**
 * @Desc Redis信息监控
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OMPRedisInfoService {
    /**
     * 初始化分页
     * @param condition
     * @return
     */
    List<OMPRedisInfo> getRedisInfoListByCondition(Map<String, Object> condition);

    /**
     * 查询对象
     * @param id
     * @return
     */
    OMPRedisInfo getRedisInfoById(String id);

    /**
     * 添加
     * @param ompRedisInfo
     * @return
     */
    int addRedisInfo(OMPRedisInfo ompRedisInfo);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delRedisInfo(Map<String, Object> condition);
}
