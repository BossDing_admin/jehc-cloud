package jehc.cloud.route.vo;

import io.netty.channel.Channel;
import lombok.Data;

/**
 * @Desc 通道信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class ChannelEntity {
    Channel channel;
    private String clientId;
    private String clientGroupId;//组 可用于客户端集群中只有一个可被激活
    private RequestInfo requestInfo;

    public ChannelEntity(){

    }

    public ChannelEntity(Channel channel){
        this.channel = channel;
    }

    public ChannelEntity(Channel channel,String clientGroupId){
        this.channel = channel;
        this.clientId = clientGroupId;
    }

    public ChannelEntity(Channel channel,String clientGroupId,String clientId){
        this.channel = channel;
        this.clientId = clientGroupId;
        this.clientId = clientId;
    }

    public ChannelEntity(Channel channel,String clientGroupId,String clientId,RequestInfo requestInfo){
        this.channel = channel;
        this.clientGroupId = clientGroupId;
        this.clientId = clientId;
        this.requestInfo =requestInfo;
    }
}
