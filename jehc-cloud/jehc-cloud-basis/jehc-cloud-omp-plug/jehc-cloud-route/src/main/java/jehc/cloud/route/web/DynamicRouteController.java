package jehc.cloud.route.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.route.model.DynamicRoute;
import jehc.cloud.route.service.DynamicRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 路由信息
* 2015-05-24 08:36:53  邓纯杰
*/
@RestController
@RequestMapping("/dynamicRoute")
@Api(value = "路由信息",tags = "路由信息",description = "路由信息")
public class DynamicRouteController extends BaseAction {

	@Autowired
	private DynamicRouteService dynamicRouteService;

	/**
	* 查询路由信息列表并分页
	* @param baseSearch
	*/
	@PostMapping(value="/list")
	@NeedLoginUnAuth
	@ApiOperation(value="查询路由信息列表并分页", notes="查询路由信息列表并分页")
	public BasePage getDynamicRouteListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<DynamicRoute> dynamicRouteList = dynamicRouteService.getDynamicRouteListByCondition(condition);
		PageInfo<DynamicRoute> page = new PageInfo<DynamicRoute>(dynamicRouteList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	* 查询单个路由
	* @param id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	@ApiOperation(value="查询单个路由", notes="查询单个路由")
	public BaseResult<DynamicRoute> getDynamicRouteById(@PathVariable("id")String id){
		DynamicRoute dynamicRoute = dynamicRouteService.getDynamicRouteById(id);
		return BaseResult.success(dynamicRoute);
	}

	/**
	 * 添加
	 * @param dynamicRoute
	 * @return
	 */
	@PostMapping(value="/add")
	@ApiOperation(value="添加", notes="添加")
	public BaseResult addDynamicRoute(@RequestBody DynamicRoute dynamicRoute){
		int i = 0;
		if(null != dynamicRoute){
			i=dynamicRouteService.addDynamicRoute(dynamicRoute);
		}
		if(i>0){
			return BaseResult.success();
		}else{
			return BaseResult.fail();
		}
	}

	/**
	* 编辑
	* @param dynamicRoute
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑", notes="编辑")
	public BaseResult updateDynamicRoute(@RequestBody DynamicRoute dynamicRoute){
		int i = 0;
		if(null !=dynamicRoute){
			i=dynamicRouteService.updateDynamicRoute(dynamicRoute);
		}
		if(i>0){
			return BaseResult.success();
		}else{
			return BaseResult.fail();
		}
	}

	/**
	 * 更新状态
	 * @param dynamicRoute
	 */
	@PutMapping(value="/updateStatus")
	@ApiOperation(value="更新状态", notes="更新状态")
	public BaseResult updateStatus(@RequestBody DynamicRoute dynamicRoute){
		int i = 0;
		if(null !=dynamicRoute){
			i=dynamicRouteService.updateStatus(dynamicRoute);
		}
		if(i>0){
			return BaseResult.success();
		}else{
			return BaseResult.fail();
		}
	}


	/**
	* 删除
	* @param id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除", notes="删除")
	public BaseResult delDynamicRoute(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=dynamicRouteService.delDynamicRoute(condition);
		}
		if(i>0){
			return BaseResult.success();
		}else{
			return BaseResult.fail();
		}
	}
}
