package jehc.cloud.scms.collect;

import jehc.cloud.scms.model.SCMSMonitor;
/**
 * @Desc 上报服务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface SCMSMonitorService {
    /**
     * 保存上报信息
     * @param scmsMonitor
     * @return
     */
    int saveSCMSMonitor(SCMSMonitor scmsMonitor);
}
