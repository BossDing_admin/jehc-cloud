package jehc.cloud.scms.model;
import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.cloud.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Desc 服务器内存
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class SCMSMonitorMem extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String mac;/**主表mac**/
	private String mem_total;/**总内存**/
	private String mem_curr_use;/**当前内存使用量**/
	private String mem_curr_sy;/**当前内存剩余量**/
	private String mem_jh_total;/**交换区总量**/
	private String mem_jh_curr_use;/**当前交换区使用量**/
	private String mem_jh_sy;/**当前交换区剩余量**/
	private String ip;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
}
