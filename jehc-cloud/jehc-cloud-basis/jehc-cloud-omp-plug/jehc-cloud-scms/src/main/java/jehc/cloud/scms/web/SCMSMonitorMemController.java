package jehc.cloud.scms.web;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.cloud.scms.service.SCMSMonitorMemService;
import jehc.cloud.common.annotation.NeedLoginUnAuth;
import jehc.cloud.common.base.BaseAction;
import jehc.cloud.common.base.BasePage;
import jehc.cloud.common.base.BaseResult;
import jehc.cloud.common.base.BaseSearch;
import jehc.cloud.common.util.JsonUtil;
import jehc.cloud.common.util.StringUtil;
import jehc.cloud.common.util.date.DateUtils;
import jehc.cloud.scms.model.SCMSMonitorMem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @Desc 服务器内存
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/scms/monitorMem")
@Api(value = "服务器内存监控",tags = "服务器内存监控",description = "服务器内存监控")
public class SCMSMonitorMemController extends BaseAction {

    @Autowired
    SCMSMonitorMemService scmsMonitorMemService;
    /**
     * 查询服务器内存列表并分页
     * @param baseSearch
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询服务器内存列表并分页", notes="查询服务器内存列表并分页")
    public BasePage getSCMSMonitorMemListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<SCMSMonitorMem> scmsMonitorMemList = scmsMonitorMemService.getSCMSMonitorMemListByCondition(condition);
        PageInfo<SCMSMonitorMem> page = new PageInfo<SCMSMonitorMem>(scmsMonitorMemList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个服务器内存信息
     * @param id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    @ApiOperation(value="查询单个服务器内存信息", notes="查询单个服务器内存信息")
    public BaseResult<SCMSMonitorMem> getSCMSMonitorMemById(@PathVariable("id")String id){
        SCMSMonitorMem scmsMonitorMem = scmsMonitorMemService.getSCMSMonitorMemById(id);
        return outDataStr(scmsMonitorMem);
    }

    /**
     * 删除
     * @param id
     */
    @DeleteMapping(value="/delete")
    @ApiOperation(value="删除", notes="删除")
    public BaseResult delSCMSMonitorMem(String id){
        int i = 0;
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id.split(","));
            i=scmsMonitorMemService.delSCMSMonitorMem(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 内存
     * @param scmsMonitorMem
     */
    @NeedLoginUnAuth
    @PostMapping(value="/mems")
    @ApiOperation(value="内存", notes="内存")
    public BaseResult<Map<String,Object>> mems(SCMSMonitorMem scmsMonitorMem){
        if(StringUtil.isEmpty(scmsMonitorMem.getMac())){
            scmsMonitorMem.setMac("-1");
        }
        PageHelper.offsetPage(0, 5);
        List<SCMSMonitorMem> scmsMonitorMemList = scmsMonitorMemService.getSCMSMonitorMemList(scmsMonitorMem);
        Map<String,Object> map = new HashMap<>();
        List<String> xdata = new ArrayList<>();
        List<String> ydata0 = new ArrayList<>();
        List<String> ydata1 = new ArrayList<>();
        List<String> ydata2 = new ArrayList<>();
        List<String> ydata3 = new ArrayList<>();
        if(!CollectionUtil.isEmpty(scmsMonitorMemList)){
            for(SCMSMonitorMem monitorMem:scmsMonitorMemList){
                xdata.add(getTime(monitorMem.getCreate_time()));
                try {
                    ydata0.add(""+(new Long(monitorMem.getMem_curr_use()))/(1024*1024));
                    ydata1.add(""+(new Long(monitorMem.getMem_curr_sy()))/(1024*1024));
                    ydata2.add(""+(new Long(monitorMem.getMem_jh_curr_use()))/(1024*1024));
                    ydata3.add(""+(new Long(monitorMem.getMem_jh_sy()))/(1024*1024));
                }catch (Exception e){
                    ydata0.add("0");
                    ydata1.add("0");
                    ydata2.add("0");
                    ydata3.add("0");
                }
            }
        }
        map.put("xdata",xdata);
        map.put("ydata0",ydata0);
        map.put("ydata1",ydata1);
        map.put("ydata2",ydata2);
        map.put("ydata3",ydata3);
        return BaseResult.success(map);
    }

    /**
     * 得到当前时间字符串 格式（HH:mm:ss）
     */
    public static String getTime(Date date) {
        return DateUtils.formatDate(date, "HH:mm:ss");
    }
}
